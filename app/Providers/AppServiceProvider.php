<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// include repository interface here
use App\Contracts\UserRepositoryInterface;

// include repository here
use App\Repositories\UserRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class,           UserRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
