<?php

namespace App\Repositories;

use App\Contracts\UserRepositoryInterface;
use App\Models\{ User, Log, Video };
use Auth;
use Carbon\Carbon;


class UserRepository implements UserRepositoryInterface
{
    private $user_model;

    public function __construct(User $user_model)
    {
        $this->user_model = $user_model;
    }

    public function addLog($data='', $action='', $media_type='', $log_type='', $time_log='')
    {
        $user_login_name = (Auth::user()) ? Auth::user()->first_name. ' ' . Auth::user()->last_name : $data->first_name. ' ' . $data->last_name;
        $user_login_name_user_type = (Auth::user()) ? (Auth::user()->user_type) ? Auth::user()->user_type : '-' : '-';
        $description = '';

        $description = $this->interactionDescription($data, $action, $user_login_name_user_type, $log_type, $user_login_name);

        return Log::create([
            'logs' => $log_type,
            'name' => $user_login_name,
            'action' => $action,
            'description' => $description,
            'date' => Carbon::now()->format('Y-m-d'),
            'time_log' => ($time_log != '') ? $time_log : Carbon::now(),
            'user_type' => $user_login_name_user_type,
            'log_type' => ($media_type != '') ? $media_type : '-',
        ]);
    }

    private function interactionDescription($data, $action, $user_type, $log_type, $user_login_name)
    {
        $log_name = "";
        if(($log_type == Log::USERLOG || $log_type == Log::CONSULTANTLOG) && ($action != Log::LIKEACTION && $action != Log::UNLIKEACTION)) {
            $log_name = $data->first_name.' '.$data->last_name;
        } else if($log_type == Log::FILELOG || $log_type == Log::PRODUCTLOG) {
            $log_name = $data->name;
        } else if($log_type == Log::CATEGORYLOG) {
            $log_name = $data->category;
        }

        $description = ucfirst($user_type) . " " . ucfirst($action) . "d a " . ucfirst($log_type) . " - " . $log_name;

        if($action == Log::LOGINACTION || $action == Log::LOGOUTACTION || $action == Log::LIKEACTION || $action == Log::UNLIKEACTION || $action == Log::WATCHINGORLISTENING || $action == Log::BROWSING) {
            if($action == Log::LOGINACTION) { 
                $actionRenamed = 'Logged in';
            } else if($action == Log::LOGOUTACTION) { 
                $actionRenamed = 'Logged out';
            } else if($action == Log::LIKEACTION || $action == Log::UNLIKEACTION) {
                $actionRenamed = $action;
                $file_data = $this->getFile($data);
                $log_name = $file_data->name;
            } else if($action == Log::WATCHINGORLISTENING) {
                $actionRenamed = "is watching / listening on a media post";
                $log_type = "";
            } else {
                $actionRenamed = "is active on the app";
                $log_type = "";
            }

            $description = ucwords($user_login_name) . " " . ucfirst($actionRenamed) . " " . ucfirst($log_type) . " - " . $log_name;
        }

        return $description;
    }

    private function getFile($file_id)
    {   
        return Video::find($file_id);
    }
}