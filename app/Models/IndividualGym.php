<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndividualGym extends Model
{
    use HasFactory;

    public $table = 'individual_gyms';

    protected $fillable = [
        'name',
        'description',
        'address',
        'imagePath',
        'franchise_id',
    ];

    public function gym_franchise()
    {
        return $this->belongsTo('App\Models\GymFranchise', "franchise_id");
    }

    public function members()
    {
        return $this->hasMany('App\Models\User', "gym_id");
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', "gym_id");
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', "gym_id");
    }

    public function dispensers()
    {
        return $this->hasMany('App\Models\Dispenser', "gym_id");
    }
}
