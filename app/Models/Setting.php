<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    public $table = 'settings';

    protected $fillable = [
        'topup_amount',
        'multiplier',
        'points',
        'bonus',
        'total_points',
    ];
}
