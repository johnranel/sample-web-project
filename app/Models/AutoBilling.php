<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutoBilling extends Model
{
    use HasFactory;

    public $table = 'auto_billing';

    protected $fillable = [
        'enable_billing',
        'amount',
        'timing',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', "user_id");
    }
}