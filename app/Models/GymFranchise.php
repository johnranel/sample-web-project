<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GymFranchise extends Model
{
    use HasFactory;

    public $table = 'gym_franchises';

    protected $fillable = [
        'name',
        'description',
        'address',
        'imagePath',
        'phone',
        'email'
    ];

    public function gyms()
    {
        return $this->hasMany("App\Models\IndividualGym", "franchise_id");
    }

    public function transactions()
    {
        return $this->hasMany("App\Models\Transaction", "franchise_id");
    }

    public function users()
    {
        return $this->hasMany("App\Models\User", "franchise_id");
    }
}
