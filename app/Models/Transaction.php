<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public $table = 'transactions';

    protected $fillable = [
        'type',
        'amount', 
        'points',
        'description',
        'stripe_client_secret',
        'user_id',
        'franchise_id',
        'gym_id',
        'product_id',
        'dispensers_id',
        'shaker_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', "user_id");
    }

    public function franchise()
    {
        return $this->belongsTo('App\Models\GymFranchise', "franchise_id");
    }

    public function gym()
    {
        return $this->belongsTo('App\Models\IndividualGym', "gym_id");
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', "product_id");
    }

    public function dispenser()
    {
        return $this->belongsTo('App\Models\Dispenser', "dispensers_id");
    }

    public function shaker()
    {
        return $this->belongsTo('App\Models\Shaker', "shaker_id");
    }
}