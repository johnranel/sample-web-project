<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dispenser extends Model
{
    use HasFactory;

    public $table = 'dispensers';

    protected $fillable = [
        'dispensers_id',
        'gym_id',
    ];

    public function gym()
    {
        return $this->belongsTo("App\Models\IndividualGym", "gym_id");
    }

    public function dispenser_product_list()
    {
        return $this->hasMany("App\Models\DispenserProductList", "dispensers_id");
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', "dispensers_id");
    }
}