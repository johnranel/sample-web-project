<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AthleteProfile extends Model
{
    use HasFactory;

    public $table = 'athlete_profile';

    protected $fillable = [
        'level_of_fitness', 
        'gym_attendance',
        'priorities',
        'type_of_training',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', "user_id");
    }
}
