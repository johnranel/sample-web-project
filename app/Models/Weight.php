<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weight extends Model
{
    use HasFactory;

    public $table = 'weight';

    protected $fillable = [
        'weight', 
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', "user_id");
    }
}
