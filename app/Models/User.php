<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Cashier\Billable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, Billable;

    const MALE = "male";
    const FEMALE = "female";

    const ADMIN = "admin";

    const SuperAdmin = "SuperAdmin";
    const FranchiseAdmin = "FranchiseAdmin";
    const GymAdmin = "GymAdmin";
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'user_type',
        'address',
        "birthdate",
        'gender',
        'mobile_number',
        'email',
        'uncrypt_password',
        'password',
        'image',
        'franchise_id',
        'gym_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function athlete_profile()
    {
        return $this->hasOne("App\Models\AthleteProfile", "user_id");
    }

    public function weight()
    {
        return $this->hasOne("App\Models\Weight", "user_id");
    }

    public function transactions()
    {
        return $this->hasMany("App\Models\Transaction", "user_id");
    }

    public function points()
    {
        return $this->hasOne("App\Models\TopUpPoint", "user_id");
    }

    public function franchise()
    {
        return $this->belongsTo("App\Models\GymFranchise", "franchise_id");
    }

    public function gym()
    {
        return $this->belongsTo("App\Models\IndividualGym", "gym_id");
    }

    public function shaker()
    {
        return $this->hasOne("App\Models\Shaker", "user_id");
    }

    public function auto_billing()
    {
        return $this->hasOne("App\Models\AutoBilling", "user_id");
    }
}
