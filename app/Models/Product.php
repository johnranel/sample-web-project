<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public $table = 'products';

    protected $fillable = [
        'manufacturer_name',
        'name',
        'type',
        'price_in_points',
        'price_in_aud',
        'cost_in_aud',
        'description',
        'flavour',
        'portion_size',
        'promoted_product',
        'product_no'
    ];

    const TYPES = [
        [ 'value' => 'protein', 'name' => 'Protein' ],
        [ 'value' => 'pre workout', 'name' => 'Pre Workout' ],
        [ 'value' => 'post workout', 'name' => 'Post Workout' ],
        [ 'value' => 'aminos', 'name' => 'Aminos' ],
        [ 'value' => 'energizer', 'name' => 'Energizer' ],
    ];

    const FLAVOURS = [
        [ 'value' => 'chocolate', 'name' => 'Chocolate' ],
        [ 'value' => 'mango madness', 'name' => 'Mango Madness' ],
        [ 'value' => 'strawberry watermelon', 'name' => 'Strawberry Watermelon' ],
        [ 'value' => 'fruit punch', 'name' => 'Fruit Punch' ],
        [ 'value' => 'green apple', 'name' => 'Green Apple' ],
        [ 'value' => 'chocolate bliss', 'name' => 'Chocolate Bliss' ],
        [ 'value' => 'apple', 'name' => 'Apple' ],
        [ 'value' => 'turbo chocolate', 'name' => 'Turbo Chocolate' ],
        [ 'value' => 'blue raz', 'name' => 'Blue Raz' ],
        [ 'value' => 'vanilla', 'name' => 'Vanilla' ],
    ];

    public function media()
    {
        return $this->hasMany("App\Models\Media", "product_id");
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', "product_id");
    }

    public function dispenser_product_list()
    {
        return $this->hasMany('App\Models\DispenserProductList', "product_id");
    }
}
