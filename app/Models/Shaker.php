<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shaker extends Model
{
    use HasFactory;
    
    public $table = 'shakers';

    protected $fillable = [
        'shaker_id',
        'price_in_aud',
        'cost_in_aud',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', "user_id");
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', "shaker_id");
    }
}
