<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DispenserProductList extends Model
{
    use HasFactory;

    public $table = 'dispenser_product_list';

    protected $fillable = [
        'dispensers_id',
        'product_id',
    ];

    public function dispenser()
    {
        return $this->belongsTo("App\Models\Dispenser", "dispensers_id");
    }

    public function products()
    {
        return $this->belongsTo("App\Models\Product", "product_id");
    }
}
