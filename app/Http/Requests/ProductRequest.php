<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'manufacturer_name' => 'required|string',
            'name'              => 'required|string',
            'type'              => 'required|string',
            'description'       => 'required|string',
            'flavour'           => 'required|string',
            'portion_size'      => 'required|integer',
        ];
    }

    public function messages()
    {   
        return [
            'manufacturer_name.required'    => 'Manufacturer name is required',
            'name.required'                 => 'Name is required',
            'type.required'                 => 'Type is required',
            'description.required'          => 'Description is required',
            'flavour.required'              => 'Flavour is required',
            'portion_size.required'         => 'Portion size is required',
        ];
    }
}
