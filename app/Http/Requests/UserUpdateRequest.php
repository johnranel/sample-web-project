<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required',
            'gender'     => 'required',
            'birthdate'  => 'required',
        ];
    }

    public function messages()
    {   
        return [
            'first_name.required'    => 'First name is required',
            'last_name.required'     => 'Last name is required',
            'email.required'         => 'Email is required',
            'gender.required'        => 'Gender is required',
            'mobile_number.required' => 'Mobile Number date is required',
            'birthdate.required'     => 'Birth date is required',
        ];
    }
}
