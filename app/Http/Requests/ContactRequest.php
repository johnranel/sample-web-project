<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'postal_code' => 'required',
            'email' => 'required|email',
            'message' => 'required|min:2',
            'country' => 'required',
        ];
    }

    public function messages()
    {   
        return [
            'first_name.required' => 'First name is required',
            'last_name.required' => 'Last name is required',
            'postal_code.required' => 'Postal code is required',
            'email.required' => 'Email is required',
            'message.required' => 'Message is required',
            'country.required' => 'Country is required'
        ];
    }
}
