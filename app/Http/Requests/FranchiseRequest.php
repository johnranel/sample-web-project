<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FranchiseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'description'   => 'required',
            'address'       => 'required',
            'phone'         => 'required',
            'email'         => 'required',
        ];
    }

    public function messages()
    {   
        return [
            'name.required'         => 'First name is required',
            'description.required'  => 'Description is required',
            'address.required'      => 'Address is required',
            'phone.required'        => 'Phone is required',
            'email.required'        => 'Email is required',
        ];
    }
}
