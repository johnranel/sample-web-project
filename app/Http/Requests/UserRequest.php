<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'gender'        => 'required',
            'mobile_number' => 'required',
            'email'         => 'required|unique:users,email',
        ];
    }

    public function messages()
    {   
        return [
            'first_name.required'    => 'First name is required',
            'last_name.required'     => 'Last name is required',
            'gender.required'        => 'Gender is required',
            'mobile_number.required' => 'Mobile number is required',
            'email.required'         => 'Email is required',
        ];
    }

}
