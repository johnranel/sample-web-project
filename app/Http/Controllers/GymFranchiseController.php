<?php

namespace App\Http\Controllers;

use App\Models\{ GymFranchise, IndividualGym, User };
use Illuminate\Http\Request;
use App\Http\Requests\FranchiseRequest;
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use Storage;
use Illuminate\Database\Eloquent\Collection;
use File;

class GymFranchiseController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:franchise-list', ['only' => ['index','show']]);
        $this->middleware('permission:franchise-create', ['only' => ['create','store']]);
        $this->middleware('permission:franchise-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:franchise-delete', ['only' => ['delete', 'destroy']]);
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            return datatables()->collection(GymFranchise::with(['users' => function ($q) { $q->where('user_type', 'FranchiseAdmin'); }])->get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.franchise-management', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function create()
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();
        $user = User::whereHas('roles', function($q){
            $q->where('name', 'FranchiseAdmin');
        })->where('username', 'LIKE', 'franchiseadmin%')->orderBy('username', 'desc')->first();

        return view('admin.franchise.create-edit', ['franchises' => $franchises, 'gyms' => $gyms, 'user' => $user]);
    }
    
    public function store(FranchiseRequest $request)
    {
        $data_franchise = $request->only('name', 'description', 'address', 'phone', 'email');
        if (isset($request['file'])) {
            $data_franchise['imagePath'] = $this->file($request['file']);
        }
        $gymFranchise = GymFranchise::create($data_franchise);

        $data_admin = $request->only('first_name', 'last_name', 'username', 'password');
        $data_admin['birthdate'] = "-";
        $data_admin['mobile_number'] = "-";
        $data_admin['gender'] = "male";
        $data_admin['user_type'] = "FranchiseAdmin";
        $data_admin['franchise_id'] = $gymFranchise->id;
        $data_admin['email'] = $request->email_admin;
        $data_admin['uncrypt_password'] = $request->password;
        $data_admin['password'] = bcrypt($request->password);
        $gymAdmin = User::create($data_admin);

        $role = Role::where('name', "FranchiseAdmin")->first();
        $gymAdmin->assignRole([$role->id]);

        if($request->ajax()) {
            return response()->json($gymFranchise);
        }

        return redirect('/franchise-management')->with('message', 'Successfully created a new franchise.');
    }

    public function delete($id)
    {
        $gymFranchise = GymFranchise::find($id);

        $gymFranchise->delete();
        return response()->json($gymFranchise);
    }

    public function getGymFranchise($id)
    {
        $gymFranchise = GymFranchise::find($id);
        return $gymFranchise;
    }

    public function edit(GymFranchise $gymfranchise)
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.franchise.create-edit', [
            'gymfranchise'  => $gymfranchise, 
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function update(FranchiseRequest $request, $id)
    {
        $gymFranchise = GymFranchise::find($id);

        if(isset($request['file'])) {
            $data['image'] = $this->file($request['file']);
        }

        $gymFranchise->name        = $request->get('name');
        $gymFranchise->description = $request->get('description');
        $gymFranchise->address     = $request->get('address');
        $gymFranchise->phone       = $request->get('phone');
        $gymFranchise->email       = $request->get('email');
        if(isset($request['file'])) {
            $gymFranchise->imagePath   = $data['image'];
        }

        $gymFranchise->save();

        if($request->ajax()) {
            return response()->json($gymFranchise);
        }

        return redirect('/franchise-management')->with('message', 'Successfully updated franchise details.');
    }

    private function file($file)
    {
        $file_tmp= $_FILES['file']['tmp_name'];
        $file_name =$_FILES['file']['name'];
        $type = pathinfo($file_name, PATHINFO_EXTENSION);
        $data = file_get_contents($file_tmp);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $base64_image = $base64;
        unset($base64);
        return $base64_image;
    }

    public function franchiseGymsFetch(GymFranchise $gymFranchise)
    {
        return response()->json($gymFranchise->gyms);
    }
}
