<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ IndividualGym, GymFranchise, User, Dispenser };
use App\Http\Requests\GymRequest;
use Spatie\Permission\Models\Role;
use DB;
use Storage;
use Illuminate\Database\Eloquent\Collection;

class IndividualGymController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:gym-list', ['only' => ['index','show']]);
        $this->middleware('permission:gym-create', ['only' => ['create','store']]);
        $this->middleware('permission:gym-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:gym-delete', ['only' => ['delete', 'destroy']]);
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            return datatables()->collection(IndividualGym::with(['members' => function ($q) { $q->where('user_type', 'GymAdmin'); }, 'gym_franchise', 'dispensers'])->get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.gym-management', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function create()
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();
        $user = User::whereHas('roles', function($q){
            $q->where('name', 'GymAdmin');
        })->where('username', 'LIKE', 'gymadmin%')->orderBy('username', 'desc')->first();

        return view('admin.gym.create-edit', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms, 
            'user'          => $user
        ]);
    }

    public function store(GymRequest $request)
    {
        $data_gym = $request->only('franchise_id', 'name', 'address', 'description', 'file');
        if (isset($request['file'])) {
            $data_gym['imagePath'] = $this->file($request['file']);
        }
        $individualGym = IndividualGym::create($data_gym);
        
        $this->createGymAdmin($request, $individualGym->id);
        $this->createDispenser($request->dispenser, $individualGym->id);

        if($request->ajax()) {
            return response()->json($individualGym);
        }

        return redirect('/gym-management')->with('message', 'Successfully created a new gym and admin.');
    }

    private function createGymAdmin($admin_data, $gym_id)
    {
        $data_admin = $admin_data->only('first_name', 'last_name', 'email', 'username', 'password', 'franchise_id');
        $data_admin['birthdate'] = "-";
        $data_admin['mobile_number'] = "-";
        $data_admin['gender'] = "male";
        $data_admin['user_type'] = "GymAdmin";
        $data_admin['gym_id'] = $gym_id;
        $data_admin['uncrypt_password'] = $admin_data->password;
        $data_admin['password'] = bcrypt($admin_data->password);
        $gymAdmin = User::create($data_admin);

        $role = Role::where('name', "GymAdmin")->first();
        $gymAdmin->assignRole([$role->id]);
    }

    public function edit(IndividualGym $individualGym)
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.gym.create-edit', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms, 
            'gym'           => $individualGym
        ]);
    }

    public function update(IndividualGym $individualGym, GymRequest $request)
    {
        $data = $request->all();
        if (isset($request['file'])) {
            $data['imagePath'] = $this->file($request['file']);
        }
        $individualGym->update($data);
        
        $this->createUpdateDispenser($individualGym->dispensers, $request->dispenser, $individualGym->id);

        if($request->ajax()) {
            return response()->json($individualGym);
        }

        return redirect('/gym-management')->with('message', 'Successfully updated gym details.');
    }

    private function createUpdateDispenser($gym_dispensers, $modified_dispensers, $gym_id)
    {
        foreach($gym_dispensers as $key => $gym_dispenser) {
            $gym_dispenser->delete();
        }

        foreach($modified_dispensers as $key => $modified_dispenser) {
            $modified_dispenser['gym_id'] = $gym_id;
            Dispenser::create($modified_dispenser);
        }
    }

    private function createDispenser($dispensers, $gym_id)
    {
        foreach($dispensers as $key => $dispenser) {
            $dispenser['gym_id'] = $gym_id;
            Dispenser::create($dispenser);
        }
    }

    public function getIndividualGym(IndividualGym $individualGym)
    {
        return $individualGym;
    }

    public function delete(IndividualGym $individualGym)
    {
        $gym_dispensers = $individualGym->dispensers;
        foreach($gym_dispensers as $key => $gym_dispenser) {
            $gym_dispenser->delete();
        }

        $individualGym->delete();

        return response()->json($individualGym);
    }

    public function gymDispenserFetch(IndividualGym $individualGym)
    {
        return response()->json($individualGym->dispensers);
    }

    public function individualGymList(Request $request,$limit = 10, $offset = 0)
    {

        $response = [];
        $model = IndividualGym::query();
        $total = $model->count();

        if ($request->search_gym_franchise != null || $request->search_gym_franchise != '') {
            $model->where('name',$request->search_gym_franchise);
            $model->orWhere(DB::raw("concat(name,' ', description)"), 'LIKE', "%".$request->search_gym_franchise."%");

            $total = $model->count();
        }

        if ($request->has('pagination.perpage')) {
            $limit = $request->get('pagination')['perpage'];
        }
        if ($request->has('pagination.page')) {
            $offset = $request->get('pagination')['page'];
            $offset = $offset - 1;
            $offset = $offset * $limit;
        }

        $pages = $model->count();

        if ($request->name != null || $request->name != '') {
            $data = $model->skip($offset)->take($limit)->orderBy($request->name, 'asc')->get();
        } else {
            $data = $model->skip($offset)->take($limit)->get();
        }

        if ($request->has('sort.sort')) {

            $isSortDecending = true;
            if ($request->get('sort')['sort'] == 'asc') {
                $isSortDecending = false;
            }
            $data = $data->sortBy($request->get('sort')['field'], SORT_REGULAR, $isSortDecending);
        }

        $data = $this->reIndexCollection($data);

        $meta = [
            'page' => $request->get('pagination')['page'],
            'pages' => $pages,
            'perpage' => $limit,
            'total' => $total,
        ];
        $response['meta'] = $meta;
        $response['data'] = $data;

        return $response;
    }

    private function file($file)
    {
        $file_tmp= $_FILES['file']['tmp_name'];
        $file_name =$_FILES['file']['name'];
        $type = pathinfo($file_name, PATHINFO_EXTENSION);
        $data = file_get_contents($file_tmp);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $base64_image = $base64;
        unset($base64);
        return $base64_image;
    }

    private function reIndexCollection(Collection $collections)
    {
        $response = [];
        foreach ($collections as $collection) {
            $response[] = $collection;
        }

        return $response;
    }
}
