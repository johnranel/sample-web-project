<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\{ User, GymFranchise, IndividualGym, Transaction, Product, DispenserProductList };
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class SuperAdminController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:super-admin-list|super-admin-create|super-admin-edit|super-admin-delete', ['only' => ['index','show', 'viewDashboardFranchise']]);
        $this->middleware('permission:super-admin-create', ['only' => ['create','store']]);
        $this->middleware('permission:super-admin-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:super-admin-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $authorizedRoles = [User::SuperAdmin, User::FranchiseAdmin, User::GymAdmin];
        if($request->ajax()) {
            return datatables()->collection(User::whereHas('roles', function ($query) use ($authorizedRoles) {
                $query->whereIn('name', $authorizedRoles);
            })->with('roles', 'franchise', 'gym')->get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.admin-management', ['franchises' => $franchises, 'gyms' => $gyms]);
    }

    public function create()
    {
        $franchises = GymFranchise::all();
        $gyms = IndividualGym::all();
        return view('admin.admin-create-edit.create-edit', [
            'franchises' => $franchises,
            'gyms'       => $gyms
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);

        $role = Role::where('name', $request->roles)->first();
        $user->assignRole([$role->id]);

        return redirect('/admin-management')->with('message', 'Successfully created a new admin.');
    }

    public function edit(User $user)
    {
        $user = User::where('id', $user->id)->with('roles')->first();
        $franchises = GymFranchise::all();
        $gyms = IndividualGym::all();
        return view('admin.admin-create-edit.create-edit', [
            'user'       => $user,
            'franchises' => $franchises,
            'gyms'       => $gyms
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SuperAdmin  $superAdmin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->all();
        $user->update($data);

        $role = Role::where('name', $request->roles)->first();
        $user->syncRoles([$role->id]);

        return redirect('/admin-management')->with('message', 'Successfully updated admin details.');
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuperAdmin  $superAdmin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->with('roles')->first();
        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuperAdmin  $superAdmin
     * @return \Illuminate\Http\Response
     */
    public function viewDashboardFranchiseTransactions(Request $request, GymFranchise $gymFranchise)
    {
        $franchise_details = $gymFranchise;
        $franchise_id = $franchise_details->id;

        if($request->ajax()) {
            $franchise_transactions = Transaction::whereHas('franchise', function($query) use ($franchise_id) { $query->where('id', $franchise_id); })->whereNotNull('product_id')->where('type', 'purchase')->with('user.shaker', 'franchise', 'gym')->get();
            return datatables()->collection($franchise_transactions)->toJson();
        }
        
        $purchase_total = Transaction::whereHas('user.franchise', function($query) use ($franchise_id) { $query->where('id', $franchise_id); })->whereNotNull('product_id')->where('type', 'purchase')->sum('amount');
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.franchise-and-gym-transactions', ['purchase_total' => $purchase_total, 'franchise_details' => $franchise_details, 'franchises' => $franchises, 'gyms' => $gyms]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuperAdmin  $superAdmin
     * @return \Illuminate\Http\Response
     */
    public function viewDashboardProductsFranchise(Request $request, GymFranchise $gymFranchise)
    {
        $franchise_details = $gymFranchise;
        $dispenser_ids = $franchise_details->gyms->pluck('dispensers')->collapse()->pluck('dispensers_id')->all();
        
        if($request->ajax()) {
            $dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();
            return datatables()->collection(Product::whereIn('id', $dispenser_selected_ids)->with('dispenser_product_list')->get())->toJson();
        }

        $product_types = Product::TYPES;
        $product_flavours = Product::FLAVOURS;
        $individual_gym = IndividualGym::all();
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.product-management', [
            'product_types'         => $product_types,
            'product_flavours'      => $product_flavours,
            'individual_gym'        => $individual_gym,
            'franchise_gym_details' => $gymFranchise,
            'franchise_details'     => $gymFranchise,
            'franchises'            => $franchises, 
            'gyms'                  => $gyms,
            'dispenser_ids'         => $dispenser_ids
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuperAdmin  $superAdmin
     * @return \Illuminate\Http\Response
     */
    public function viewDashboardGymTransactions(Request $request, IndividualGym $individualGym)
    {
        $gym_details = $individualGym;
        $gym_id = $individualGym->id;
        if($request->ajax()) {
            $franchise_transactions = Transaction::whereHas('gym', function($query) use ($gym_id) { $query->where('id', $gym_id); })->whereNotNull('product_id')->where('type', 'purchase')->with('user.shaker', 'franchise', 'gym')->get();
            return datatables()->collection($franchise_transactions)->toJson();
        }

        $purchase_total = Transaction::whereHas('user.gym', function($query) use ($gym_id) { $query->where('id', $gym_id); })->whereNotNull('product_id')->where('type', 'purchase')->sum('amount');
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        $franchise_details = IndividualGym::where('id', $individualGym->id)->with('gym_franchise')->first();
        $franchise_details = $franchise_details->gym_franchise;

        $dispensers = Transaction::whereHas('user.gym', function($query) use ($gym_id) { $query->where('id', $gym_id); })->whereNotNull('product_id')->where('type', 'purchase')->with('dispenser')->first();
        
        return view('admin.gym-transactions', [
            'purchase_total'    => $purchase_total, 
            'gym_details'       => $gym_details,
            'franchise_details' => $franchise_details, 
            'franchises'        => $franchises, 
            'gyms'              => $gyms,
            'dispensers'        => $dispensers
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuperAdmin  $superAdmin
     * @return \Illuminate\Http\Response
     */
    public function viewDashboardProductsGym(Request $request, IndividualGym $individualGym)
    {
        $franchise_details = $individualGym->gym_franchise;
        $dispenser_ids = $individualGym->dispensers->pluck('dispensers_id')->all();

        if($request->ajax()) {
            $dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();
            return datatables()->collection(Product::whereIn('id', $dispenser_selected_ids)->with('dispenser_product_list')->get())->toJson();
        }

        $product_types = Product::TYPES;
        $product_flavours = Product::FLAVOURS;
        $individual_gym = IndividualGym::all();
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.product-management', [
            'product_types'         => $product_types,
            'product_flavours'      => $product_flavours,
            'individual_gym'        => $individual_gym,
            'franchise_gym_details' => $franchise_details,
            'franchise_details'     => $franchise_details,
            'franchises'            => $franchises, 
            'gym_details'           => $individualGym,
            'gyms'                  => $gyms,
            'dispenser_ids'         => $dispenser_ids
        ]);
    }
}
