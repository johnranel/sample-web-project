<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Hash;
use App\Http\Requests\{ UserRequest, UserUpdateRequest };
use App\Models\{ User, GymFranchise, IndividualGym };
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use DataTables;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list', ['only' => ['index','show']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['delete', 'destroy']]);
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            return datatables()->collection(User::with('points', 'franchise', 'shaker', 'athlete_profile', 'weight')->whereDoesntHave('roles')->get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.user-management', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function create()
    {
        $franchises = GymFranchise::all();
        $gyms = IndividualGym::get();

        return view('admin.user.create-edit', [
            'franchises' => $franchises,
            'gyms'       => $gyms
        ]);
    }

    public function edit(User $user)
    {
        $franchises = GymFranchise::all();
        $gyms = IndividualGym::get();

        return view('admin.user.create-edit', [
            'user'       => $user,
            'franchises' => $franchises,
            'gyms'       => $gyms
        ]);
    }

    public function store(UserRequest $request)
    {
        $data = $request->all();
        if (isset($request['image'])) {
            $data['image'] = $this->file($request['image']);
        }
        $data['password'] = bcrypt('P@ssword1');
        $user = User::create($data);
        return redirect('/user-management')->with('message', 'Successfully created a new user.');;
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $data = $request->all();
        if (isset($request['image'])) {
            $data['image'] = $this->file($request['image']);
        }
        $user->update($data);
        $user->athlete_profile->update($data);
        $user->weight->update($data);
        return redirect('/user-management')->with('message', 'Successfully updated user details.');;
    }

    private function file($file)
    {
        $file_tmp = $_FILES['image']['tmp_name'];
        $file_name = $_FILES['image']['name'];
        $type = pathinfo($file_name, PATHINFO_EXTENSION);
        $data = file_get_contents($file_tmp);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $base64_image = $base64;
        unset($base64);
        return $base64_image;
    }

    public function delete(User $user)
    {
        $user->delete();
        return response()->json($user);
    }

    public function getUser(User $user)
    {
        return response()->json($user);
    }

    public function checkUsername($username)
    {
        $user = User::where('username', 'LIKE', $username .'%')->orderBy('username', 'desc')->first();
        return response()->json($user);
    }

    public function userList(Request $request)
    {
        $response = [];

        $model = User::query();

        // if (auth()->user()->hasRole('SuperAdmin')) {
        //     $model->whereHas('roles', function($q){
        //             $q->where('name', 'FranchiseAdmin')
        //                 ->orWhere('name', 'SuperAdmin')
        //                 ->orWhere('name', 'GymAdmin');
        //         })->orWhereNotNull('user_type');
        // } else {
            // $model->doesntHave('roles')
            //     ->with('points', 'gym');

            $model->where('user_type', 'superAdmin')->orWhere('user_type', 'franchiseAdmin')->orWhere('user_type', 'gymAdmin')->orWhereNull('user_type')->with('points', 'gym.gym_franchise');
        // }

        $total = $model->count();

        if($request->search_user!=null || $request->search_user!='') {
            $model->where('first_name',$request->search_user);
            $model->orWhere(DB::raw("concat(first_name,' ', last_name)"), 'LIKE', "%".$request->search_user."%");
            $total = $model->count();
        }

        if ($request->has('pagination.perpage')) {
            $limit = $request->get('pagination')['perpage'];
        }

        if ($request->has('pagination.page')) {
            $offset = $request->get('pagination')['page'];
            $offset = $offset - 1;
            $offset = $offset * $limit;
        }

        $pages = $model->count();

        if ($request->first_name != null || $request->first_name != '') {
            $data = $model->skip($offset)->take($limit)->orderBy($request->first_name, 'asc')->get();
        } else {
            $data = $model->skip($offset)->take($limit)->get();
        }

        if ($request->has('sort.sort')) {

            $isSortDecending = true;
            if ($request->get('sort')['sort'] == 'asc') {
                $isSortDecending = false;
            }
            $data = $data->sortBy($request->get('sort')['field'], SORT_REGULAR, $isSortDecending);
        }

        foreach($data as $val){
            $val->fullname = $val->first_name.' '.$val->last_name;
        }

        $data = $this->reIndexCollection($data);

        $meta = [
            'page' => $request->get('pagination')['page'],
            'pages' => $pages,
            'perpage' => $limit,
            'total' => $total,
        ];
        $response['meta'] = $meta;
        $response['data'] = $data;
        $response['userType'] = 0;

        if (auth()->user()->user_type === 'superAdmin' || auth()->user()->user_type === 'franchiseAdmin') {
            $response['userType'] = 1;
        }

        return $response;
    }

    private function reIndexCollection(Collection $collections)
    {
        $response = [];
        foreach ($collections as $collection) {
            $response[] = $collection;
        }

        return $response;
    }

}
