<?php

namespace App\Http\Controllers;

use App\Models\{ GymFranchise, User, Product, IndividualGym, Transaction, Dispenser, Shaker };
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use DataTables;

class PageController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->hasRole('SuperAdmin')) {
            return redirect()->intended('user-transactions');
        } elseif(auth()->user()->hasRole('FranchiseAdmin')) {
            return redirect()->intended('franchise-and-gym-transactions');
        } elseif(auth()->user()->hasRole('GymAdmin')) {
            return redirect()->intended('gym-transactions');
        }
    }

    public function franchiseAndGymTransactions(Request $request)
    {
        if(!auth()->user()->hasRole(['SuperAdmin', 'FranchiseAdmin'])) {
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        }

        $franchise_details = User::where('id', auth()->user()->id)->with('franchise')->first();
        $franchise_id = $franchise_details->franchise->id;

        if($request->ajax()) {
            $franchise_transactions = Transaction::whereHas('franchise', function($query) use ($franchise_id) { $query->where('id', $franchise_id); })->whereNotNull('product_id')->where('type', 'purchase')->with('user.shaker', 'franchise', 'gym')->get();
            return datatables()->collection($franchise_transactions)->toJson();
        }
        
        $purchase_total = Transaction::whereHas('user.franchise', function($query) use ($franchise_id) { $query->where('id', $franchise_id); })->whereNotNull('product_id')->where('type', 'purchase')->sum('amount');

        return view('admin.franchise-and-gym-transactions', [
            'purchase_total'    => $purchase_total, 
            'franchise_details' => $franchise_details->franchise
        ]);
    }

    public function gymTransactions(Request $request)
    {
        if(!auth()->user()->hasRole(['SuperAdmin', 'GymAdmin'])) {
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        }

        $gym_details = User::where('id', auth()->user()->id)->with('gym.dispensers.dispenser_product_list.products', 'franchise')->first();
        $franchise_details = $gym_details->franchise;
        $gym_id = $gym_details->gym->id;

        if($request->ajax()) {
            $franchise_transactions = Transaction::whereHas('gym', function($query) use ($gym_id) { $query->where('id', $gym_id); })->whereNotNull('product_id')->where('type', 'purchase')->with('user.shaker', 'franchise', 'gym', 'dispenser')->get();
            return datatables()->collection($franchise_transactions)->toJson();
        }

        $purchase_total = Transaction::whereHas('user.gym', function($query) use ($gym_id) { $query->where('id', $gym_id); })->whereNotNull('product_id')->where('type', 'purchase')->sum('amount');
        $dispensers = Transaction::whereHas('user.gym', function($query) use ($gym_id) { $query->where('id', $gym_id); })->whereNotNull('product_id')->where('type', 'purchase')->get();

        return view('admin.gym-transactions', [
            'purchase_total'    => $purchase_total, 
            'gym_details'       => $gym_details->gym, 
            'franchise_details' => $franchise_details,
            'dispensers'        => $dispensers
        ]);
    }

    public function userTransactions(Request $request)
    {
        if($request->ajax()) {
            return datatables()->collection(Transaction::with('user.shaker', 'franchise', 'gym', 'product')->get())->toJson();
        }

        $top_up_total = Transaction::where('type', 'top-up')->sum('amount');
        $purchase_total = Transaction::where('type', 'purchase')->sum('amount');
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.user-transaction', [
            'top_up_total'      => $top_up_total, 
            'purchase_total'    => $purchase_total, 
            'franchises'        => $franchises, 
            'gyms'              => $gyms
        ]);
    }

    public function gymDispenser(Request $request)
    {
        if(!auth()->user()->hasRole(['SuperAdmin'])) {
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        }

        if($request->ajax()) {
            return datatables()->collection(Dispenser::with('gym.gym_franchise')->get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.gym-dispenser', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function shakerManagement(Request $request)
    {
        if(!auth()->user()->hasRole(['SuperAdmin'])) {
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        }

        if($request->ajax()) {
            return datatables()->collection(Shaker::with('user')->get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.shaker-management', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function superAdminSettings()
    {
        return view('admin.superadmin-settings');
    }
}
