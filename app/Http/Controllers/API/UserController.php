<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\{ Hash, Password };
use Illuminate\Support\Str;
use App\Models\{ User, AthleteProfile, Weight };
use App\Http\Requests\API\{ UserUpdateRequest, UserRequest };
use Carbon\Carbon;

class UserController extends Controller
{
    public function update(UserUpdateRequest $request)
    {
        $data = $request->except(['current_weight', 'level_of_fitness', 'gym_attendance']);

        auth()->user()->update($data);

        Weight::where('user_id', auth()->user()->id)->update([
            'weight' => $request->current_weight
        ]);

        AthleteProfile::where('user_id', auth()->user()->id)->update([
            'level_of_fitness'  => $request->level_of_fitness,
            'gym_attendance'    => $request->gym_attendance
        ]);

        $user = $this->user_data_fetch(auth()->user()->id);

        return response()->json($user, 200);
    }

    public function store(UserRequest $request)
    {
        $data = $request->except(['current_weight', 'level_of_fitness', 'gym_attendance']);
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);

        Weight::create([
            'user_id'   => $user->id,
            'weight'    => $request->current_weight
        ]);

        AthleteProfile::create([
            'user_id'           => $user->id,
            'level_of_fitness'  => $request->level_of_fitness,
            'gym_attendance'    => $request->gym_attendance
        ]);
        
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
        ], 200);
    }

    public function updateAthleteProfile(Request $request)
    {
        $athlete_data = AthleteProfile::where('user_id', $request->user_id)->first();

        if(auth()->user()->id == $request->user_id) {
            if($athlete_data != null) {
                $athlete_data->update([
                    'priorities'        => $request->priorities,
                    'type_of_training'  => $request->type_of_training
                ]);
            } else {
                AthleteProfile::create([
                    'priorities'        => $request->priorities,
                    'type_of_training'  => $request->type_of_training,
                    'user_id'           => $request->user_id
                ]);
            }

            $user = $this->user_data_fetch($request->user_id);

            return response()->json([
                'athlete_profile' => $athlete_data,
                'user'            => $user
            ], 200);
        }

        return response()->json([
            'athlete_profile' => null,
            'user'            => null
        ], 500);
    }

    public function updateWeight(Request $request)
    {
        $weight_data = Weight::where('user_id', $request->user_id)->first();

        if(auth()->user()->id == $request->user_id) {
            if($weight_data != null) {
                $weight_data->update([
                    'user_id'   => $request->user_id,
                    'weight'    => $request->current_weight
                ]);
            } else {
                Weight::create([
                    'user_id'   => $request->user_id,
                    'weight'    => $request->current_weight
                ]);
            }

            $user = $this->user_data_fetch($request->user_id);

            return response()->json([
                'weight'    => $weight_data,
                'user'      => $user
            ], 200);
        }

        return response()->json([
            'weight' => null,
            'user'   => null
        ], 500);
    }

    public function forgotPassword(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return response()->json([
            'status' => __($status)
        ]);
    }

    public function resetPasswordToken($token, $email)
    {
        return redirect()->away('supplementstation://reset-password?token='.$token.'&email='.$email);
    }

    public function resetPassword(Request $request)
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ]);
     
                $user->save();
     
                event(new PasswordReset($user));
            }
        );

        return response()->json([
            'status' => __($status)
        ]);
    }

    private function user_data_fetch($user_id)
    {
        $user = User::where('id', $user_id)
                    ->with(['weight'=> function($query) {
                        $query->orderBy("created_at", "desc")
                            ->first();
                    }, 'athlete_profile'=> function($query) {
                        $query->first();
                    }, 'franchise', 'points', 'gym.dispensers', 'auto_billing'])->firstOrFail();

        return $user;
    }
}