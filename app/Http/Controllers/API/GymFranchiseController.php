<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GymFranchise;

class GymFranchiseController extends Controller
{
    public function getGymFranchise()
    {
        $gym_franchise = GymFranchise::with('gyms')->get();
        
        return response()->json([
            'gym_franchise' => $gym_franchise
        ]);
    }
}
