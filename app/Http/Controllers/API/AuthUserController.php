<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\LoginRequest;
use App\Models\{ User, Log };
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class AuthUserController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Invalid login details'
            ], 401);
        }

        $user = User::where('email', $request['email'])
                    ->with(['weight'=> function($query) {
                        $query->orderBy("created_at", "desc")
                            ->first();
                    }, 'athlete_profile'=> function($query) {
                        $query->first();
                    }, 'franchise', 'points', 'gym.dispensers', 'auto_billing'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'user' => $user
        ]);
    }

    public function logout()
    {
        Auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'Logged out',
        ]);
    }

    public function authCheck()
    {
        if(Auth()->user()) {
            return response()->json([
                'user' => Auth()->user(),
            ]);
        }

        return response()->json([
            'user' => '',
        ]);
    }
}
