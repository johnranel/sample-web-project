<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\{ User, Transaction, TopUpPoint, Setting, Shaker, AutoBilling };

class TransactionController extends Controller
{
    public function pay(Request $request)
    {
        $stripeCustomer = auth()->user()->createOrGetStripeCustomer();

        $payment = auth()->user()->pay(
            $request->get('amount'), ['setup_future_usage' => 'on_session', 'payment_method_types' => ['card']]
        );

        return response()->json([
            "customer_id" => $stripeCustomer->id, 
            "client_secret" => $payment->client_secret,
        ]);
    }

    public function linkPaymentCard()
    {
        $stripeCustomer = auth()->user()->createOrGetStripeCustomer();
        $setupIntent = auth()->user()->createSetupIntent(['customer' => auth()->user()->stripe_id, 'payment_method_types' => ["card"]]);

        if (auth()->user()->hasStripeId()) {
            auth()->user()->syncStripeCustomerDetails();
            auth()->user()->updateDefaultPaymentMethodFromStripe();
        }

        return response()->json([
            "customer_id" => $stripeCustomer->id, 
            "setup_intent_id" => $setupIntent->client_secret,
        ]);
    }

    public function autoBilling(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;

        $autoBilling = AutoBilling::where('user_id', auth()->user()->id)->first();

        if($autoBilling) {
            $autoBilling->update($data);
        } else {
            $autoBilling = AutoBilling::create($data);
        }


        $user = $this->user_data_fetch(auth()->user()->id);

        return response()->json([
            "user"         => $user,
            "auto_billing" => $autoBilling
        ]);
    }

    public function store(Request $request)
    {
        $transaction_request = $request->all();

        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

        try {
            $checkTransactions = Transaction::where("stripe_client_secret", $request->stripe_client_secret)->first();
            if(empty($checkTransactions)) {
                $paymentIntent = substr_replace($request->stripe_client_secret,"",27);
                $paymentDetails = $stripe->paymentIntents->retrieve($paymentIntent);
                if($paymentDetails->status == "succeeded") {
                    $top_up_settings = Setting::where('topup_amount', $request->amount)->first();
                    $transaction_request['user_id'] = auth()->user()->id;
                    $transaction_request['franchise_id'] = auth()->user()->franchise->id;
                    $transaction_request['gym_id'] = auth()->user()->gym->id;
                    if($request->type == "purchase") {
                        $transaction_request['points'] = $request->amount;
                        $transaction_request['amount'] = $request->amount_aud;
                    } else {
                        $transaction_request['points'] = $top_up_settings->total_points;
                    }

                    $trasaction_data = Transaction::create($transaction_request);
                    
                    $points_data = TopUpPoint::where('user_id', auth()->user()->id)->first();
                    $points_request = $request->except(['type', 'description', 'stripe_client_secret']);
                    $points_request['user_id'] = auth()->user()->id;
                    if($request->type != "purchase") {
                        $points_request['points'] = $top_up_settings->total_points;
                    }

                    if(empty($points_data)) {
                        $points_data = TopUpPoint::create($points_request);
                    } else {
                        if($request->type == "purchase") {
                            $points_data['points'] -= $request->amount;
                            $points_data['amount'] -= $request->amount_aud;
                        } else {
                            $points_data['points'] += $top_up_settings->total_points;
                            $points_data['amount'] += $top_up_settings->topup_amount;
                        }
                        
                        $points_data->save();
                    }

                    if(isset($request->shaker_id)) {
                        $shaker = Shaker::where('shaker_id', $request->shaker_id)->first();
                        $shaker['user_id'] = auth()->user()->id;
                        $shaker->save();
                    }

                    return response()->json([
                        'trasaction_data' => $trasaction_data,
                        'points_data'     => $points_data
                    ]);
                }
                return response()->json(["message" => "Stripe client is incomplete."], 500);
            }
            return response()->json(["message" => "Stripe client secret already used."], 500);
        } catch (Exception $e) {
            return response()->json(["message" => "Stripe client secret not found."], 500);
        }
    }

    public function getPoints()
    {
        return response()->json([
            'points_data' => auth()->user()->points
        ]);
    }

    public function getTransactions($offsetData, $transactionType)
    {
        $transaction_data = Transaction::with('product.media')->where('user_id', auth()->user()->id)->where('type', $transactionType);

        if($transactionType != "top-up") {
            $transaction_data = $transaction_data->whereNotNull('product_id');
        }
        
        $transaction_data = $transaction_data
                                ->offset($offsetData)
                                ->orderBy('created_at', 'desc')
                                ->limit(10)
                                ->get();

        return response()->json([
            'transaction_data' => $transaction_data
        ]);
    }

    private function user_data_fetch($user_id)
    {
        $user = User::where('id', $user_id)
                    ->with(['weight'=> function($query) {
                        $query->orderBy("created_at", "desc")
                            ->first();
                    }, 'athlete_profile'=> function($query) {
                        $query->first();
                    }, 'franchise', 'points', 'gym.dispensers', 'auto_billing'])->firstOrFail();

        return $user;
    }
}
