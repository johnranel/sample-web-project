<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ Product, DispenserProductList };
use Auth;

class ProductController extends Controller
{
    public function getProducts()
    {
        $dispenser_ids = Auth::user()->gym->dispensers->pluck('dispensers_id')->all();
        //$dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();

        $product_list = Product::with('media', 'dispenser_product_list')
            //->whereIn('id', $dispenser_selected_ids)
            ->get();

        if($product_list->isEmpty()) {
        	return response()->json([
                'message' => 'No product available.'
            ], 401);
        }

        return response()->json([
            'product_list'  => $product_list,
            'dispenser_ids' => $dispenser_ids
        ]);
    }

    public function getLimitedProducts($offsetData)
    {
        $dispenser_ids = Auth::user()->gym->dispensers->pluck('dispensers_id')->all();
        //$dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();

        $product_list = Product::with('media', 'dispenser_product_list')
            //->whereIn('id', $dispenser_selected_ids)
            ->offset($offsetData)
            ->orderBy('created_at', 'desc')
            ->limit(6)
            ->groupBy('id')
            ->get();
        
        return response()->json([
            'product_list'  => $product_list,
            'dispenser_ids' => $dispenser_ids
        ]);
    }

    public function getLimitedPromotedProducts($offsetData)
    {
        $dispenser_ids = Auth::user()->gym->dispensers->pluck('dispensers_id')->all();
        //$dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();

        $discounted_product_list = Product::with('media', 'dispenser_product_list')
            //->whereIn('id', $dispenser_selected_ids)
            ->where('promoted_product', true)
            ->offset($offsetData)
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();

        return response()->json([
            'discounted_product_list' => $discounted_product_list,
            'dispenser_ids'           => $dispenser_ids
        ]);
    }

    public function getFilteredProducts(Request $request)
    {
        $dispenser_ids = Auth::user()->gym->dispensers->pluck('dispensers_id')->all();
        //$dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();

        $filtered_product_list = Product::with('media', 'dispenser_product_list')
            ->where(function ($query) use ($request) {
                if(!empty($request->flavours)) {
                    $query->whereIn('flavour', $request->flavours);
                }
                if(!empty($request->types)) {
                    $query->whereIn('type', $request->types);
                }
                if(!empty($request->brands)) {
                    $query->whereIn('manufacturer_name', $request->brands);
                }
                if(!empty($request->keyword)) {
                    $query->where('description', $request->keyword);
                }
            })
            //->whereIn('id', $dispenser_selected_ids)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json([
            'filtered_product_list' => $filtered_product_list,
            'dispenser_ids'         => $dispenser_ids
        ]);
    }

    public function getProductBrandsTypesAndFlavours()
    {
        $dispenser_ids = Auth::user()->gym->dispensers->pluck('dispensers_id')->all();
        //$dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();
    
        $product_types = Product::TYPES;
        $product_flavours = Product::FLAVOURS;
        $product_brands = Product::whereNotNull('manufacturer_name')
                            ->groupBy('manufacturer_name')
                            ->get('manufacturer_name');
                            //whereIn('id', $dispenser_selected_ids)

        return response()->json([
            'product_types'    => $product_types,
            'product_flavours' => $product_flavours,
            'product_brands'   => $product_brands,
            'dispenser_ids'    => $dispenser_ids
        ]);
    }
}
