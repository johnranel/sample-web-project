<?php

namespace App\Http\Controllers;

use App\Models\{ Shaker, GymFranchise, IndividualGym };
use Illuminate\Http\Request;

class ShakerController extends Controller
{
    public function create()
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.shaker.create-edit', ['franchises' => $franchises, 'gyms' => $gyms]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $shaker = Shaker::create($data);

        if($request->ajax()) {
            return response()->json($shaker);
        }

        return redirect('/shaker-management')->with('message', 'Successfully created a new shaker.');
    }

    public function edit(Shaker $shaker)
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.shaker.create-edit', ['franchises' => $franchises, 'gyms' => $gyms, 'shaker' => $shaker]);
    }

    public function update(Shaker $shaker, Request $request)
    {
        $data = $request->all();
        $shaker->update($data);

        if($request->ajax()) {
            return response()->json($shaker);
        }

        return redirect('/shaker-management')->with('message', 'Successfully updated shaker details.');
    }

    public function delete(Shaker $shaker)
    {
        $shaker->delete();
        return response()->json($shaker);
    }
}
