<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
use App\Http\Requests\ContactRequest;

class HomeController extends Controller
{
    /**
     * Show the home page.
     */
    public function index()
    {
        return view('home.index');
    }

    /**
     * Send contact form.
     */
    public function submitContact(ContactRequest $request)
    {
        Mail::to(env('OWNER_MAIL'))->send(new Contact($request));
        
        return view('home.index');
    }
}