<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Setting, GymFranchise, IndividualGym };
use DataTables;

class SettingsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:settings-list', ['only' => ['index','show']]);
        $this->middleware('permission:settings-create', ['only' => ['create','store']]);
        $this->middleware('permission:settings-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:settings-delete', ['only' => ['delete', 'destroy']]);
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            return datatables()->collection(Setting::get())->toJson();
        }

        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.settings-management', [
            'franchises'    => $franchises, 
            'gyms'          => $gyms
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if($data['bonus'] == null) {
            $data['bonus'] = 0;
        }
        $settingsData = Setting::where('topup_amount', $request->topup_amount)->first();
        if(empty($settingsData)) {
            $settings = Setting::create($data);
        } else {
            $settingsData->update($data);
            $settings = $settingsData;
        }
        return response()->json($settings);
    }

    public function update(Request $request, Setting $settings)
    {
        $data = $request->all();
        if($data['bonus'] == null) {
            $data['bonus'] = 0;
        }
        $settings->update($data);
        return redirect('/settings-management');
    }

    public function delete(Setting $settings)
    {
        $settings->delete();
        return response()->json($settings);
    }
}
