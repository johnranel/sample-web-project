<?php

namespace App\Http\Controllers;

use App\Models\{ Dispenser, GymFranchise, IndividualGym, Product, DispenserProductList };
use Illuminate\Http\Request;

class DispenserController extends Controller
{
    public function create(Dispenser $dispenser)
    {
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();
        $products = Product::get();
        $selected_products = DispenserProductList::where('dispensers_id', $dispenser->dispensers_id)->get();

        return view('admin.dispenser.create-edit', [
            'franchises'        => $franchises, 
            'gyms'              => $gyms, 
            'dispenser'         => $dispenser, 
            'products'          => $products, 
            'selected_products' => $selected_products
        ]);
    }

    public function store(Dispenser $dispenser, Request $request)
    {
        DispenserProductList::where('dispensers_id', $dispenser->dispensers_id)->delete();

        if(isset($request->product_id)) {
            foreach($request->product_id as $key => $id) {
                DispenserProductList::create(['dispensers_id' => $dispenser->dispensers_id, 'product_id' => $id]);
            }
        }

        return redirect('/dispenser-management')->with('message', 'Successfully assigned the products on dispenser.');
    }
}
