<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\{ Product, Media, IndividualGym, User, GymFranchise, DispenserProductList };
use App\Http\Requests\{ ProductRequest };
use File;
use Auth;
use Carbon\Carbon;
use DB;
use Storage;
use Illuminate\Database\Eloquent\Collection;
use DataTables;

class ProductController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:product-list', ['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['delete', 'destroy']]);
    }

    public function index(Request $request)
    {
        $user_details = User::where('id', auth()->user()->id)->first();
        $dispenser_ids = null;
        $franchise_gym_details = null;
        if(auth()->user()->hasRole('FranchiseAdmin')) {
            $franchise_gym_details = $user_details->franchise;
            $dispenser_ids = $user_details->franchise->gyms->pluck('dispensers')->collapse()->pluck('dispensers_id')->all();
        } elseif(auth()->user()->hasRole('GymAdmin')) {
            $franchise_gym_details = $user_details->franchise;
            $dispenser_ids = $user_details->gym->dispensers->pluck('dispensers_id')->all();
        }

        if($request->ajax()) {
            if(auth()->user()->hasRole('FranchiseAdmin') || auth()->user()->hasRole('GymAdmin')) {
                $dispenser_selected_ids = DispenserProductList::whereIn('dispensers_id', $dispenser_ids)->pluck('product_id')->toArray();
                return datatables()->collection(Product::whereIn('id', $dispenser_selected_ids)->with('dispenser_product_list')->get())->toJson();
            } else {
                return datatables()->collection(Product::get())->toJson();
            }
        }

        $product_types = Product::TYPES;
        $product_flavours = Product::FLAVOURS;
        $individual_gym = IndividualGym::all();
        $franchises = GymFranchise::get();
        $gyms = IndividualGym::get();

        return view('admin.product-management', [
            'product_types'         => $product_types,
            'product_flavours'      => $product_flavours,
            'individual_gym'        => $individual_gym,
            'franchise_gym_details' => $franchise_gym_details,
            'franchises'            => $franchises,
            'gyms'                  => $gyms,
            'dispenser_ids'         => $dispenser_ids
        ]);
    }

    public function create()
    {
        $product_types = Product::TYPES;
        $product_flavours = Product::FLAVOURS;
        $franchises = GymFranchise::all();
        $gyms = IndividualGym::get();

        return view('admin.product.create-edit', [
            'product_types'     => $product_types,
            'product_flavours'  => $product_flavours,
            'franchises'        => $franchises,
            'gyms'              => $gyms
        ]);
    }

    public function edit(Product $product)
    {
        $product_details = Product::where('id', $product->id)->with('media')->first();
        $product_types = Product::TYPES;
        $product_flavours = Product::FLAVOURS;
        $franchises = GymFranchise::all();
        $gyms = IndividualGym::get();

        return view('admin.product.create-edit', [
            'product_details'   => $product_details,
            'product_types'     => $product_types,
            'product_flavours'  => $product_flavours,
            'franchises'        => $franchises,
            'gyms'              => $gyms
        ]);
    }

    public function store(ProductRequest $request)
    {
        $images = $request->images;
        $nutrients = $request->nutrients;
        $data = $request->except(['images', 'nutrients']);
        $product = Product::create($data);
        $this->moveImage($images, $product);
        $this->moveImage($nutrients, $product, 'nutrients');

        return redirect('/product-management')->with('message', 'Successfully created a new product.');
    }

    public function delete(Product $product)
    {
        $product->delete();
        return response()->json($product);
    }

    public function getProduct(Product $product)
    {
        $product = Product::with('media')
            ->where('id', $product->id)
            ->first();

        return $product;
    }

    public function update(ProductRequest $request, Product $product)
    {
        $images = $request->images;
        $nutrients = $request->nutrients;
        $data = $request->except(['images', 'nutrients']);
        if(!isset($request->promoted_product)) {
            $data['promoted_product'] = null;
        }
        $product->update($data);
        $this->moveImage($images, $product);
        $this->moveImage($nutrients, $product, 'nutrients');

        return redirect('/product-management')->with('message', 'Successfully updated product details.');
    }

    public function checkProduct($product_no)
    {
        $product = Product::where('product_no', 'LIKE', $product_no .'%')->orderBy('product_no', 'desc')->first();
        return response()->json($product);
    }

    public function productList(Request $request, $limit = 10, $offset = 0)
    {
        $response = [];

        $model = Product::query();

        $total = $model->count();

        if($request->search_product!=null || $request->search_product!='') {
            $model->orWhere('name','like','%'.$request->search_product.'%');
            $total = $model->count();
        }

        if($request->type!=null || $request->type!=''){
            $model->where('type',$request->type);
            $total = $model->count();
        }

        if ($request->has('pagination.perpage')) {
            $limit = $request->get('pagination')['perpage'];
        }

        if ($request->has('pagination.page')) {
            $offset = $request->get('pagination')['page'];
            $offset = $offset - 1;
            $offset = $offset * $limit;
        }

        $pages = $model->count();

        if ($request->name != null || $request->name != '') {
            $data = $model->with('media')->skip($offset)->take($limit)->orderBy($request->name, 'asc')->get();
        } else {
            $data = $model->with('media')->skip($offset)->take($limit)->get();
        }

        if ($request->has('sort.sort')) {

            $isSortDecending = true;
            if ($request->get('sort')['sort'] == 'asc') {
                $isSortDecending = false;
            }
            $data = $data->sortBy($request->get('sort')['field'], SORT_REGULAR, $isSortDecending);
        }

        $data = $this->reIndexCollection($data);

        $meta = [
            'page' => $request->get('pagination')['page'],
            'pages' => $pages,
            'perpage' => $limit,
            'total' => $total,
        ];
        $response['meta'] = $meta;
        $response['data'] = $data;
        $response['userType'] = 0;

        if (auth()->user()->user_type === 'superAdmin' || auth()->user()->user_type === 'FranchiseAdmin') {
            $response['userType'] = 1;
        }

        return $response;
    }

    private function reIndexCollection(Collection $collections)
    {
        $response = [];
        foreach ($collections as $collection) {
            $response[] = $collection;
        }

        return $response;
    }

    public function uploadImages(Request $request)
    {
        $image_name = Str::random(10);
        $extension = $request->file->getClientOriginalExtension();

        $file_path = public_path('uploads/temp/images');

        if (!File::exists($file_path)) {
            $file_dir = File::makeDirectory($file_path, 0777, true, true);
        }

        $request->file->move($file_path, $image_name.".".$extension);

        return "uploads/temp/images/".$image_name.".".$extension;
    }

    public function deleteImage(Request $request)
    {
        if(File::exists(public_path($request->file_name))) {
            File::delete(public_path($request->file_name));
        }

        return response()->json('Successfully deleted uploaded image.');
    }

    private function moveImage($images, $product, $type = 'image')
    {
        if(!File::exists(public_path('uploads/images/'.$product->id))) {
            File::makeDirectory('uploads/images/'.$product->id, 0777, true, true);
        }

        if(!empty($images)) {
            foreach($images as $image) {
                $imageName = Str::random(10).".png";

                if(File::exists(public_path($image))) {
                    File::move(public_path($image), public_path('uploads/images/'.$product->id.'/'.$imageName));
                }

                Media::create([
                    'path'       => url('/') . '/uploads/images/'.$product->id.'/'.$imageName,
                    'type'       => $type,
                    'product_id' => $product->id
                ]);
            }
        }

        return true;
    }

    public function deleteMedia(Media $media)
    {
        $image_path = str_replace(url('/')."/", "", $media->path);
        $media->delete();

        if(File::exists(public_path($image_path))) {
            File::delete(public_path($image_path));
        }

        return $image_path;
    }
}
