<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Carbon;
use App\Models\{ User, AutoBilling };

class UserCharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Charges users with their preferred auto billing amount and timing.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $auto_billing = AutoBilling::get();
       
        foreach($auto_billing as $key => $auto_bill) {
            $dt = Carbon::createFromDate($auto_bill->updated_at);
            if($auto_bill->enable_billing) {
                if($auto_bill->timing == "weekly" && $dt->isLastWeek()) {
                    $user = User::where("id", $auto_bill->user_id)->first();
                    $paymentMethod = $user->paymentMethods();
                    $user->charge($auto_bill->amount * 100, $paymentMethod[0]->id);
                }

                if($auto_bill->timing == "monthly" && $dt->isLastMonth()) {
                    $user = User::where("id", $auto_bill->user_id)->first();
                    $paymentMethod = $user->paymentMethods();
                    $user->charge($auto_bill->amount * 100, $paymentMethod[0]->id);
                }
            }
        }
    }
}
