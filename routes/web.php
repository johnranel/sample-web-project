<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{ PageController, AuthController, CategoryController };

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('contact', [App\Http\Controllers\HomeController::class, 'submitContact'])->name('contact');

Route::get('login', [App\Http\Controllers\AuthController::class, 'getLogin'])->name('getlogin');
Route::post('login', [App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::get('logout', [App\Http\Controllers\AuthController::class, 'logout'])->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('list/user', [App\Http\Controllers\UserController::class, 'userList'])->name('list.user');
        Route::get('list/product', [App\Http\Controllers\ProductController::class, 'productList'])->name('list.product');
        Route::get('list/gymFranchise', [App\Http\Controllers\GymFranchiseController::class, 'gymFranchiseList'])->name('list.gym.franchise');
        Route::get('list/individual-gym', [App\Http\Controllers\IndividualGymController::class, 'individualGymList'])->name('list.indiviudal.gym');

        Route::group(['prefix' => 'log'], function () {
            Route::get('list/product', [App\Http\Controllers\ProductController::class, 'productLogList'])->name('log.list.product');
            Route::get('list/user', [App\Http\Controllers\UserController::class, 'userLogList'])->name('log.list.user');
        });
    });

    Route::get('home', [App\Http\Controllers\PageController::class, 'index'])->name('home');
    //Dashboards
    Route::get('user-management', [App\Http\Controllers\UserController::class, 'index'])->name('user.management');
    Route::get('admin-management', [App\Http\Controllers\SuperAdminController::class, 'index'])->name('admin.management');
    Route::get('product-management', [App\Http\Controllers\ProductController::class, 'index'])->name('product.management');
    Route::get('franchise-management', [App\Http\Controllers\GymFranchiseController::class, 'index'])->name('franchise.management');
    Route::get('gym-management', [App\Http\Controllers\IndividualGymController::class, 'index'])->name('gym.management');
    Route::get('franchise-and-gym-transactions', [App\Http\Controllers\PageController::class, 'franchiseAndGymTransactions'])->name('franchise.and.gym.transactions');
    Route::get('gym-transactions', [App\Http\Controllers\PageController::class, 'gymTransactions'])->name('gym.transactions');
    Route::get('user-transactions', [App\Http\Controllers\PageController::class, 'userTransactions'])->name('user.transactions');
    Route::get('settings-management', [App\Http\Controllers\SettingsController::class, 'index'])->name('settings');
    Route::get('dispenser-management', [App\Http\Controllers\PageController::class, 'gymDispenser'])->name('dispenser.management');
    Route::get('shaker-management', [App\Http\Controllers\PageController::class, 'shakerManagement'])->name('shaker.management');

    //Admin
    Route::get('admin/create', [App\Http\Controllers\SuperAdminController::class, 'create'])->name('create.admin');
    Route::post('admin/store', [App\Http\Controllers\SuperAdminController::class, 'store'])->name('store.admin');
    Route::get('franchise/gyms/fetch/{gymFranchise}', [App\Http\Controllers\GymFranchiseController::class, 'franchiseGymsFetch'])->name('franchise.gym.fetch');
    Route::get('admin/edit/{user}', [App\Http\Controllers\SuperAdminController::class, 'edit'])->name('edit.admin');
    Route::post('admin/update/{user}', [App\Http\Controllers\SuperAdminController::class, 'update'])->name('update.admin');

    //Dispenser
    Route::get('dispenser/assign/products/{dispenser}', [App\Http\Controllers\DispenserController::class, 'create'])->name('assign.dispenser.products');
    Route::post('dispenser/assign/products/{dispenser}', [App\Http\Controllers\DispenserController::class, 'store'])->name('update.dispenser.products');

    //User
    Route::get('user/create', [App\Http\Controllers\UserController::class, 'create'])->name('create.user');
    Route::post('user/store', [App\Http\Controllers\UserController::class, 'store'])->name('store.user');
    Route::get('user/edit/{user}', [App\Http\Controllers\UserController::class, 'edit'])->name('edit.user');
    Route::post('user/update/{user}', [App\Http\Controllers\UserController::class, 'update'])->name('update.user');
    Route::get('user/get/{user}', [App\Http\Controllers\UserController::class, 'getUser'])->name('get.user');
    Route::post('user/delete/{user}', [App\Http\Controllers\UserController::class, 'delete'])->name('delete.user');
    Route::get('username/check/{username}', [App\Http\Controllers\UserController::class, 'checkUsername'])->name('username.check');

    //Shaker
    Route::get('shaker/create', [App\Http\Controllers\ShakerController::class, 'create'])->name('create.shaker');
    Route::post('shaker/store', [App\Http\Controllers\ShakerController::class, 'store'])->name('store.shaker');
    Route::get('shaker/edit/{shaker}', [App\Http\Controllers\ShakerController::class, 'edit'])->name('edit.shaker');
    Route::post('shaker/update/{shaker}', [App\Http\Controllers\ShakerController::class, 'update'])->name('update.shaker');
    Route::post('shaker/delete/{shaker}', [App\Http\Controllers\ShakerController::class, 'delete'])->name('delete.shaker');

    //SuperAdmin
    Route::post('super-admin/add', [App\Http\Controllers\SuperAdminController::class, 'add'])->name('add.super-admin');
    //Franchise Dashboard View
    Route::get('dashboard/view/admin/franchise/transactions/{gymFranchise}', [App\Http\Controllers\SuperAdminController::class, 'viewDashboardFranchiseTransactions'])->name('dashboard.view.franchise.transactions');
    Route::get('dashboard/view/admin/franchise/products/{gymFranchise}', [App\Http\Controllers\SuperAdminController::class, 'viewDashboardProductsFranchise'])->name('dashboard.view.franchise.products');
    //Gym Dashboard View
    Route::get('dashboard/view/admin/gym/transactions/{individualGym}', [App\Http\Controllers\SuperAdminController::class, 'viewDashboardGymTransactions'])->name('dashboard.view.gym.transactions');
    Route::get('dashboard/view/admin/gym/products/{individualGym}', [App\Http\Controllers\SuperAdminController::class, 'viewDashboardProductsGym'])->name('dashboard.view.gym.products');
    
    //Product
    Route::get('product/create', [App\Http\Controllers\ProductController::class, 'create'])->name('create.product');
    Route::post('product/store', [App\Http\Controllers\ProductController::class, 'store'])->name('store.product');
    Route::get('product/edit/{product}', [App\Http\Controllers\ProductController::class, 'edit'])->name('edit.product');
    Route::post('product/update/{product}', [App\Http\Controllers\ProductController::class, 'update'])->name('update.product');
    Route::get('product/get/{product}', [App\Http\Controllers\ProductController::class, 'getProduct'])->name('get.product');
    Route::post('product/delete/{product}', [App\Http\Controllers\ProductController::class, 'delete'])->name('delete.product');
    Route::get('product/check/{product}', [App\Http\Controllers\ProductController::class, 'checkProduct'])->name('product.check');

    //Product Media
    Route::post('upload/images', [App\Http\Controllers\ProductController::class, 'uploadImages'])->name('upload.images');
    Route::post('delete/image', [App\Http\Controllers\ProductController::class, 'deleteImage'])->name('delete.image');
    Route::post('delete/media/{media}', [App\Http\Controllers\ProductController::class, 'deleteMedia'])->name('delete.media');

    //Roles
    Route::resource('roles', App\Http\Controllers\RoleController::class);

    //Franchise
    Route::get('franchise/create', [App\Http\Controllers\GymFranchiseController::class, 'create'])->name('create.franchise');
    Route::post('franchise/store', [App\Http\Controllers\GymFranchiseController::class, 'store'])->name('store.franchise');
    Route::get('franchise/edit/{gymfranchise}', [App\Http\Controllers\GymFranchiseController::class, 'edit'])->name('edit.franchise');
    Route::post('franchise/update/{gymfranchise}', [App\Http\Controllers\GymFranchiseController::class, 'update'])->name('update.franchise');
    Route::get('franhise/get/{gymfranchise}', [App\Http\Controllers\GymFranchiseController::class, 'getGymFranchise'])->name('get.franchise');
    Route::post('franchise/delete/{gymfranchise}', [App\Http\Controllers\GymFranchiseController::class, 'delete'])->name('delete.franchise');

    //Gym
    Route::get('gym/create', [App\Http\Controllers\IndividualGymController::class, 'create'])->name('create.gym');
    Route::post('gym/store', [App\Http\Controllers\IndividualGymController::class, 'store'])->name('store.gym');
    Route::get('gym/edit/{individualGym}', [App\Http\Controllers\IndividualGymController::class, 'edit'])->name('edit.gym');
    Route::post('gym/update/{individualGym}', [App\Http\Controllers\IndividualGymController::class, 'update'])->name('update.gym');
    Route::get('gym/get/{individualGym}', [App\Http\Controllers\IndividualGymController::class, 'getIndividualGym'])->name('get.gym');
    Route::post('gym/delete/{individualGym}', [App\Http\Controllers\IndividualGymController::class, 'delete'])->name('delete.gym');
    Route::get('gym/dispenser/fetch/{individualGym}', [App\Http\Controllers\IndividualGymController::class, 'gymDispenserFetch'])->name('gym.dispenser.fetch');

    //Settings
    Route::post('settings/store', [App\Http\Controllers\SettingsController::class, 'store'])->name('store.settings');
    Route::post('settings/update/{settings}', [App\Http\Controllers\SettingsController::class, 'update'])->name('update.settings');
    Route::post('settings/delete/{settings}', [App\Http\Controllers\SettingsController::class, 'delete'])->name('delete.settings');
});
