<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{API\UserController, API\AuthUserController, API\ProductController, API\GymFranchiseController, API\TransactionController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [AuthUserController::class, 'logout']);

    Route::put('/user/update', [UserController::class, 'update']);
    Route::post('/user/athlete-profile/update', [UserController::class, 'updateAthleteProfile']);
    Route::post('/user/weight/update', [UserController::class, 'updateWeight']);
    Route::get('/user/points/get', [TransactionController::class, 'getPoints']);
    Route::get('/user/transaction/get/limit/{offsetData}/type/{transactionType}', [TransactionController::class, 'getTransactions']);

    Route::get('/products/get-all', [ProductController::class, 'getProducts']);
    Route::get('/products/get/{offsetData}', [ProductController::class, 'getLimitedProducts']);
    Route::get('/products/promoted/get/{offsetData}', [ProductController::class, 'getLimitedPromotedProducts']);
    Route::get('/product/brand-types-and-flavours/get', [ProductController::class, 'getProductBrandsTypesAndFlavours'])->name('add.product');
    Route::post('/products/filter', [ProductController::class, 'getFilteredProducts']);

    Route::post('/pay', [TransactionController::class, 'pay']);
    Route::post('/save/transaction', [TransactionController::class, 'store']);
    Route::post('/link/payment/card', [TransactionController::class, 'linkPaymentCard']);
    Route::post('/auto-billing', [TransactionController::class, 'autoBilling']);
});

Route::post('/login', [AuthUserController::class, 'login']);
Route::post('/registration', [UserController::class, 'store']);

Route::get('/auth/check', [AuthUserController::class, 'authCheck']);

Route::post('/forgot-pasword', [UserController::class, 'forgotPassword']);
Route::get('/reset-password/{token}/{email}', [UserController::class, 'resetPasswordToken'])->name('password.reset');
Route::post('/reset-password', [UserController::class, 'resetPassword']);

Route::get('/gym-franchise/get-all', [GymFranchiseController::class, 'getGymFranchise']);