<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateFranchiseAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'FRANCHISE',
            'last_name' => 'ADMIN',
            'birthdate' => '',
            'gender' => 'male',
            'mobile_number' => '123456879',
            'email' => 'userfranchiseadmin@gmail.com',
            'password' => bcrypt('123456'),
            'username' => 'userfranchiseadmin',
            'user_type' => 'franchiseAdmin'
        ]);

        $role = Role::create(['name' => 'FranchiseAdmin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
