<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateSuperAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'SUPER',
            'last_name' => 'ADMIN',
            'birthdate' => '',
            'gender' => 'male',
            'mobile_number' => '123456879',
            'email' => 'usersuperadmin@gmail.com',
            'password' => bcrypt('123456'),
            'username' => 'usersuperadmin',
            'user_type' => 'superAdmin'
        ]);

        $role = Role::create(['name' => 'SuperAdmin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
