<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'super-admin-list',
            'super-admin-create',
            'super-admin-edit',
            'super-admin-delete',
            'franchise-admin-list',
            'franchise-admin-create',
            'franchise-admin-edit',
            'franchise-admin-delete',
            'gym-admin-list',
            'gym-admin-create',
            'gym-admin-edit',
            'gym-admin-delete',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete'
        ];

        foreach ($permissions as $permission) {
            $checkPermission = Permission::where('name', '=', $permission)->first();

            if (!$checkPermission) {
                Permission::create(['name' => $permission]);
            }
        }
    }
}
