<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeederChanges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionsRemoved = [
            'franchise-admin-list',
            'franchise-admin-create',
            'franchise-admin-edit',
            'franchise-admin-delete',
            'gym-admin-list',
            'gym-admin-create',
            'gym-admin-edit',
            'gym-admin-delete',
        ];

        $permissionsAdded = [
            'franchise-transaction-list',
            'franchise-list',
            'franchise-create',
            'franchise-edit',
            'franchise-delete',
            'gym-transaction-list',
            'gym-list',
            'gym-create',
            'gym-edit',
            'gym-delete',
            'user-transaction-list',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'settings-list',
            'settings-create',
            'settings-edit',
            'settings-delete',
        ];

        foreach ($permissionsRemoved as $permissionRemove) {
            $checkPermission = Permission::where('name', '=', $permissionRemove)->first();

            if ($checkPermission) {
                $checkPermission->delete();
            }
        }

        foreach ($permissionsAdded as $permissionAdd) {
            $checkPermission = Permission::where('name', '=', $permissionAdd)->first();

            if (!$checkPermission) {
                Permission::create(['name' => $permissionAdd]);
            }
        }
    }
}
