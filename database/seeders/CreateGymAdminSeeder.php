<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateGymAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'GYM',
            'last_name' => 'ADMIN',
            'birthdate' => '',
            'gender' => 'male',
            'mobile_number' => '123456879',
            'email' => 'usergymadmin@gmail.com',
            'password' => bcrypt('123456'),
            'username' => 'usergymadmin',
            'user_type' => 'gymAdmin'
        ]);

        $role = Role::create(['name' => 'GymAdmin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
