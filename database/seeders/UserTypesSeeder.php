<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\UserTypes;
use DB;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = [
            [
                'name_of_type' => 'Individual',
            ],
            [
                'name_of_type' => 'Admin',
            ],
            [
                'name_of_type' => 'SuperAdmin',
            ],
        ];

        foreach ($userTypes as $userType) {
            
            $checkUserType = UserTypes::where('name_of_type', '=', $userType['name_of_type'])->first();

            if (!$checkUserType) {
                DB::table('user_types')->insert([
                    'name_of_type' => $userType['name_of_type'],
                ]);
            }
        }
    }
}
