<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('athlete_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('level_of_fitness')->nullable();
            $table->string('gym_attendance')->nullable();
            $table->string('priorities')->nullable();
            $table->string('type_of_training')->nullable();
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('athlete_profile');
    }
};
