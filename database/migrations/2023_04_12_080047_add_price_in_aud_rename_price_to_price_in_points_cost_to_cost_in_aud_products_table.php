<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->renameColumn('price', 'price_in_points');
            $table->renameColumn('cost', 'cost_in_aud');
            $table->string('price_in_aud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->renameColumn('price_in_points', 'price');
            $table->renameColumn('cost_in_aud', 'cost');
            $table->dropColumn('price_in_aud');
        });
    }
};
