<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->renameColumn('points_per_dollar', 'multiplier');
            $table->renameColumn('dollar_per_drink', 'points');
            $table->renameColumn('number_of_drink', 'bonus');
            $table->renameColumn('price_per_drink_in_aud', 'total_points');
            $table->dropColumn('price_per_drink_in_points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->renameColumn('multiplier', 'points_per_dollar');
            $table->renameColumn('points', 'dollar_per_drink');
            $table->renameColumn('bonus', 'number_of_drink');
            $table->renameColumn('total_points', 'price_per_drink_in_aud');
            $table->string('price_per_drink_in_points');
        });
    }
};
