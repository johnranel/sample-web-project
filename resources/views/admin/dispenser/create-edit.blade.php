@extends('layouts.app')

@section('title', 'Product Management')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('dispenser.management') }}" style="color: #000;">Dispenser</a> <i class="fa fa-chevron-right"></i> <a href="{{ route('assign.dispenser.products', ['dispenser' => $dispenser->id]) }}" style="color: #000;">
                Assign Products
            </a>
        </div>
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5" id="product_management">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container mt-4 p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 id="title_text">
                                Assign Products to Dispenser {{ $dispenser->dispensers_id }}
                            </h5>
                        </div>
                    </div>
                    <form action="{{ route('update.dispenser.products', ['dispenser' => $dispenser->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table id="product-table" class="table">
                                    <thead>
                                        <tr>
                                            <th width="5%">
                                            </th>
                                            <th width="33%">
                                                Brand
                                            </th>
                                            <th width="33%">
                                                Product Name
                                            </th>
                                            <th width="33%">
                                                Flavour
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $key => $product)
                                            @php
                                                foreach($selected_products as $key_select => $selected_product) {
                                                    if($product->id == $selected_product->product_id) {
                                                        $product['selected'] = true;
                                                    }
                                                }
                                            @endphp
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="product_id[{{$key}}]" value="{{ $product->id }}" @if($product->selected == true) checked @endif />
                                                </td>
                                                <td>
                                                    {{ $product->name }}
                                                </td>
                                                <td>
                                                    {{ $product->manufacturer_name }}
                                                </td>
                                                <td>
                                                    {{ $product->flavour }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right btn-container">
                                <a href="{{ route('dispenser.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-secondary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    
@endsection
