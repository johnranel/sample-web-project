@extends('layouts.app')

@section('title', 'Gym Transactions')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <style>
        .button-custom {
            padding: 0 20px 0 20px !important;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="gym_franchise_management">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-xl-4 col-lg-5 col-md-6">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body" style="background-color: #fff; box-shadow: unset;">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-md font-weight-bold text-dark text-uppercase mb-1">
                                            Purchases (Total)
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-left" id="purchses-total">AU $ {{ $purchase_total }}.00</div>
                                    </div>
                                    <div class="col-auto">
                                        <a class="purchases-date-range" role=button>
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 mt-4" style="justify-content: end; align-items: end; display: flex;">
                        <button type="button" class="form-control button-custom" id="pdf-download" style="width: auto !important; float: right;"><i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i> Download as PDF</button>
                        <button type="button" class="form-control mr-2 button-custom" id="excel-download" style="width: auto !important; float: right;"><i class="fa fa-file-excel-o mr-1" aria-hidden="true"></i> Download as Excel Sheet</button>
                        <div style="display: flex; align-items: center;">
                            Show
                            <select class="form-control ml-2 mr-2 dataTable_pageLength">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                            </select>
                            entries
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="table-container mt-4 p-4">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 col-md-4 m-4 text-center">
                            <img src="{{ $franchise_details->imagePath }}" height="auto" width="100%" class="mb-3" />
                            <div class="pl-4 pr-4">{{ $gym_details->address }}</div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 table-responsive">
                            <table id="gym-transactions-table" class="table" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Dispenser ID</th>
                                        <th>Shaker ID</th>
                                        <th>SS Member ID</th>
                                        <th>User Name</th>
                                        <th>Price</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="date-range-modal" tabindex="-1" role="dialog" aria-labelledby="dateRangeModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dateRangeModal">Date Range</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Min Date <span class="text-danger">*</span></label>
                        <input type="date" id="min" name="min" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Max Date <span class="text-danger">*</span></label>
                        <input type="date" id="max" name="max" autocomplete="off" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button-custom" id="date-range-filter">Search</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.29.4/moment.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script>
        var total = 0;

        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = $("#min").val();
                var max = $("#max").val();
                var date = data[4];
                var price = data[3].replace('AU $', '');
                if((( min == "" || max == "" ) || (moment(date).isSameOrAfter(min) && moment(date).isSameOrBefore(max)))) {
                    return true;
                }
                return false;
            }
        );

        var base_url = $('meta[name="base-url"]').attr('content');

        window.addEventListener("beforeunload", function () {
            $("#content").addClass("animate-out");
        });

        $(document).ready(function() {
            let current_url = window.location.href;
            var dataTable = $('#gym-transactions-table').DataTable({
                ajax: `${current_url}`,
                language: {
                    'paginate': {
                    'previous': '<i class="las la-angle-left"></i>',
                    'next': '<i class="las la-angle-right"></i>'
                    }
                },
                columns: [
                    {
                        data: 'dispensers_id',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                    {
                        data: 'user.shaker.shaker_id',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                    {
                        data: 'user.id',
                        render: function (data, type, row) {
                            let member_id = parseInt(data) + 1000;
                            return member_id;
                        }
                    },
                    {
                        data: 'user.first_name',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${data} ${row.user.last_name}</span>`;
                        }
                    },
                    {
                        data: 'amount',
                        render: function (data, type, row) {
                            return `AU $${data}.00`;
                        }
                    },
                    {
                        data: 'created_at',
                        render: function (data, type, row) {
                            let date = moment(data).format('YYYY-MM-DD')
                            return `${date}`;
                        }
                    }
                ],
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'pdf',
                    title: 'Transactions',
                    filename: 'transactions_table_supplement_station'
                }, {
                    extend: 'excel',
                    title: 'Transactions',
                    filename: 'transactions_table_supplement_station'
                }],
                "lengthChange": false,
                "info": false,
                "bAutoWidth": false,
                responsive: true,
                scrollCollapse: true
            });

            $(".dataTable_pageLength").on('change', function () {
                let table_length = $(this).val();
                dataTable.page.len(table_length).draw();
            });

            $('#date-range-filter').on('click', function () {
                total = 0;
                dataTable.draw();
            });

            $("#gym-transactions-table_filter").remove();

            $(".purchases-date-range").click(function () {
                $("#date-range-modal").modal("show");
                total = 0;
            });

            $("#pdf-download").click(function (){
                $(".buttons-pdf").trigger('click');
            });

            $("#excel-download").click(function (){
                $(".buttons-excel").trigger('click');
            });
        });
    </script>
@endsection
