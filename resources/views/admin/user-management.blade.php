@extends('layouts.app')

@section('title', 'User Management')

@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css">
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="user_management">
            <div class="col-xl-6 col-lg-6 col-md-6"></div>
            <div class="col-xl-6 col-lg-6 col-md-6 pl-0" style="justify-content: end; align-items: end; display: flex;">
                <div style="display: flex; align-items: center;">
                    Show
                    <select class="form-control ml-2 mr-2 dataTable_pageLength">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30" selected="selected">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                    </select>
                    entries
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <table id="user-table" class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>SS Member ID</th>
                                <th>Shaker ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Date of Birth</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Gender</th>
                                <th>Balance <br>in Points</th>
                                <th>Member <br> <span style="font-size: 13px;"> of Franchise<span></th>
                                <th>Weight <br>in kg</th>
                                <th>Level of Fitness</th>
                                <th>Gym Attendance</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        var dataTable = $('#user-table').DataTable({
            ajax: `${base_url}/user-management`,
            language: {
                'paginate': {
                'previous': '<i class="las la-angle-left"></i>',
                'next': '<i class="las la-angle-right"></i>'
                }
            },
            columns: [
                {
                    data: 'id',
                    render: function (data, type, row) {
                        let member_id = parseInt(data) + 1000;
                        return member_id;
                    }
                },
                {
                    data: 'shaker.shaker_id',
                    render: function (data, type, row) {
                        let shaker_id = "-";
                        if(data != null) {
                            shaker_id = data;
                        }
                        return shaker_id;
                    }
                },
                {
                    data: 'first_name',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'last_name',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'birthdate',
                    render: function (data, type, row) {
                        return moment(data).format('YYYY-MM-DD');
                    }
                },
                {
                    data: 'email'
                },
                {
                    data: 'mobile_number',
                    render: function (data, type, row) {
                        return `+${data}`;
                    }
                },
                {
                    data: 'gender',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'points.points',
                    render: function (data, type, row) {
                        let points = 0;
                        if(data) {
                            points = data;
                        }
                        return points;
                    }
                },
                {
                    data: 'franchise.name',
                    render: function (data, type, row) {
                        let franchise = "No membership available.";
                        if(data) {
                            franchise = data;
                        }
                        return `<span style="text-transform: capitalize;">${franchise}</span>`;
                    }
                },
                {
                    data: 'weight',
                    render: function (data, type, row) {
                        return data.weight;
                    }
                },
                {
                    data: 'athlete_profile',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data.level_of_fitness}</span>`;
                    }
                },
                {
                    data: 'athlete_profile',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data.gym_attendance}</span>`;
                    }
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        var editRemove = `<a href="${base_url}/user/edit/${data}" title="Edit"><i class="las la-user-edit"></i></a>`;
                        editRemove += `<a href="#" onclick="deleteUser(${data})" title="Delete"><i class="las la-trash"></i></a>`;

                        return editRemove;
                    }
                }
            ],
            order: [ [0, 'desc'] ],
            "pageLength": 30,
            "lengthChange": false,
            searching: false,
            "info": false,
            "bAutoWidth": false,
            responsive: true,
            scrollCollapse: true
        });

        function deleteUser(id) {
            Swal.fire({
                title: 'Are you sure you want to delete this user?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'/user/delete/'+id,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res){
                                toastr.success('User successfully deleted', 'Success!');
                                dataTable.ajax.reload();
                            }
                        },
                        error: function (er) {
                            toastr.error('Deleting user failed', 'Failed!');
                        }
                    });
                }
            });
        }

        $(".dataTable_pageLength").on('change', function () {
            let table_length = $(this).val();
            dataTable.page.len(table_length).draw();
        });

        window.addEventListener("beforeunload", function () {
            $("#content").addClass("animate-out");
        });

        @if(session()->has('message'))
            var success_message = "{{ session()->get('message') }}";
            toastr.success(`${success_message}`);
        @endif
    </script>
@endsection
