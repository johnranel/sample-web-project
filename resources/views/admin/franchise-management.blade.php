@extends('layouts.app')

@section('title', 'Franchise Management')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="franchise_management">
            <div class="col-xl-8 col-lg-6 col-md-8"></div>
            <div class="col-xl-4 col-lg-6 col-md-4 text-right">
                <a href="{{ route('create.franchise') }}" class="button-custom"><i class="las la-plus"></i> Add New Franchise</a>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <table id="franchise-table" class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Franchise Logo</th>
                                <th>Franchise Name</th>
                                <th>Headquarter Address</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Admin <br> <span style="font-size: 12px; font-family: 'MontserratRegular';"> Username <br> Password<span></th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        var dataTable = $('#franchise-table').DataTable({
            ajax: `${base_url}/franchise-management`,
            language: {
                'paginate': {
                'previous': '<i class="las la-angle-left"></i>',
                'next': '<i class="las la-angle-right"></i>'
                }
            },
            columns: [
                {
                    data: 'imagePath',
                    render: function (data, type, row) {
                        if(data) {
                            return `<img src="${data}" width="100" />`;
                        } else {
                            return `-`;
                        }
                    }
                },
                {
                    data: 'name',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'address',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'phone',
                    render: function (data, type, row) {
                        if(data == null) {
                            data = "-"
                        }
                        return data;
                    }
                },
                {
                    data: 'email',
                    render: function (data, type, row) {
                        if(data == null) {
                            data = "-"
                        }
                        return data;
                    }
                },
                {
                    data: 'users',
                    render: function (data, type, row) {
                        let admin_data = "";
                        jQuery.each(data, function(index, item) {
                            admin_data += `<span>${item.username}</span> <br> <span>${item.uncrypt_password}</span>`
                        });
                        return admin_data;
                    }
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        var editRemove = `<a href="/franchise/edit/${data}" title="Edit"><i class="las la-edit"></i></a>`;
                        editRemove += `<a href="#" onclick="deleteFranchise(${data})" title="Delete"><i class="las la-trash"></i></a>`;

                        return editRemove;
                    }
                }
            ],
            "lengthChange": false,
            searching: false,
            "info": false,
            "bAutoWidth": false,
            responsive: true,
            scrollCollapse: true
        });

        function deleteFranchise(id) {
            Swal.fire({
                title: 'Are you sure you want to delete this franchise?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'/franchise/delete/'+id,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res){
                                toastr.success('Franchise successfully deleted', 'Success!');
                                dataTable.ajax.reload();
                            }
                        },
                        error: function (er) {
                            toastr.error('Deleting franchise failed', 'Failed!');
                        }
                    });
                }
            });
        }

        @if(session()->has('message'))
            var success_message = "{{ session()->get('message') }}";
            toastr.success(`${success_message}`);
        @endif
    </script>
@endsection
