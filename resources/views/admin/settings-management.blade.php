@extends('layouts.app')

@section('title', 'Super Admin Settings')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="gym_franchise_management">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <table id="settings-table" class="pb-2 table" style="width: 100%">
                        <thead>
                            <tr>
                                <th data-orderable="false">
                                    <div style="border: 2px solid #eee; border: 2px solid #676767; padding: .75rem; margin-top: 1rem;">
                                        Top-up amount
                                    </div>
                                    @if(auth()->user()->hasRole('SuperAdmin'))
                                        <input type="hidden" name="id">
                                        <select name="topup_amount" class="form-control mt-3">
                                            <option value="30">30</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="150">150</option>
                                        </select>
                                    @endif
                                </th>
                                <th data-orderable="false">
                                    <div style="border: 2px solid #eee; border: 2px solid #676767; padding: .75rem; margin-top: 1rem;">
                                        Multiplier
                                    </div>
                                    @if(auth()->user()->hasRole('SuperAdmin'))
                                        <input type="number" name="multiplier" class="form-control mt-3">
                                    @endif
                                </th>
                                <th data-orderable="false">
                                    <div style="border: 2px solid #eee; border: 2px solid #676767; padding: .75rem; margin-top: 1rem;">
                                        Points
                                    </div>
                                    @if(auth()->user()->hasRole('SuperAdmin'))
                                        <input type="number" name="points" class="form-control mt-3" disabled>
                                    @endif
                                </th>
                                <th data-orderable="false">
                                    <div style="border: 2px solid #eee; border: 2px solid #676767; padding: .75rem; margin-top: 1rem;">
                                        Bonus in %
                                    </div>
                                    @if(auth()->user()->hasRole('SuperAdmin'))
                                        <input type="number" name="bonus" class="form-control mt-3" value="0">
                                    @endif
                                </th>
                                <th data-orderable="false">
                                    <div style="border: 2px solid #eee; border: 2px solid #676767; padding: .75rem; margin-top: 1rem;">
                                        Total Points
                                    </div>
                                    @if(auth()->user()->hasRole('SuperAdmin'))
                                        <input type="number" name="total_points" class="form-control mt-3" disabled>
                                    @endif
                                </th data-orderable="false">
                                @if(auth()->user()->hasRole('SuperAdmin'))
                                    <th>
                                        <div style="border: 2px solid #eee; border: 2px solid #676767; padding: .75rem; margin-top: 1rem;">
                                            Action
                                        </div>
                                        <button type="button" id="store_settings" class="btn btn-secondary form-control mt-3">Save</button>
                                    </th>
                                @endif
                            </tr>
                            <tr>
                                <th colspan="6" style="text-align: left !important;"><h5>Current</h5></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        var dataTable = $('#settings-table').DataTable({
            ajax: `${base_url}/settings-management`,
            language: {
                'paginate': {
                'previous': '<i class="las la-angle-left"></i>',
                'next': '<i class="las la-angle-right"></i>'
                }
            },
            columns: [
                {
                    data: 'topup_amount',
                    render: function (data, type, row) {
                        return "AU$ " + data;
                    }
                },
                {
                    data: 'multiplier'
                },
                {
                    data: 'points'
                },
                {
                    data: 'bonus'
                },
                {
                    data: 'total_points'
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        let stringify = JSON.stringify(row);
                        var editRemove = `<a href="#" onclick='updateSettings(${stringify})' title="Edit"><i class="las la-edit"></i></a>`;
                        editRemove += `<a href="#" onclick="deleteSettings(${data})" title="Delete"><i class="las la-trash"></i></a>`;

                        return editRemove;
                    }
                }
            ],
            order: [ [4, 'asc'] ],
            "lengthChange": false,
            searching: false,
            "info": false,
            "bAutoWidth": false,
            responsive: true,
            scrollCollapse: true
        });

        $(document).on("click", "#store_settings", function () {
            $validation_response = form_validation();
            if($validation_response) {
                $.ajax({
                    url: base_url+'/settings/store',
                    type: 'post',
                    data : $validation_response,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (res) {
                        if(res){
                            toastr.success('Settings successfully created.', 'Success!');
                            clear_form();
                            dataTable.ajax.reload();
                        }
                    },
                    error: function (er) {
                        toastr.error('Creating settings failed.', 'Failed!');
                    }
                });
            }
        });

        function form_validation () {
            let id                          = $("input[name='id']").val();
            let topup_amount                = $("select[name='topup_amount']").val();
            let multiplier                  = $("input[name='multiplier']").val();
            let points                      = $("input[name='points']").val();
            let bonus                       = $("input[name='bonus']").val();
            let total_points                = $("input[name='total_points']").val();

            let error_message = [];
            if(multiplier == "") {
                error_message.push("Multiplier");
            }

            if(points == "") {
                error_message.push("Points");
            }

            if(total_points == "") {
                error_message.push("Total Points");
            }

            if(multiplier == "" || points == "" || total_points == "") {
                error_message = error_message.join(", ");
                error_message += " the following fields needs to be filled up.";
                toastr.error(error_message);
                return false;
            } else {
                if(id == "") {
                    return {'topup_amount': topup_amount, 'multiplier': multiplier, 'points': points, 'bonus': bonus, 'total_points': total_points};
                } else {
                    return {'id': id, 'topup_amount': topup_amount, 'multiplier': multiplier, 'points': points, 'bonus': bonus, 'total_points': total_points};
                }
            }
        }

        function deleteSettings(id) {
            Swal.fire({
                title: 'Are you sure you want to delete this setting?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'/settings/delete/'+id,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res){
                                toastr.success('Settings successfully deleted', 'Success!');
                                dataTable.ajax.reload();
                            }
                        },
                        error: function (er) {
                            toastr.error('Deleting settings failed', 'Failed!');
                        }
                    });
                }
            });
        }

        function updateSettings(data) {
            $("input[name='id']").val(data.id);
            $("select[name='topup_amount']").val(data.topup_amount);
            $("input[name='multiplier']").val(data.multiplier);
            $("input[name='points']").val(data.points);
            $("input[name='bonus']").val(data.bonus);
            $("input[name='total_points']").val(data.total_points);

            $("#store_settings").attr('id', 'update_settings');
        }

        $(document).on("click", "#update_settings", function () {
            $validation_response = form_validation();
            if($validation_response) {
                $.ajax({
                    url: base_url+'/settings/update/'+$validation_response.id,
                    type: 'post',
                    data : $validation_response,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (res) {
                        if(res){
                            toastr.success('Settings successfully updated.', 'Success!');
                            $("#update_settings").attr('id', 'store_settings');
                            clear_form();
                            dataTable.ajax.reload();
                        }
                    },
                    error: function (er) {
                        toastr.error('Updating settings failed.', 'Failed!');
                    }
                });
            }
        });

        function clear_form () {
            $("input[name='id']").val("");
            $("select[name='topup_amount']").val("");
            $("input[name='multiplier']").val("");
            $("input[name='points']").val("");
            $("input[name='bonus']").val("");
            $("input[name='total_points']").val("");
        }

        $("select[name='topup_amount']").on("change", function() {
            $("input[name='bonus']").trigger("keyup");
            $("input[name='multiplier']").trigger("keyup");
        });

        $("input[name='multiplier']").on("keyup", function() {
            let top_up = $("select[name='topup_amount']").val();
            let multiplier = $(this).val();

            $("input[name='points']").val(top_up * multiplier);
            $("input[name='bonus']").trigger("keyup");
        });

        $("input[name='bonus'], input[name='multiplier']").on("click", function() {
            $("input[name='multiplier']").trigger("keyup");
            $("input[name='bonus']").trigger("keyup");
        });

        $("input[name='bonus']").on("keyup", function() {
            let bonus_value = $(this).val();
            
            if($(this).val() == "") {
                bonus_value = 0;
            }
            
            let points = parseInt($("input[name='points']").val());
            let bonus = parseInt(bonus_value);

            $("input[name='total_points']").val((points * bonus) / 100 + points);
            $("input[name='multiplier']").trigger("keyup");
        });

        @if(!auth()->user()->hasRole('SuperAdmin'))
            dataTable.column(5).visible(false);
        @endif
    </script>
@endsection
