@extends('layouts.app')

@section('title', 'Product')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('product.management') }}" style="color: #000;">Supplements</a> <i class="fa fa-chevron-right"></i> <a href="@if(isset($product_details)) {{ route('edit.product', ['product' => $product_details->id]) }} @else {{ route('create.product') }} @endif" style="color: #000;">
                @if(isset($product_details))
                    Edit
                @else
                    Create
                @endif
            </a>
        </div>
        <div class="col-md-12" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
            <form action="@if(isset($product_details)) {{ route('update.product', ['product' => $product_details->id]) }} @else {{ route('store.product') }} @endif" method="post" enctype="multipart/form-data">
                @csrf
                <div class="" style="padding:20px;">
                    <h5 id="title_text">
                        @if(isset($product_details)) 
                            Update 
                        @else
                            Add New 
                        @endif
                        Supplement
                    </h5>
                    <h6>
                        All mandatory fields are marked with a red asterisk <span style="color: red;">*</span>.
                    </h6>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                @if(!isset($product_details))
                                    <label for="manufacturer_name">Image</label>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    @if(isset($product_details))
                                        <div class="mb-3">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>Product Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($product_details->media as $product_images)
                                                        @if($product_images->type == "image")
                                                        <tr>
                                                            <td>
                                                                <img src="{{ $product_images->path }}" width="80" height="80" />
                                                            </td>
                                                            <td>
                                                                <a href="#" class="view-image" data-image_link="{{ $product_images->path }}" title="View Image"><i class="las la-eye"></i></a>
                                                                <a href="#" class="delete-image" data-id="{{ $product_images->id }}" title="Delete"><i class="las la-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    <div class="mb-3">
                                        <div class="dropzone" id="product_dropzone" style="padding: 7px; min-height: 134px;">
                                            <!--begin::Message-->
                                            <div class="dz-message needsclick">
                                                <!--begin::Icon-->
                                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                                <!--end::Icon-->
                                                <!--begin::Info-->
                                                <div class="ms-4">
                                                    <h5 class="fs-5 fw-bold text-gray-900 mb-1">Drop supplement images or click to upload.</h5>
                                                    <span class="fs-7 fw-semibold text-gray-400">Upload up to 10 images</span>
                                                </div>
                                                <!--end::Info-->
                                            </div>
                                        </div>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if(isset($product_details))
                                        <div class="mb-3">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>Nutrients Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($product_details->media as $product_images)
                                                        @if($product_images->type == "nutrients")
                                                        <tr>
                                                            <td>
                                                                <img src="{{ $product_images->path }}" width="80" height="80" />
                                                            </td>
                                                            <td>
                                                                <a href="#" class="view-image" data-image_link="{{ $product_images->path }}" title="View Image"><i class="las la-eye"></i></a>
                                                                <a href="#" class="delete-image" data-id="{{ $product_images->id }}" title="Delete"><i class="las la-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    <div class="mb-3">
                                        <div class="dropzone" id="nutrients_dropzone" style="padding: 7px; min-height: 134px;">
                                            <!--begin::Message-->
                                            <div class="dz-message needsclick">
                                                <!--begin::Icon-->
                                                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                                <!--end::Icon-->
                                                <!--begin::Info-->
                                                <div class="ms-4">
                                                    <h5 class="fs-5 fw-bold text-gray-900 mb-1">Drop supplement ingredients image or click to upload.</h5>
                                                    <span class="fs-7 fw-semibold text-gray-400">Upload up to 10 images</span>
                                                </div>
                                                <!--end::Info-->
                                            </div>
                                        </div>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="product_no">Supplement No. <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="product_no" id="product_no" @if(isset($product_details)) value="{{ $product_details->product_no }}" @endif readonly required>
                                @if($errors->has('product_no'))
                                    <span class="text-danger">Product No. is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="manufacturer_name">Manufacturer Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="manufacturer_name" id="manufacturer_name" @if(isset($product_details)) value="{{ $product_details->manufacturer_name }}" @endif required>
                                @if($errors->has('manufacturer_name'))
                                    <span class="text-danger">Manufacturer name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="name">Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" @if(isset($product_details)) value="{{ $product_details->name }}" @endif required>
                                @if($errors->has('name'))
                                    <span class="text-danger">Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="type">Powder type <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="type" id="type" required>
                                    <option value=""></option>
                                    @foreach($product_types as $key => $types)
                                        <option value="{{ $types['value'] }}" @if(isset($product_details)) {!! ($product_details->type == $types['value']) ? 'selected' : '' !!} @endif>{{ $types['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('type'))
                                    <span class="text-danger">Type is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="price_in_points">Price in Points <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="price_in_points" id="price_in_points" @if(isset($product_details)) value="{{ $product_details->price_in_points }}" @endif required>
                                @if($errors->has('price_in_points'))
                                    <span class="text-danger">Price in points is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="price_in_aud">Price in AUD <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="price_in_aud" id="price_in_aud" @if(isset($product_details)) value="{{ $product_details->price_in_aud }}" @endif required>
                                @if($errors->has('price_in_aud'))
                                    <span class="text-danger">Price in AUD is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="cost_in_aud">Cost in AUD <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="cost_in_aud" id="cost_in_aud" @if(isset($product_details)) value="{{ $product_details->cost_in_aud }}" @endif required>
                                @if($errors->has('cost_in_aud'))
                                    <span class="text-danger">Cost in AUD is required.</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="description">Description <span style="color: red;">*</span></label>
                                <textarea class="form-control" rows="5" cols="50" name="description" id="description" required>@if(isset($product_details)){{$product_details->description}}@endif</textarea>
                                @if($errors->has('description'))
                                    <span class="text-danger">Description is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="flavours">Flavour <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="flavour" id="flavours" required>
                                    <option value="-">-</option>
                                    @foreach($product_flavours as $key => $flavours)
                                        <option value="{{ $flavours['value'] }}" @if(isset($product_details)) {!! ($product_details->flavour == $flavours['value']) ? 'selected' : '' !!} @endif>{{ $flavours['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('flavours'))
                                    <span class="text-danger">Flavour is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="portion_size">Serving Size in Grams <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="portion_size" id="portion_size" @if(isset($product_details)) value="{{ $product_details->portion_size }}" @endif required>
                                @if($errors->has('portion_size'))
                                    <span class="text-danger">Serving Size is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <input type="checkbox" name="promoted_product" id="promoted_product" value="1" @if(isset($product_details)) {!! ($product_details->promoted_product) ? 'checked' : '' !!} @endif>
                                <label for="promoted_product">Is this supplement being promoted?</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right btn-container">
                            <a href="{{ route('product.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-secondary">
                                @if(isset($product_details)) 
                                    Update 
                                @else
                                    Save
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="imageViewerModal" tabindex="-1" role="dialog" aria-labelledby="imageViewerModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="imageViewerModalLabel">Image Viewer</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img id="image-viewer" width="100%" />
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');

        var productDropzone = new Dropzone("#product_dropzone", {
            url: base_url+"/upload/images", // Set the url for your upload script location
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 3,
            maxFilesize: 10, // MB
            acceptedFiles: 'image/*',
            init: function() {
                this.on("success", function(file, server){
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");
                    var addHiddenImage = Dropzone.createElement("<input type='hidden' name='images[]' class='image_dropzone' value='" + server + "' />");
                    var addPImageLink = Dropzone.createElement("<p class='d-none image-p-link' data-image_link='" + server + "' /></p>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;
                    var fileNameFromServer = server;
                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        $.ajax({
                            method: "POST",
                            url: base_url+'/delete/image',
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: { file_name: fileNameFromServer}
                        });

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                    file.previewElement.appendChild(addHiddenImage);
                    file.previewElement.appendChild(addPImageLink);
                });
            }
        });

        $(document).on('click', '.dz-preview', function() {
            let imageElem = $(this).find("p");
            $("#image-viewer").attr("src", base_url+"/"+imageElem.attr("data-image_link"));
            $("#imageViewerModal").modal("show");
        });

        $(document).on('click', '.view-image', function() {
            let imageLink = $(this).attr("data-image_link");
            $("#image-viewer").attr("src", imageLink);
            $("#imageViewerModal").modal("show");
        });

        var nutrientsDropzone = new Dropzone("#nutrients_dropzone", {
            url: base_url+"/upload/images", // Set the url for your upload script location
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 3,
            maxFilesize: 10, // MB
            acceptedFiles: 'image/*',
            init: function() {
                this.on("success", function(file, server){
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");
                    var addHiddenImage = Dropzone.createElement("<input type='hidden' name='nutrients[]' value='" + server + "' />");
                    var addPImageLink = Dropzone.createElement("<p class='d-none image-p-link' data-image_link='" + server + "' /></p>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;
                    var fileNameFromServer = server;

                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form
                        e.preventDefault();
                        e.stopPropagation();

                        $.ajax({
                            method: "POST",
                            url: base_url+'/delete/image',
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: { file_name: fileNameFromServer}
                        });

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                    file.previewElement.appendChild(addHiddenImage);
                    file.previewElement.appendChild(addPImageLink);
                });
            }
        });

        $(document).on('click', ".delete-image", function() {
            let _this = $(this);
            let image_id = _this.attr('data-id');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this image file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: "POST",
                        url: base_url+'/delete/media/'+image_id,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res) {
                                _this.parent().parent().remove();
                                swal("Poof! Your image file has been deleted!", {
                                    icon: "success",
                                });
                            }
                        }
                    });
                }
            });
        });

        $("#manufacturer_name").keyup(function () {
                product_char = $(this).val().substr(0, 2).toUpperCase();
                product_no = 1000;
                $.ajax({
                    url: `${base_url}/product/check/${product_char}`,
                    success: function(result){
                        let extract_num = 1000;
                        if(result.product_no) {
                            extract_num = result.product_no.replace(/^\D+/g, '');
                            extract_num = parseInt(extract_num);
                        }
                        $("#product_no").val(product_char+(extract_num+1));
                    }
                });
            });
    </script>
@endsection