@extends('layouts.app')

@section('title', 'Gym')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('gym.management') }}" style="color: #000;">Gym</a> <i class="fa fa-chevron-right"></i> <a href="@if(isset($gym)) {{ route('edit.gym', ['individualGym' => $gym->id]) }} @else {{ route('create.gym') }} @endif" style="color: #000;">
                @if(isset($gym))
                    Edit
                @else
                    Create
                @endif
            </a>
        </div>
        <div class="col-md-12" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
            <form action="@if(isset($gym)) {{ route('update.gym', ['individualGym' => $gym->id]) }} @else {{ route('store.gym') }} @endif" method="post" enctype="multipart/form-data">
                @csrf
                <div style="padding:20px;">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 id="title_text">
                                @if(isset($gym)) 
                                    Update 
                                @else
                                    Add New 
                                @endif
                                Gym
                            </h5>
                            <h6>
                                All mandatory fields are marked with a red asterisk <span style="color: red;">*</span>.
                            </h6>
                            <br>
                        </div>
                        @if(!isset($gym))
                        <div class="col-md-6">
                            <h5 id="title_text"> Gym Admin </h5>
                            <h6>
                                Automatically generated.
                            </h6>
                        </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($gym))
                                @if(isset($gym->imagePath))
                                    <div class="mb-3">
                                        <img src="{{ $gym->imagePath }}" style="object-fit: cover; height: 100px;" />
                                    </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="mb-3 d-none">
                                <label for="image">Gym Logo</label>
                                <input type="file" class="form-control" placeholder="image" name="file" id="image">
                            </div>
                            <div class="mb-3">
                                <label for="name">Gym Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" @if(isset($gym)) value="{{ $gym->name }}" @endif required>
                                @if($errors->has('name'))
                                    <span class="text-danger">Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3 d-none">
                                <label for="description">Description <span style="color: red;">*</span></label>
                                <textarea class="form-control" rows="5" cols="50" name="description" id="description" required>@if(isset($gym)){{$gym->description}}@else - @endif</textarea>
                                @if($errors->has('description'))
                                    <span class="text-danger">Description is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="flavours">Franchise <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="franchise_id" required>
                                    <option value=""></option>
                                    @foreach($franchises as $key => $franchise)
                                        <option value="{{ $franchise['id'] }}" @if(isset($gym)) {!! ($gym->franchise_id == $franchise['id']) ? 'selected' : '' !!} @endif>{{ $franchise['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('franchise_id'))
                                    <span class="text-danger">Franchise is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="address">Location <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="address" id="address" @if(isset($gym)) value="{{ $gym->address }}" @endif required>
                                @if($errors->has('address'))
                                    <span class="text-danger">Location is required.</span>
                                @endif
                            </div>
                            
                            <div class="mb-3">
                                <label for="dispenser_id">Dispenser IDs <span style="color: red;">*</span></label>
                                <div class="repeater">
                                    <div data-repeater-list="dispenser">
                                        @if(!empty($gym))
                                            @forelse($gym->dispensers as $key => $dispenser)
                                                <div data-repeater-item style="flex-direction: row;" class="d-flex mb-3">
                                                    <input type="text" class="form-control" name="dispensers_id" id="dispenser_id" value="{{ $dispenser->dispensers_id }}" required>
                                                    <input type="hidden" name="id" value="{{ $dispenser->id }}">
                                                    <button data-repeater-delete type="button" class="btn btn-danger ml-2"><i class="fa fa-close"></i></button>
                                                </div>
                                            @empty
                                                <div data-repeater-item style="flex-direction: row;" class="d-flex mb-3">
                                                    <input type="text" class="form-control" name="dispensers_id" id="dispenser_id" required>
                                                    <button data-repeater-delete type="button" class="btn btn-danger ml-2"><i class="fa fa-close"></i></button>
                                                </div>
                                            @endforelse
                                        @else
                                            <div data-repeater-item style="flex-direction: row;" class="d-flex mb-3">
                                                <input type="text" class="form-control" name="dispensers_id" id="dispenser_id" required>
                                                <button data-repeater-delete type="button" class="btn btn-danger ml-2"><i class="fa fa-close"></i></button>
                                            </div>
                                        @endif
                                    </div>
                                    <button data-repeater-create type="button" class="btn btn-success border-rounded"><i class="las la-plus mr-2"></i>Add dispenser id</button>
                                </div>
                            </div>
                        </div>
                        @if(!isset($gym))
                            @php
                                $seed = str_split(Illuminate\Support\Str::random(10)
                                    .'!@#^*_'); // and any other characters
                                shuffle($seed); // probably optional since array_is randomized; this may be redundant
                                $password = '';
                                foreach (array_rand($seed, 15) as $k) $password .= $seed[$k];
                            @endphp
                            <div class="col-lg-6 col-md-6">
                                <div class="mb-3 d-none">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="Gym" readonly required>
                                </div>
                                <div class="mb-3 d-none">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" value="Gym Admin" readonly required>
                                </div>
                                <div class="mb-3 d-none">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" @if(isset($user)) value="{{ Illuminate\Support\Str::random(10) }}@gmail.com" @endif readonly required>
                                </div>
                                <div class="mb-3">
                                    <label for="full_name">Name</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name" @if(isset($user)) value="{{ 'Gym Admin' }}" @endif readonly required>
                                </div>
                                <div class="mb-3">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username" @if(isset($user)) value="{{ 'gymadmin' }}" @endif readonly required>
                                </div>
                                <div class="mb-3">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control" name="password" id="password" value="{{ $password }}" readonly required>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6 text-right btn-container">
                            <a href="{{ route('gym.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-secondary">
                                @if(isset($gym)) 
                                    Update 
                                @else
                                    Save
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.js"></script>
    <script>
        $(function() {
            $('.repeater').repeater({
                isFirstItemUndeletable: true,
            });

            let first_name = "";
            let username = "";
            let last_name = "Gym Admin";
            var base_url = $('meta[name="base-url"]').attr('content');

            var slug = function(str) {
                str = str.replace(/^\s+|\s+$/g, ''); // trim
                str = str.toLowerCase();

                // remove accents, swap ñ for n, etc
                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i = 0, l = from.length; i < l; i++) {
                    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                        .replace(/\s+/g, '-') // collapse whitespace and replace by -
                        .replace(/-+/g, '-'); // collapse dashes

                return str;
            };

            $("#name").keyup(function () {
                name = $(this).val();
                username = name.replace(/ /g,'_');
                username = slug(username.toLowerCase()) + "_gym_admin_";
                $("#first_name").val(name);
                $("#full_name").val(`${name} ${last_name}`);
                $("#username").val(username);

                $.ajax({
                    url: `${base_url}/username/check/${username}`, 
                    success: function(result){
                        let extract_num = 0;
                        if(result.username) {
                            extract_num = result.username.replace(/^\D+/g, '');
                            extract_num = parseInt(extract_num);
                        }
                        $("#username").val(username+(extract_num+1));
                        $("#email").val(username+(extract_num+1)+"@gmail.com");
                    }
                });
            });
        });
    </script>
@endsection