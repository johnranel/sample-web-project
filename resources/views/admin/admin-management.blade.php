@extends('layouts.app')

@section('title', 'Admin Management')

@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/user_management/page.css')}}">
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="user_management">
            <div class="col-xl-8 col-lg-6 col-md-8"></div>
            <div class="col-xl-4 col-lg-6 col-md-4 text-right">
                <a href="{{ route('create.admin') }}" class="button-custom"><i class="las la-plus"></i> Add New Admin</a>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <table id="admin-table" class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Role</th>
                                <th>Gym / Location</th>
                                <th>Franchise</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        var dataTable = $('#admin-table').DataTable({
            ajax: `${base_url}/admin-management`,
            language: {
                'paginate': {
                    'previous': '<i class="las la-angle-left"></i>',
                    'next': '<i class="las la-angle-right"></i>'
                }
            },
            columns: [
                {
                    data: 'roles',
                    render: function (data, type, row) {
                        let admin_type = "";
                        if(data[0].name == "SuperAdmin") {
                            admin_type = "Super Admin";
                        } else if(data[0].name == "FranchiseAdmin" || data[0].name == "franchiseAdmin") {
                            admin_type = "Admin";
                        } else if(data[0].name == "GymAdmin"){
                            admin_type = "Facilitator Admin";
                        }

                        return admin_type;
                    }
                },
                {
                    data: 'franchise.name',
                    render: function (data, type, row) {
                        let franchise = "-";
                        if(data) {
                            if(row.roles[0].name == "GymAdmin") {
                                if(row.gym) {
                                    franchise = `${row.gym.name}`;
                                }
                            }
                        }
                        return `<span style="text-transform: capitalize;">${franchise}</span>`;
                    }
                },
                {
                    data: 'franchise.name',
                    render: function (data, type, row) {
                        let franchise = "-";
                        if(data) {
                            if(row.roles[0].name == "FranchiseAdmin" || row.roles[0].name == "GymAdmin") {
                                franchise = data;
                            }
                        }
                        return `<span style="text-transform: capitalize;">${franchise}</span>`;
                    }
                },
                {
                    data: 'first_name',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${row.first_name} ${row.last_name}</span>`;
                    }
                },
                {
                    data: 'username'
                },
                {
                    data: 'uncrypt_password'
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        var editRemove = `<a href="${base_url}/admin/edit/${data}" title="Edit"><i class="las la-user-edit"></i></a>`;
                        editRemove += `<a href="#" onclick="deleteUser(${data})" title="Delete"><i class="las la-trash"></i></a>`;

                        return editRemove;
                    }
                }
            ],
            order: [ [0, 'desc'] ],
            "lengthChange": false,
            searching: false,
            "info": false,
            "bAutoWidth": false,
            responsive: true,
            scrollCollapse: true
        });

        function deleteUser(id) {
            Swal.fire({
                title: 'Are you sure you want to delete this admin?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'/user/delete/'+id,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res){
                                toastr.success('Admin successfully deleted', 'Success!');
                                dataTable.ajax.reload();
                            }
                        },
                        error: function (er) {
                            toastr.error('Deleting admin failed', 'Failed!');
                        }
                    });
                }
            });
        }

        window.addEventListener("beforeunload", function () {
            $("#content").addClass("animate-out");
        });

        @if(session()->has('message'))
            var success_message = "{{ session()->get('message') }}";
            toastr.success(`${success_message}`);
        @endif
    </script>
@endsection
