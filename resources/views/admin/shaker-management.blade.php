@extends('layouts.app')

@section('title', 'Shaker Management')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="gym_franchise_management">
            <div class="col-xl-12 col-lg-12 col-md-12" style="justify-content: end; align-items: end; display: flex;">
                <div class="mr-2" style="display: flex; align-items: center;">
                    <a href="{{ route('create.shaker') }}" class="button-custom"><i class="las la-plus"></i> Add New Shaker</a>
                </div>
                <div style="display: flex; align-items: center;">
                    Show
                    <select class="form-control ml-2 mr-2 dataTable_pageLength">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                    </select>
                    entries
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <table id="shaker-table" class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Shaker ID</th>
                                <th>User Name</th>
                                <th>SS Member ID</th>
                                <th>Price in AUD</th>
                                <th>Cost in AUD</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.4.0/js/dataTables.dateTime.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        var dataTable = $('#shaker-table').DataTable({
            ajax: `${base_url}/shaker-management`,
            language: {
                'paginate': {
                    'previous': '<i class="las la-angle-left"></i>',
                    'next': '<i class="las la-angle-right"></i>'
                }
            },
            columns: [
                {
                    data: 'shaker_id',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'user.first_name',
                    render: function (data, type, row) {
                        if(data == null) {
                            return "-";
                        }
                        return `<span style="text-transform: capitalize;">${data} ${row.user.last_name}</span>`;
                    }
                },
                {
                    data: 'user.id',
                    render: function (data, type, row) {
                        let member_id = "-";
                        if(data != null) {
                            member_id = parseInt(data) + 1000;
                        }
                        return member_id;
                    }
                },
                {
                    data: 'price_in_aud',
                    render: function (data, type, row) {
                        return `AU $${data}.00`;
                    }
                },
                {
                    data: 'cost_in_aud',
                    render: function (data, type, row) {
                        return `AU $${data}.00`;
                    }
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        var editRemove = `<a href="${base_url}/shaker/edit/${data}" title="Edit"><i class="las la-edit"></i></a>`;
                        editRemove += `<a href="#" onclick="deleteShaker(${data})" title="Delete"><i class="las la-trash"></i></a>`;

                        return editRemove;
                    }
                }
            ],
            "lengthChange": false,
            "info": false,
            "bAutoWidth": false,
            searching: false,
            responsive: true,
            scrollCollapse: true
        });

        function deleteShaker(id) {
            Swal.fire({
                title: 'Are you sure you want to delete this shaker?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'/shaker/delete/'+id,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res){
                                toastr.success('Shaker successfully deleted', 'Success!');
                                dataTable.ajax.reload();
                            }
                        },
                        error: function (er) {
                            toastr.error('Deleting shaker failed', 'Failed!');
                        }
                    });
                }
            });
        }

        @if(session()->has('message'))
            var success_message = "{{ session()->get('message') }}";
            toastr.success(`${success_message}`);
        @endif
    </script>
@endsection