@extends('layouts.app')

@section('title', 'User Transactions')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <style>
        .button-custom {
            padding: 0 20px 0 20px !important;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="gym_franchise_management">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-xl-4 col-lg-5 col-md-6" id="top-ups-total-container">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body" style="background-color: #fff; box-shadow: unset;">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-md font-weight-bold text-dark text-uppercase mb-1">
                                            Top Ups (Total)
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-left" id="top-ups-total">AU $ {{ $top_up_total }}.00</div>
                                    </div>
                                    <div class="col-auto">
                                        <a class="top-ups-date-range" role=button>
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6" id="purchases-total-container">
                        <div class="card shadow h-100 py-2">
                            <div class="card-body" style="background-color: #fff; box-shadow: unset;">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-md font-weight-bold text-dark text-uppercase mb-1">
                                            Purchases (Total)
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-left" id="purchases-total">AU $ {{ $purchase_total }}.00</div>
                                    </div>
                                    <div class="col-auto">
                                        <a class="purchases-date-range" role=button>
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 mt-4" style="justify-content: end; align-items: end; display: flex;">
                        <button type="button" class="form-control button-custom" id="pdf-download" style="width: auto !important; float: right;"><i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i> Download as PDF</button>
                        <button type="button" class="form-control mr-2 button-custom" id="excel-download" style="width: auto !important; float: right;"><i class="fa fa-file-excel-o mr-1" aria-hidden="true"></i> Download as Excel Sheet</button>
                        <div style="display: flex; align-items: center;">
                            Show
                            <select class="form-control ml-2 mr-2 dataTable_pageLength">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50" selected="selected">50</option>
                            </select>
                            entries
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <table id="user-transactions-table" class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Dispenser ID</th>
                                <th>Shaker ID</th>
                                <th>Gym / Location</th>
                                <th>Franchise</th>
                                <th>User Name</th>
                                <th>Type of Sale</th>
                                <th>Amount</th>
                                <th>Supplement No.</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="date-range-modal" tabindex="-1" role="dialog" aria-labelledby="dateRangeModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dateRangeModal">Date Range</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Min Date <span class="text-danger">*</span></label>
                        <input type="date" id="min" name="min" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Max Date <span class="text-danger">*</span></label>
                        <input type="date" id="max" name="max" autocomplete="off" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button-custom" id="date-range-filter">Search</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.4.0/js/dataTables.dateTime.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.29.4/moment.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script>
        var topUpsTotal = 0;
        var purchasesTotal = 0;

        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = $("#min").val();
                var max = $("#max").val();
                var type = $("#date-range-filter").attr("data-filterType");
                var date = data[8];
                var dataType = data[5];
                var price = data[6].replace('AU $', '');
                if((( min == "" || max == "" ) || (moment(date).isSameOrAfter(min) && moment(date).isSameOrBefore(max))) && ((type === undefined || type == "") || type === dataType)) {
                    return true;
                }
                return false;
            }
        );

        var base_url = $('meta[name="base-url"]').attr('content');

        window.addEventListener("beforeunload", function () {
            $("#content").addClass("animate-out");
        });

        $(document).ready(function() {
            var dataTable = $('#user-transactions-table').DataTable({
                ajax: `${base_url}/user-transactions`,
                language: {
                    'paginate': {
                        'previous': '<i class="las la-angle-left"></i>',
                        'next': '<i class="las la-angle-right"></i>'
                    }
                },
                columns: [
                    {
                        data: 'created_at',
                        render: function (data, type, row) {
                            let date = moment(data).format('YYYY-MM-DD')
                            return `${date}`;
                        }
                    },
                    {
                        data: 'dispensers_id',
                        render: function (data, type, row) {
                            if(data == null) {
                                data = "-"
                            }
                            return data;
                        }
                    },
                    {
                        data: 'user.shaker.shaker_id',
                        render: function (data, type, row) {
                            let prod_code = "-";
                            if(row.type == "purchase" && (row.shaker_id != null || row.product_id != null)) {
                                prod_code = data;
                            }
                            return prod_code;
                        }
                    },
                    {
                        data: 'gym.address',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${row.gym.name}</span>`;
                        }
                    },
                    {
                        data: 'franchise.name',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${data}</span>`;
                        }
                    },
                    {
                        data: 'user.first_name',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${data} ${row.user.last_name}</span>`;
                        }
                    },
                    {
                        data: 'type',
                        render: function (data, type, row) {
                            let type_of_sale = "-";
                            if(row.type == "purchase" && row.product_id != null) {
                                type_of_sale = "Drink Purchase";
                            } else if(row.type == "purchase" && row.shaker_id != null) {
                                type_of_sale = "Shaker Purchase";
                            } else {
                                type_of_sale = "Top-up";
                            }
                            return `<span style="text-transform: capitalize;">${type_of_sale}</span>`;
                        }
                    },
                    {
                        data: 'amount',
                        render: function (data, type, row) {
                            return `AU $${data}.00`;
                        }
                    },
                    {
                        data: 'product.product_no',
                        render: function (data, type, row) {
                            let prod_code = "-";
                            if(row.type == "purchase" && row.product_id != null) {
                                prod_code = data;
                            }
                            return prod_code;
                        }
                    }
                ],
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'pdf',
                    title: 'Transactions',
                    filename: 'transactions_table_supplement_station'
                }, {
                    extend: 'excel',
                    title: 'Transactions',
                    filename: 'transactions_table_supplement_station'
                }],
                order: [ [2, 'desc'] ],
                "pageLength": 50,
                "lengthChange": false,
                "info": false,
                "bAutoWidth": false,
                responsive: true,
                scrollCollapse: true
            });

            $(".dataTable_pageLength").on('change', function () {
                let table_length = $(this).val();
                dataTable.page.len(table_length).draw();
            });

            $('#date-range-filter').on('click', function () {
                topUpsTotal = 0;
                purchasesTotal = 0;
                dataTable.draw();
            });

            $("#user-transactions-table_filter").remove();

            $(".top-ups-date-range").click(function () {
                $("#date-range-modal").modal("show");
                $("#date-range-filter").attr("data-filterType", "top-up");
                topUpsTotal = 0;
                purchasesTotal = 0;
            });

            $(".purchases-date-range").click(function () {
                $("#date-range-modal").modal("show");
                $("#date-range-filter").attr("data-filterType", "purchase");
                topUpsTotal = 0;
                purchasesTotal = 0;
            });

            $("#pdf-download").click(function (){
                $(".buttons-pdf").trigger('click');
            });

            $("#excel-download").click(function (){
                $(".buttons-excel").trigger('click');
            });
        });
    </script>
@endsection
