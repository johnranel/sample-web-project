@extends('layouts.app')

@section('title', 'Supplements Management')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="product_management">
            @if(auth()->user()->hasRole('SuperAdmin'))
                <div class="col-xl-4 col-lg-2 col-md-4"></div>
                <div class="col-xl-8 col-lg-10 col-md-8" style="justify-content: end; align-items: end; display: flex;">
                    <div class="mr-2" style="display: flex; align-items: center;">
                        <a href="{{ route('create.product') }}" class="button-custom"><i class="las la-plus"></i> Add New Supplement</a>
                    </div>
                    <div style="display: flex; align-items: center;">
                        Show
                        <select class="form-control ml-2 mr-2 dataTable_pageLength">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                        </select>
                        entries
                    </div>
                </div>
            @else
                <div class="col-xl-4 col-lg-2 col-md-4"></div>
                <div class="col-xl-8 col-lg-10 col-md-8 pl-0" style="justify-content: end; align-items: end; display: flex;">
                    <div style="display: flex; align-items: center;">
                        Show
                        <select class="form-control ml-2 mr-2 dataTable_pageLength">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                        </select>
                        entries
                    </div>
                </div>
            @endif
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container mt-4 p-4">
                    <div class="row">
                        @if(isset($dispenser_ids))
                            <div class="col-md-12 d-inline-flex align-items-center">
                                <div>Dispenser</div>
                                <nav class="nav nav-pills nav-fill ml-3" id="nav_pills">
                                    <a class="nav-item nav-link filter-dispenser active" data-filter="All" style="cursor: pointer;">All</a>
                                    @foreach($dispenser_ids as $key => $dispenser_id)
                                        <a class="nav-item nav-link filter-dispenser" data-filter="{{ $dispenser_id }}" style="cursor: pointer;">{{ $dispenser_id }}</a>
                                    @endforeach
                                </nav>
                            </div>
                        @endif
                        @if(!auth()->user()->hasRole('SuperAdmin') || str_contains(Request::path(), 'dashboard/view/admin/franchise/products') || str_contains(Request::path(), 'dashboard/view/admin/gym/products'))
                            <div class="col-xl-3 col-lg-4 col-md-4 m-4 text-center">
                                <img src="{{ $franchise_gym_details->imagePath }}" height="auto" width="100%" class="mb-3" />
                                @if(!auth()->user()->hasRole('FranchiseAdmin'))
                                    @if(str_contains(Request::path(), 'dashboard/view/admin/gym/products'))
                                        <div class="pl-4 pr-4">{{ $gym_details->address }}</div>
                                    @else
                                        <div class="pl-4 pr-4">{{ $franchise_gym_details->address }}</div>
                                    @endif
                                @endif
                            </div>
                        @endif
                        <div class="col-xl-12 col-lg-12 col-md-12 table-responsive">
                            <table id="product-table" class="table" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Supplement No.
                                        </th>
                                        <th>
                                            Brand
                                        </th>
                                        <th>
                                            Dispenser ID
                                        </th>
                                        <th>
                                            Product Name
                                        </th>
                                        <th>
                                            Powder Type
                                        </th>
                                        <th>
                                            Flavour
                                        </th>
                                        <th>
                                            Serving Size in Grams
                                        </th>
                                        <th>
                                            Cost in AUD
                                        </th>
                                        <th>
                                            Price in AUD
                                        </th>
                                        <th>
                                            Price in Points
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.all.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        let current_url = window.location.href;
        var dataTable = $('#product-table').DataTable({
            ajax: `${current_url}`,
            language: {
                'paginate': {
                'previous': '<i class="las la-angle-left"></i>',
                'next': '<i class="las la-angle-right"></i>'
                }
            },
            columns: [
                {
                    data: 'product_no',
                    render: function (data, type, row) {
                        return data;
                    }
                },
                {
                    data: 'manufacturer_name',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'dispenser_product_list',
                    render: function (data, type, row) {
                        let dispenser_id = "";
                        jQuery.each(data, function(index, item) {
                            dispenser_id += `${item.dispensers_id} <br>`
                        });
                        return dispenser_id;
                    }
                },
                {
                    data: 'name',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'type',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'flavour',
                    render: function (data, type, row) {
                        return `<span style="text-transform: capitalize;">${data}</span>`;
                    }
                },
                {
                    data: 'portion_size'
                },
                {
                    data: 'cost_in_aud',
                    render: function (data, type, row) {
                        return `AU $${data}.00`;
                    }
                },
                {
                    data: 'price_in_aud',
                    render: function (data, type, row) {
                        return `AU $${data}.00`;
                    }
                },
                {
                    data: 'price_in_points'
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        var editRemove = `<a href="${base_url}/product/edit/${data}" title="Edit"><i class="las la-edit"></i></a>`;
                        editRemove += `<a href="#" onclick="deleteProduct(${data})" title="Delete"><i class="las la-trash"></i></a>`;

                        return editRemove;
                    }
                }
            ],
            "lengthChange": false,
            "info": false,
            "bAutoWidth": false,
            responsive: true,
            scrollCollapse: true
        });

        function deleteProduct(id) {
            Swal.fire({
                title: 'Are you sure you want to delete this product?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'/product/delete/'+id,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (res) {
                            if(res){
                                toastr.success('Product successfully deleted', 'Success!');
                                dataTable.ajax.reload();
                            }
                        },
                        error: function (er) {
                            toastr.error('Deleting product failed', 'Failed!');
                        }
                    });
                }
            });
        }

        $(".dataTable_pageLength").on('change', function () {
            let table_length = $(this).val();
            dataTable.page.len(table_length).draw();
        });

        $(".filter-dispenser").click(function() {
            $(".filter-dispenser").removeClass("active");
            $(this).addClass("active");
            let filter_data = $(this).attr("data-filter");
            if(filter_data == "All") {
                dataTable.column(2).search("").draw();
            } else {
                dataTable.column(2).search(filter_data).draw();
            }
        });

        dataTable.column(2).visible(false);
        $("#product-table_filter").addClass("d-none");

        window.addEventListener("beforeunload", function () {
            $("#content").addClass("animate-out");
        });

        @if(!auth()->user()->hasRole('SuperAdmin'))
            dataTable.column(9).visible(false);
        @endif

        @if(session()->has('message'))
            var success_message = "{{ session()->get('message') }}";
            toastr.success(`${success_message}`);
        @endif
    </script>
@endsection
