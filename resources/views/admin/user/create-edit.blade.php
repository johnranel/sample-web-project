@extends('layouts.app')

@section('title', 'User')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('user.management') }}" style="color: #000;">Users</a> <i class="fa fa-chevron-right"></i> <a href="@if(isset($user)) {{ route('edit.user', ['user' => $user->id]) }} @else {{ route('create.user') }} @endif" style="color: #000;">
                @if(isset($user))
                    Edit
                @else
                    Create
                @endif
            </a>
        </div>
        <div class="col-md-12" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
            <form action="@if(isset($user)) {{ route('update.user', ['user' => $user->id]) }} @else {{ route('store.user') }} @endif" method="post" enctype="multipart/form-data">
                @csrf
                <div class="" style="padding:20px;">
                    <h5 id="title_text">
                        @if(isset($user)) 
                            Update 
                        @else
                            Add New 
                        @endif
                        User
                    </h5>
                    <h6>
                        All mandatory fields are marked with a red asterisk <span style="color: red;">*</span>.
                    </h6>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <h6 style="font-weight: bold;">
                                Personal Information
                            </h6>
                            @if(isset($user))
                                @if(isset($user->image))
                                    <div class="mb-3">
                                        <img src="{{ $user->image }}" style="object-fit: cover; height: 150px; width: 150px; border-radius: 50%;" />
                                    </div>
                                @endif
                            @endif
                            <div class="mb-3">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" placeholder="image" name="image" id="image">
                            </div>
                            <div class="mb-3">
                                <label for="first_name">First Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="first_name" id="first_name" @if(isset($user)) value="{{ $user->first_name }}" @endif required>
                                @if($errors->has('first_name'))
                                    <span class="text-danger">First Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="last_name">Last Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="last_name" id="last_name" @if(isset($user)) value="{{ $user->last_name }}" @endif required>
                                @if($errors->has('last_name'))
                                    <span class="text-danger">Last Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="email">Email <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" @if(isset($user)) value="{{ $user->email }}" @endif required>
                                @if($errors->has('email'))
                                    <span class="text-danger">Email is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="birthdate">Date of Birth <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" name="birthdate" id="birthdate" @if(isset($user)) value="{{ date('Y-m-d', strtotime($user->birthdate)) }}" @endif required>
                                @if($errors->has('birthdate'))
                                    <span class="text-danger">Birthdate is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="gender">Gender <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="gender" id="gender" required>
                                    <option value="not defined" @if(isset($user)) {!! ($user->gender == 'not defined') ? 'selected' : '' !!} @endif>Not defined</option>
                                    <option value="male" @if(isset($user)) {!! ($user->gender == 'male') ? 'selected' : '' !!} @endif>Male</option>
                                    <option value="female" @if(isset($user)) {!! ($user->gender == 'female') ? 'selected' : '' !!} @endif>Female</option>
                                </select>
                                @if($errors->has('gender'))
                                    <span class="text-danger">Gender is required.</span>
                                @endif
                            </div>
                            <h6 style="font-weight: bold;">
                                Contact Information
                            </h6>
                            <div class="mb-3">
                                <label for="address">Address <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="address" id="address" @if(isset($user)) value="{{ $user->address }}" @endif required>
                                @if($errors->has('address'))
                                    <span class="text-danger">Address is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="mobile_number">Mobile Number <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="mobile_number" id="mobile_number" @if(isset($user)) value="{{ $user->mobile_number }}" @endif required>
                                @if($errors->has('mobile_number'))
                                    <span class="text-danger">Mobile Number is required.</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                            <h6 style="font-weight: bold;">
                                Athlete Profile
                            </h6>
                            <div class="mb-3">
                                <label for="weight">Weight in kg<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="weight" id="weight" @if(isset($user)) value="{{ $user->weight->weight }}" @endif required>
                                @if($errors->has('weight'))
                                    <span class="text-danger">Weight is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="level_of_fitness">Level of Fitness <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="level_of_fitness" required>
                                    <option value="beginner" @if(isset($user)) {!! ($user->athlete_profile->level_of_fitness == "beginner") ? 'selected' : '' !!} @endif>Beginner</option>
                                    <option value="intermediate" @if(isset($user)) {!! ($user->athlete_profile->level_of_fitness == "intermediate") ? 'selected' : '' !!} @endif>Intermediate</option>
                                    <option value="advanced" @if(isset($user)) {!! ($user->athlete_profile->level_of_fitness == "advanced") ? 'selected' : '' !!} @endif>Advanced</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="gym_attendance">Gym Attendance <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="gym_attendance" required>
                                    <option value="casual" @if(isset($user)) {!! ($user->athlete_profile->gym_attendance == "casual") ? 'selected' : '' !!} @endif>Casual</option>
                                    <option value="regular" @if(isset($user)) {!! ($user->athlete_profile->gym_attendance == "regular") ? 'selected' : '' !!} @endif>Regular</option>
                                    <option value="enthusiast" @if(isset($user)) {!! ($user->athlete_profile->gym_attendance == "enthusiast") ? 'selected' : '' !!} @endif>Enthusiast</option>
                                </select>
                            </div>
                            <h6 style="font-weight: bold;">
                                Franchise
                            </h6>
                            <div class="mb-3">
                                <label for="flavours">Franchise <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="franchise_id" required>
                                    <option value=""></option>
                                    @foreach($franchises as $key => $franchise)
                                        <option value="{{ $franchise['id'] }}" @if(isset($user)) {!! ($user->franchise_id == $franchise['id']) ? 'selected' : '' !!} @endif>{{ $franchise['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('franchise_id'))
                                    <span class="text-danger">Franchise is required.</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right btn-container">
                            <a href="{{ route('user.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-secondary">
                                @if(isset($user)) 
                                    Update 
                                @else
                                    Save
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    
@endsection