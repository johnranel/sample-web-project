@extends('layouts.app')

@section('title', 'Franchise')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('shaker.management') }}" style="color: #000;">Shaker</a> <i class="fa fa-chevron-right"></i> <a href="@if(isset($shaker)) {{ route('edit.shaker', ['shaker' => $shaker->id]) }} @else {{ route('create.shaker') }} @endif" style="color: #000;">
                @if(isset($shaker))
                    Edit
                @else
                    Create
                @endif
            </a>
        </div>
        <div class="col-md-12" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
            <form action="@if(isset($shaker)) {{ route('update.shaker', ['shaker' => $shaker->id]) }} @else {{ route('store.shaker') }} @endif" method="post" enctype="multipart/form-data">
                @csrf
                <div style="padding:20px;">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 id="title_text">
                                @if(isset($shaker)) 
                                    Update 
                                @else
                                    Add New 
                                @endif
                                Shaker
                            </h5>
                            <h6>
                                All mandatory fields are marked with a red asterisk <span style="color: red;">*</span>.
                            </h6>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="shaker_id">Shaker ID <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="shaker_id" id="shaker_id" @if(isset($shaker)) value="{{ $shaker->shaker_id }}" @endif required>
                                @if($errors->has('shaker_id'))
                                    <span class="text-danger">Shaker ID is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="price_in_aud">Price in AUD <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="price_in_aud" id="price_in_aud" @if(isset($shaker)) value="{{ $shaker->price_in_aud }}" @endif required>
                                @if($errors->has('price_in_aud'))
                                    <span class="text-danger">Price in AUD is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="cost_in_aud">Cost in AUD <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" name="cost_in_aud" id="cost_in_aud" @if(isset($shaker)) value="{{ $shaker->cost_in_aud }}" @endif required>
                                @if($errors->has('cost_in_aud'))
                                    <span class="text-danger">Cost in AUD is required.</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 text-right btn-container">
                            <a href="{{ route('shaker.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-secondary">
                                @if(isset($shaker)) 
                                    Update 
                                @else
                                    Save
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
        $(function() {
            let first_name = "";
            let username = "";
            let last_name = "Franchise Admin";
            var base_url = $('meta[name="base-url"]').attr('content');

            var slug = function(str) {
                str = str.replace(/^\s+|\s+$/g, ''); // trim
                str = str.toLowerCase();

                // remove accents, swap ñ for n, etc
                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i = 0, l = from.length; i < l; i++) {
                    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                        .replace(/\s+/g, '-') // collapse whitespace and replace by -
                        .replace(/-+/g, '-'); // collapse dashes

                return str;
            };

            $("#name").keyup(function () {
                name = $(this).val();
                username = name.replace(/ /g,'_');
                username = slug(username.toLowerCase()) + "_shaker_admin_";
                $("#first_name").val(name);
                $("#full_name").val(`${name} ${last_name}`);
                $("#username").val(username);

                $.ajax({
                    url: `${base_url}/username/check/${username}`, 
                    success: function(result){
                        let extract_num = 0;
                        if(result.username) {
                            extract_num = result.username.replace(/^\D+/g, '');
                            extract_num = parseInt(extract_num);
                        }
                        $("#username").val(username+(extract_num+1));
                        $("#email").val(username+(extract_num+1)+"@gmail.com");
                    }
                });
            });
        });
    </script>
@endsection