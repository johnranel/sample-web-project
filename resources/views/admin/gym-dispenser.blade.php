@extends('layouts.app')

@section('title', 'Dispenser Management')
@section('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <style>
        @media (min-width: 992px) {
            .button-custom {
                padding: 0 20px 0 20px !important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        @include('common.view-admin-switch')
        @include('checklist.index')
        <!-- Content Row -->
        <div class="row col-xl-12 col-lg-12 col-md-12 mt-4 mb-5 logs" id="gym_franchise_management">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-container table-responsive mt-4 p-4">
                    <div>Note: New Dispensers are added when creating a new gym/location.</div>
                    <table id="gym-dispenser-table" class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Dispenser ID</i></th>
                                <th>Gym / Location</i></th>
                                <th>Franchise</i></th>
                                <th>Assign Products</i></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.4.0/js/dataTables.dateTime.min.js"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        $(document).ready(function() { 
            var dataTable = $('#gym-dispenser-table').DataTable({
                ajax: `${base_url}/dispenser-management`,
                language: {
                    'paginate': {
                        'previous': '<i class="las la-angle-left"></i>',
                        'next': '<i class="las la-angle-right"></i>'
                    }
                },
                columns: [
                    {
                        data: 'dispensers_id',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${data}</span>`;
                        }
                    },
                    {
                        data: 'gym.name',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${data}</span>`;
                        }
                    },
                    {
                        data: 'gym.gym_franchise.name',
                        render: function (data, type, row) {
                            return `<span style="text-transform: capitalize;">${data}</span>`;
                        }
                    },
                    {
                        data: 'id',
                        render: function (data, type, row) {
                            return `<a href="${base_url}/dispenser/assign/products/${data}" title="Edit"><i class="las la-edit"></i></a>`;
                        }
                    }
                ],
                "lengthChange": false,
                "info": false,
                "bAutoWidth": false,
                searching: false,
                responsive: true,
                scrollCollapse: true
            });
        });
    </script>
@endsection