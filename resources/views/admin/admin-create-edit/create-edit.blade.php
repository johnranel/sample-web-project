@extends('layouts.app')

@section('title', 'Admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('user.management') }}" style="color: #000;">Admin</a> <i class="fa fa-chevron-right"></i> <a href="@if(isset($user)) {{ route('edit.admin', ['user' => $user->id]) }} @else {{ route('create.admin') }} @endif" style="color: #000;">
                @if(isset($user))
                    Edit
                @else
                    Create
                @endif
            </a>
        </div>
        <div class="col-md-12" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
            <form action="@if(isset($user)) {{ route('update.admin', ['user' => $user->id]) }} @else {{ route('store.admin') }} @endif" method="post" enctype="multipart/form-data">
                @csrf
                <div class="" style="padding:20px;">
                    <h5 id="title_text">
                        @if(isset($user)) 
                            Update 
                        @else
                            Add New 
                        @endif
                        Admin
                    </h5>
                    <h6>
                        All mandatory fields are marked with a red asterisk <span style="color: red;">*</span>.
                    </h6>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($user))
                                @if(isset($user->image))
                                    <div class="mb-3">
                                        <img src="{{ $user->image }}" style="object-fit: cover; height: 150px; width: 150px; border-radius: 50%;" />
                                    </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" placeholder="image" name="image" id="image">
                            </div>
                            <div class="mb-3">
                                <label for="first_name">First Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="first_name" id="first_name" @if(isset($user)) value="{{ $user->first_name }}" @endif required>
                                @if($errors->has('first_name'))
                                    <span class="text-danger">First Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="last_name">Last Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="last_name" id="last_name" @if(isset($user)) value="{{ $user->last_name }}" @endif required>
                                @if($errors->has('last_name'))
                                    <span class="text-danger">Last Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3 d-none">
                                <label for="mobile_number">Mobile Number <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="mobile_number" id="mobile_number" @if(isset($user)) value="{{ $user->mobile_number }}" @else value="-" @endif required>
                                @if($errors->has('mobile_number'))
                                    <span class="text-danger">Mobile Number is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="username">Username <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="username" id="username" @if(isset($user)) value="{{ $user->username }}" @endif required>
                                @if($errors->has('username'))
                                    <span class="text-danger">Username is required.</span>
                                @endif
                            </div>
                            @if(!isset($user))
                                <div class="mb-3">
                                    <label for="password">Password <span style="color: red;">*</span></label>
                                    <input type="password" class="form-control" name="password" id="password" @if(isset($user)) value="{{ $user->password }}" @endif required>
                                    @if($errors->has('password'))
                                        <span class="text-danger">Password is required.</span>
                                    @endif
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 d-none">
                                <label for="email">Email <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" @if(isset($user)) value="{{ $user->email }}" @else value="{{ Illuminate\Support\Str::random(10) . '@gmail.com' }}" @endif required>
                                @if($errors->has('email'))
                                    <span class="text-danger">Email is required.</span>
                                @endif
                            </div>
                            <div class="mb-3 d-none">
                                <label for="gender">Gender <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="gender" id="gender" required>
                                    <option value="not defined" @if(isset($user)) {!! ($user->gender == 'not defined') ? 'selected' : '' !!} @endif>Not defined</option>
                                    <option value="male" @if(isset($user)) {!! ($user->gender == 'male') ? 'selected' : '' !!} @endif>Male</option>
                                    <option value="female" @if(isset($user)) {!! ($user->gender == 'female') ? 'selected' : '' !!} @endif>Female</option>
                                </select>
                                @if($errors->has('gender'))
                                    <span class="text-danger">Gender is required.</span>
                                @endif
                            </div>
                            <div class="mb-3 d-none">
                                <label for="birthdate">Birthdate <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" name="birthdate" id="birthdate" @if(isset($user)) value="{{ date('Y-m-d') }}" @else value="{{ date('Y-m-d') }}" @endif required>
                                @if($errors->has('birthdate'))
                                    <span class="text-danger">Birthdate is required.</span>
                                @endif
                            </div>
                            <div class="mb-4">
                                <label>Role <span style="color: red;">*</span></label> <br>
                                <input type="radio" name="roles" id="SuperAdmin" value="SuperAdmin" @if(isset($user)) {!! ($user->roles[0]->name == "SuperAdmin") ? 'checked=checked' : '' !!} @endif required>
                                <label for="SuperAdmin" class="mr-3 text-xs">Super Admin</label>
                                <input type="radio" name="roles" id="FranchiseAdmin" value="FranchiseAdmin" @if(isset($user)) {!! ($user->roles[0]->name == "FranchiseAdmin") ? 'checked=checked' : '' !!} @endif required>
                                <label for="FranchiseAdmin" class="mr-3 text-xs">Admin</label>
                                <input type="radio" name="roles" id="GymAdmin" value="GymAdmin" @if(isset($user)) {!! ($user->roles[0]->name == "GymAdmin") ? 'checked=checked' : '' !!} @endif required>
                                <label for="GymAdmin" class="text-xs">Facilitator Admin</label>
                            </div>
                            <div class="mb-3">
                                <label for="franchise_id">Franchise <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="franchise_id" id="franchise_id" required>
                                    <option value=""></option>
                                    @foreach($franchises as $key => $franchise)
                                        <option value="{{ $franchise['id'] }}" @if(isset($user)) {!! ($user->franchise_id == $franchise['id']) ? 'selected' : '' !!} @endif>{{ $franchise['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('gym_id'))
                                    <span class="text-danger">Franchise is required.</span>
                                @endif
                            </div>
                            <div class="mb-3 gym-field-container @if(isset($user)) @if($user->roles[0]->name == 'SuperAdmin' || $user->roles[0]->name == 'FranchiseAdmin') d-none @endif @endif" @if(isset($user)) data-gym_id="{{$user->gym_id}}" @endif>
                                <label for="gym_id">Gym <span style="color: red;">*</span></label>
                                <select class="form-control mr-2" name="gym_id" id="gym_id" @if(isset($user)) @if($user->roles[0]->name != 'SuperAdmin' || $user->roles[0]->name != 'FranchiseAdmin') required="required" @endif @endif>
                                    <option value=""></option>
                                </select>
                                @if($errors->has('gym_id'))
                                    <span class="text-danger">Gym is required.</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right btn-container">
                            <a href="{{ route('admin.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-secondary">
                                @if(isset($user)) 
                                    Update 
                                @else
                                    Save
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        var gym_id = $(".gym-field-container").attr("data-gym_id");
        $("input[type=radio]").click(function() {
            if($(this).val() == "GymAdmin") {
                $(".gym-field-container").removeClass("d-none");
                $("select[name='gym_id']").attr("required",true);
                
            } else {
                $(".gym-field-container").addClass("d-none");
                $("select[name='gym_id']").removeAttr("required");
            }
        });

        $("#franchise_id").change(function() {
            $(".rm-option").remove();
            $.ajax({
                method: "get",
                url: `${base_url}/franchise/gyms/fetch/`+$(this).val(), 
                success: function(result){
                    $.each(result, function(key,valueObj){
                        let selected = "";
                        if(valueObj.id == gym_id) {
                            selected = "selected"
                        }
                        $("#gym_id").append(`<option class="rm-option" value="${valueObj.id}" ${selected}>${valueObj.name}</option>`);
                    });
                }
            });
        });

        $("#franchise_id").trigger('change');
    </script>
@endsection