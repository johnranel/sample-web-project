@extends('layouts.app')

@section('title', 'Dashboard')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/dashboard_index/page.css')}}">
@endsection

@section('content')
    <input type="text" class="d-none" value="{{ $male }}" id="male">
    <input type="text" class="d-none" value="{{ $female }}" id="female">
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4 ml-2">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>

        <div class="row col-md-12">
                
            <div class="col-md-3">
                <div class="card" style="border:unset; background-color:unset; align-items: left;">
                    <div class="card-body d-block" id="total-user">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-s mb-1">
                                    Total Users</div>
                                <div class="h5 mb-0">{{ $users }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6"></div>
            
            <div class="col-md-3">
                <h4 class="text-gray-800" style="text-align:center;">User Gender Chart</h4>
                <canvas id="myChart" width="200" height="200"></canvas>
            </div>

        </div>
        <div class="col-md-9">
            <nav class="nav nav-pills nav-fill" id="nav_pills">
                <a class="nav-item nav-link active" href="javascript:void(0);">User</a>
            </nav>

        </div>
        <div class="row col-md-12 mt-5 mb-5">
            <div class="col-md-12 logs" id="user_logs">
                <div class="row align-items-center mb-2">
                    <div class="col-md-8 d-flex">
                        <h4 class="text-gray-800 mr-3 my-1">User Logs</h4>
                        <select name="" id="filter_by_type" class="form-control mr-2" style="width: 150px; ">
                            <option>Likes</option>
                            <option>Time Spent</option>
                            <option>Viewers</option>
                        </select>
                    </div>
                    <div class="col-md-4 d-flex" style="text-align:end;">
                        <input type="text" placeholder="Search user log" class="form-control" id="search_user_log">
                    </div>
                </div>
                <br>
                <table id="user-log-table">

                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js" referrerpolicy="no-referrer"></script>
    
    <script src="/theme/dist/default/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="/theme/dist/default/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <script>
        var base_url = $('meta[name="base-url"]').attr('content');
        $(function(){
            $('.dropdown-toggle').dropdown();
            
            var search_log = $('#search_user_log').val();
            getUserLogList(search_log);

            $('#search_user_log').keyup(function() {
                var search_log = $('#search_user_log').val();
                getUserLogList(search_log);
            });

            function getUserLogList(search=''){
                var user_log_datatable = $('#user-log-table').mDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                            method: 'GET',
                            url: base_url+'/ajax/log/list/user?search_log_user='+search,
                                map: function(raw) {
                                    var dataSet = raw;

                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        saveState: {
                            cookie: true,
                            webstorage: true
                        },
                        },
                        layout: {
                            theme: 'default',
                            class: 'table',
                            scroll: false,

                            icons: {
                                sort: {asc: 'fa fa-chevron-up', desc: 'fa fa-chevron-down'}
                            }
                        },
                    
                        sortable: true,
                        pagination: true,
                        toolbar: {
                            items: {
                                pagination: {
                                    pageSizeSelect: [10, 25],
                                    navigation: {
                                        first: false,
                                        last: false
                                    }
                                },
                                info: false,
                            },
                        },
                        
                        translate:{
                            records: {
                                processing: '',
                                noRecords: '<span id="norecord"> No records found! </span>',
                            },
                            toolbar:{
                                pagination: {
                                    items:{
                                        info: 'viewList'
                                    }
                                }
                            }
                        },    

                        search: {
                            input: $('#search_log_consultant'),
                        },

                        columns: [
                            {
                                field: 'space',
                                title: '',
                                sortable:false,
                                template: function (row) {
                                    return '';
                                }
                            },
                            {
                                field: 'logs',
                                title: 'Logs',
                                template: function (row) {
                                    return row.logs;
                                }
                            },{
                                field: 'name',
                                title: 'Name',
                                template: function (row) {
                                    return row.name;
                                }
                            },{
                                field: 'action',
                                title: 'Action',
                                template: function (row) {
                                    return row.action;
                                }
                            }, {
                                field: 'description',
                                title: 'Description',
                                sortable:false,
                                template: function(row) {
                                    return row.description;
                                }
                            },
                            {
                                field: 'date',
                                title: 'Date',
                                template: function(row) {
                                    return row.date;
                                }
                            },
                            {
                                field: 'time_log',
                                title: 'Time Logged',
                                template: function(row) {
                                    return row.time_log;
                                }
                            },
                            {
                                field: 'user_type',
                                title: 'User Type',
                                sortable:false,
                                template: function(row) {
                                    return row.user_type;
                                }
                            },
                            {
                                field: '',
                                title: '',
                                sortable:false,
                                template: function(row) {
                                    return '';
                                }
                            }
                        ]
                    });

                return user_log_datatable;
            }
        });

        $('.nav-link').on('click',function(){
            $('.nav-link').removeClass('active'); 
            $(this).addClass('active'); 
            $('.logs').addClass('d-none');
            var navs = $(this).text();
            $('#user_logs').fadeOut();
            if(navs=='User') {
                $('#user_logs').fadeIn();
                $('#user_logs').removeClass('d-none');
            }
        });

        $(function(){
            var ctx = document.getElementById('myChart');
            var male = $('#male').val();
            var female = $('#female').val();
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: [
                        'Male',
                        'Female'
                    ],
                    datasets: [{
                            label: 'My First Dataset',
                            data: [male, female],
                            backgroundColor: [
                            '#C6C6C6',
                            '#ADACAD'
                            ],
                            hoverOffset: 4
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            position:'right',
                            display: true
                        }
                    }
                }

            });
        });

        window.addEventListener("beforeunload", function () {
            $("#content").addClass("animate-out");
        });
    </script>
@endsection