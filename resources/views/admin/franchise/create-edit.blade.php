@extends('layouts.app')

@section('title', 'Franchise')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/product_management/page.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 mb-3">
            <a href="{{ route('franchise.management') }}" style="color: #000;">Franchise</a> <i class="fa fa-chevron-right"></i> <a href="@if(isset($gymfranchise)) {{ route('edit.franchise', ['gymfranchise' => $gymfranchise->id]) }} @else {{ route('create.franchise') }} @endif" style="color: #000;">
                @if(isset($gymfranchise))
                    Edit
                @else
                    Create
                @endif
            </a>
        </div>
        <div class="col-md-12" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
            <form action="@if(isset($gymfranchise)) {{ route('update.franchise', ['gymfranchise' => $gymfranchise->id]) }} @else {{ route('store.franchise') }} @endif" method="post" enctype="multipart/form-data">
                @csrf
                <div style="padding:20px;">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 id="title_text">
                                @if(isset($gymfranchise)) 
                                    Update 
                                @else
                                    Add New 
                                @endif
                                Franchise
                            </h5>
                            <h6>
                                All mandatory fields are marked with a red asterisk <span style="color: red;">*</span>.
                            </h6>
                            <br>
                        </div>
                        @if(!isset($gymfranchise))
                            <div class="col-md-6">
                                <h5 id="title_text"> Franchise Admin </h5>
                                <h6>
                                    Automatically generated.
                                </h6>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($gymfranchise))
                                @if(isset($gymfranchise->imagePath))
                                    <div class="mb-3">
                                        <img src="{{ $gymfranchise->imagePath }}" style="object-fit: cover; height: 100px;" />
                                    </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="image">Franchise Logo</label>
                                <input type="file" class="form-control" placeholder="image" name="file" id="image">
                            </div>
                            <div class="mb-3">
                                <label for="name">Franchise Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" @if(isset($gymfranchise)) value="{{ $gymfranchise->name }}" @endif required>
                                @if($errors->has('name'))
                                    <span class="text-danger">Name is required.</span>
                                @endif
                            </div>
                            <div class="mb-3 d-none">
                                <label for="description">Description <span style="color: red;">*</span></label>
                                <textarea class="form-control" rows="5" cols="50" name="description" id="description" required>@if(isset($gymfranchise)){{$gymfranchise->description}}@else - @endif</textarea>
                                @if($errors->has('description'))
                                    <span class="text-danger">Description is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="address">Headquarter Address <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="address" id="address" @if(isset($gymfranchise)) value="{{ $gymfranchise->address }}" @endif required>
                                @if($errors->has('address'))
                                    <span class="text-danger">Headquarter Address is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="phone">Phone <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="phone" id="phone" @if(isset($gymfranchise)) value="{{ $gymfranchise->phone }}" @endif required>
                                @if($errors->has('phone'))
                                    <span class="text-danger">Phone is required.</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="email">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="email" @if(isset($gymfranchise)) value="{{ $gymfranchise->email }}" @endif required>
                                @if($errors->has('email'))
                                    <span class="text-danger">Email is required.</span>
                                @endif
                            </div>
                        </div>
                        @if(!isset($gymfranchise))
                            @php
                                $seed = str_split(Illuminate\Support\Str::random(10)
                                    .'!@#^*_'); // and any other characters
                                shuffle($seed); // probably optional since array_is randomized; this may be redundant
                                $password = '';
                                foreach (array_rand($seed, 15) as $k) $password .= $seed[$k];
                            @endphp
                            <div class="col-lg-6 col-md-6">
                                <div class="mb-3 d-none">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="Franchise" readonly required>
                                </div>
                                <div class="mb-3 d-none">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" value="Franchise Admin" readonly required>
                                </div>
                                <div class="mb-3 d-none">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email_admin" id="email" @if(isset($user)) value="{{ 'franchiseadmin' }}@gmail.com" @endif readonly required>
                                </div>
                                <div class="mb-3">
                                    <label for="full_name">Name</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name" @if(isset($user)) value="{{ 'FRANCHISE Admin' }}" @endif readonly required>
                                </div>
                                <div class="mb-3">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username" @if(isset($user)) value="{{ 'franchiseadmin' }}" @endif readonly required>
                                </div>
                                <div class="mb-3">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control" name="password" id="password" value="{{ $password }}" readonly required>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6 text-right btn-container">
                            <a href="{{ route('franchise.management') }}" type="button" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-secondary">
                                @if(isset($gymfranchise)) 
                                    Update 
                                @else
                                    Save
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
        $(function() {
            let first_name = "";
            let username = "";
            let last_name = "Franchise Admin";
            var base_url = $('meta[name="base-url"]').attr('content');

            var slug = function(str) {
                str = str.replace(/^\s+|\s+$/g, ''); // trim
                str = str.toLowerCase();

                // remove accents, swap ñ for n, etc
                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i = 0, l = from.length; i < l; i++) {
                    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                        .replace(/\s+/g, '-') // collapse whitespace and replace by -
                        .replace(/-+/g, '-'); // collapse dashes

                return str;
            };

            $("#name").keyup(function () {
                name = $(this).val();
                username = name.replace(/ /g,'_');
                username = slug(username.toLowerCase()) + "_franchise_admin_";
                $("#first_name").val(name);
                $("#full_name").val(`${name} ${last_name}`);
                $("#username").val(username);

                $.ajax({
                    url: `${base_url}/username/check/${username}`, 
                    success: function(result){
                        let extract_num = 0;
                        if(result.username) {
                            extract_num = result.username.replace(/^\D+/g, '');
                            extract_num = parseInt(extract_num);
                        }
                        $("#username").val(username+(extract_num+1));
                        $("#email").val(username+(extract_num+1)+"@gmail.com");
                    }
                });
            });
        });
    </script>
@endsection