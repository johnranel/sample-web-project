@if(auth()->user()->hasRole('SuperAdmin'))
<div class="row col-xl-12 col-lg-12 col-md-12">
    <div class="col-xl-7 col-lg-9 col-md-11">
        <nav class="nav nav-pills nav-fill" id="nav_pills">
            <a class="nav-item nav-link @if(!str_contains(Request::path(), 'dashboard/view/admin/franchise/transactions') && !str_contains(Request::path(), 'dashboard/view/admin/franchise/products') && !str_contains(Request::path(), 'dashboard/view/admin/gym/transactions') && !str_contains(Request::path(), 'dashboard/view/admin/gym/products')) active @endif" href="{{ route('user.transactions') }}">Super Admin <br> <span style="font-size: 10px;">Owner</a>
            <a class="nav-item nav-link franchise-selector-modal @if(str_contains(Request::path(), 'dashboard/view/admin/franchise/transactions') || str_contains(Request::path(), 'dashboard/view/admin/franchise/products')) active @endif" style="cursor: pointer;">Admin <br> <span style="font-size: 10px;">Gym Franchises</span></a>
            <a class="nav-item nav-link gym-selector-modal @if(str_contains(Request::path(), 'dashboard/view/admin/gym/transactions') || str_contains(Request::path(), 'dashboard/view/admin/gym/products')) active @endif" style="cursor: pointer;">Facilitator Admin <br> <span style="font-size: 10px;">Gyms</span></a>
        </nav>
    </div>
</div>
@endif