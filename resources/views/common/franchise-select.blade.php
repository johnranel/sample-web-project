<div class="modal fade" id="franchise-selector-modal" tabindex="-1" role="dialog" aria-labelledby="franchiseSelectorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="franchiseSelectorModal">Select Franchise</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="fa fa-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select class="form-control select-franchise">
                        @foreach($franchises as $key => $franchise)
                        <option value="{{ $franchise->id }}">{{ $franchise->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button class="button-custom" id="view-dashboard-franchise-btn">View</button>
            </div>
        </div>
    </div>
</div>