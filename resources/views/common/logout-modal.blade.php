<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <button class="btn btn-secondary mr-2 px-5" onClick="window.location.href = '{{ route('logout') }}'"> Logout </button>
                <button class="btn btn-secondary px-5" type="button" data-dismiss="modal">Cancel</button>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
