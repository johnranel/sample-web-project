<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion col-sm-2" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <div class="sidebar-brand d-flex align-items-center justify-content-center mb-5 mt-4" href="#">
        <h3 class="side-bar-logo">MuscleMart</h3>
    </div>
    <div class="burger-div fas fa-align-justify"></div>
    <!-- Nav Item - Dashboard -->
    <div class="mt-5" style="box-shadow: 5px 5px 10px #888888; border-radius: 2em; background-color: #fff;">
        @if(!str_contains(Request::path(), 'dashboard/view/admin/franchise/transactions') && !str_contains(Request::path(), 'dashboard/view/admin/franchise/products') && !str_contains(Request::path(), 'dashboard/view/admin/gym/transactions') && !str_contains(Request::path(), 'dashboard/view/admin/gym/products'))
            @if(auth()->user()->hasRole(['SuperAdmin', 'FranchiseAdmin', 'GymAdmin']))
                <li class="nav-item pt-3 {{ (Request::path() ==  'franchise-and-gym-transactions' || Request::path() ==  'gym-transactions' || Request::path() ==  'user-transactions') ? 'active' : ''  }}">
                    <a class="nav-link" href="@if(auth()->user()->hasRole('FranchiseAdmin')) {{ route('franchise.and.gym.transactions') }} @elseif(auth()->user()->hasRole('GymAdmin')) {{ route('gym.transactions') }} @elseif(auth()->user()->hasRole('SuperAdmin')) {{ route('user.transactions') }} @endif">
                        <span>Transactions</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->hasRole('SuperAdmin'))
                <li class="nav-item {{ (Request::path() ==  'user-management' || str_contains(Request::path(), 'user/edit')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('user.management') }}">
                        <span>User Profiles</span>
                    </a>
                </li>
                <li class="nav-item {{ (Request::path() ==  'dispenser-management' || str_contains(Request::path(), 'dispenser/assign/products')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('dispenser.management') }}">
                        <span>Dispensers</span>
                    </a>
                </li>
                <li class="nav-item {{ (Request::path() ==  'shaker-management' || Request::path() ==  'shaker/create' || str_contains(Request::path(), 'shaker/edit')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('shaker.management') }}">
                        <span>Shakers</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->hasRole(['SuperAdmin', 'FranchiseAdmin', 'GymAdmin']))
                <li class="nav-item {{ (Request::path() ==  'product-management' || Request::path() ==  'product/create' || str_contains(Request::path(), 'product/edit')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('product.management') }}">
                        <span>Supplements</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->hasRole('SuperAdmin'))
                <li class="nav-item {{ (Request::path() ==  'franchise-management' || Request::path() ==  'franchise/create' || str_contains(Request::path(), 'franchise/edit')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('franchise.management') }}">
                        <span>Franchises</span>
                    </a>
                </li>
                <li class="nav-item {{ (Request::path() ==  'gym-management' || Request::path() == 'gym/create' || str_contains(Request::path(), 'gym/edit')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('gym.management') }}">
                        <span>Gyms</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::path() ==  'settings-management' ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('settings') }}">
                        <span>Settings</span>
                    </a>
                </li>
                <li class="nav-item {{ (Request::path() ==  'admin-management' || Request::path() == 'admin/create' || str_contains(Request::path(), 'admin/edit')) ? 'active' : ''  }}">
                    <a class="nav-link" href="{{ route('admin.management') }}">
                        <span>Admin Profiles</span>
                    </a>
                </li>
            @endif
        @elseif(str_contains(Request::path(), 'dashboard/view/admin/franchise/transactions') || str_contains(Request::path(), 'dashboard/view/admin/franchise/products'))
            <li class="nav-item pt-3 {{ str_contains(Request::path(), 'dashboard/view/admin/franchise/transactions') ? 'active' : ''  }}">
                <a class="nav-link" href="{{ URL('/') . '/dashboard/view/admin/franchise/transactions/' . $franchise_details->id }}">
                    <span>Transactions</span>
                </a>
            </li>
            <li class="nav-item {{ str_contains(Request::path(), 'dashboard/view/admin/franchise/products') ? 'active' : ''  }}">
                <a class="nav-link" href="{{ URL('/') . '/dashboard/view/admin/franchise/products/' . $franchise_details->id }}">
                    <span>Supplements</span>
                </a>
            </li>
        @elseif(str_contains(Request::path(), 'dashboard/view/admin/gym/transactions') || str_contains(Request::path(), 'dashboard/view/admin/gym/products'))
            <li class="nav-item pt-3 {{ str_contains(Request::path(), 'dashboard/view/admin/gym/transactions') ? 'active' : ''  }}">
                <a class="nav-link" href="{{ URL('/') . '/dashboard/view/admin/gym/transactions/' . $gym_details->id }}">
                    <span>Transactions</span>
                </a>
            </li>
            <li class="nav-item {{ str_contains(Request::path(), 'dashboard/view/admin/gym/products') ? 'active' : ''  }}">
                <a class="nav-link" href="{{ URL('/') . '/dashboard/view/admin/gym/products/' . $gym_details->id }}">
                    <span>Supplements</span>
                </a>
            </li>
        @endif
    </div>
</ul>
