<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle"src="{{asset('images/profile.png')}}" width="54" height="50">
                <!-- <span class="mr-2 d-none d-lg-inline text-gray-600" style="color:#000; background-color:#fff;">{{ ucfirst(Auth()->user()->first_name) . ' ' . ucfirst(Auth()->user()->last_name) }}</span> -->
                <span class="mr-2 d-none d-lg-inline text-gray-600" style="color:#000; background-color:#fff;">
                    @if(auth()->user()->hasRole('GymAdmin'))
                        Facilitator Admin
                    @elseif(auth()->user()->hasRole('FranchiseAdmin'))
                        Admin
                    @elseif(auth()->user()->hasRole('SuperAdmin'))
                        Super Admin
                    @endif
                </span>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="userDropdown" style="text-align:center;">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
