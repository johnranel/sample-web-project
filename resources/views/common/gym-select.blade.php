<div class="modal fade" id="gym-selector-modal" tabindex="-1" role="dialog" aria-labelledby="gymSelectorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="gymSelectorModal">Select Gym</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="fa fa-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select class="form-control select-gym">
                        @foreach($gyms as $key => $gym)
                        <option value="{{ $gym->id }}">{{ $gym->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button class="button-custom" id="view-dashboard-gym-btn">View</button>
            </div>
        </div>
    </div>
</div>