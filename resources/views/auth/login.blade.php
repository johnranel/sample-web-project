@extends('auth.layouts.app')

@section('title', 'Login')

@section('scripts')
 <script>
    function password_show_hide() {
        var x = document.getElementById("password");
        var show_eye = document.getElementById("show_eye");
        var hide_eye = document.getElementById("hide_eye");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
</script>
@endsection

@section('styles')
    <style>
        @font-face {
            font-family: 'MontserratLight';
            src: url('/fonts/MontserratLight.ttf');
        }

        @font-face {
            font-family: 'MontserratRegular';
            src: url('/fonts/MontserratRegular.ttf');
        }

        @font-face {
            font-family: 'MontserratBold';
            src: url('/fonts/MontserratBold.ttf');
        }

        .p-viewer {
            z-index: 9999;
            position: absolute;
            top: 30%;
            right: 10px;
        }

        .fa-eye {
            color: #C9EA3B;
        }

        .logo {
            height: 250px;
        }

        .card-body {
            box-shadow: 0 10px 25px 0 rgb(0 0 0 / 5%), 0 6px 30px 0 rgb(0 0 0 / 10%);
            background-image: -webkit-linear-gradient(#fff, #fff, #fff);
        }

        .btn-info:hover {
            background-image: -webkit-linear-gradient(#fff, #fff, #fff);
            color: #000;
        }

        .btn-info {
            color: #ffffff;
            box-shadow: 0 10px 25px 0 rgb(0 0 0 / 5%), 0 6px 30px 0 rgb(0 0 0 / 20%);
            background-image: -webkit-linear-gradient(#000, #000, #000);
            border-radius: 2em !important;
            border: 2px solid #C9EA3B !important;
            padding: 5px;
            width: 97%;
        }

        .text-dark {
            color: #000 !important;
        }

        .form-control:focus {
            box-shadow: unset !important;
        }

        .form-control {
            border-radius: 2rem;
            border: 2px solid #C9EA3B !important;
        }

        .input-group-text {
            border-radius: 2rem;
            border: 2px solid #C9EA3B !important;
            background-color: #000 !important;
        }

        body {
            font-family: 'MontserratRegular';
            color: #000;
            background-image: -webkit-linear-gradient(#ffffff, #dedede, #bababa);
        }

        .card {
            background-color: transparent;
        }
    </style>
@endsection
@section('content')
<div class="row justify-content-center mx-auto">

    <div class="col-xl-12 text-center pt-5 mt-5">
        <img src="{{ asset('images/supplement-station-logo-2.PNG') }}" class="logo" alt="logo">
    </div>
    <div class="col-xl-8 col-lg-12 col-md-8">
        <div class="card o-hidden border-0 my-4">
            <form method="post" action="{{ route('login') }}">
                <div class="card-body p-0 mt-4 ml-4 mr-4 mb-3">
                    <!-- Nested Row within Card Body -->
                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <div class="py-4">
                                <h4 class="text-dark" style="font-family: 'MontserratBold'">Admin Login</h4>
                                @if (session('error'))
                                    <span class="text-danger"> {{ session('error') }}</span>
                                @endif
                                @csrf
                                <div class="form-group">
                                    @if ($errors->has('credential_error'))
                                        <span class="text-danger"> {{ $errors->first('credential_error') }}</span>
                                    @endif
                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" style="border: 1px solid #80C8BC" autofocus placeholder="Username">
                                    @if($errors->has('username'))
                                        <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                    <input name="password" type="password" value="" class="input form-control form-control form-control-user @error('password') is-invalid @enderror" id="password"  placeholder="Password" aria-label="password" aria-describedby="basic-addon1" />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="password_show" onclick="password_show_hide();">
                                        <i class="fas fa-eye" id="show_eye"></i>
                                        <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                        </span>
                                    </div>
                                    </div>

                                    @if($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col text-center pb-5">
                    <button class="btn btn-info">
                        Login
                    </button>
                </div>
            </form>
        </div>
    </div>

</div>


@endsection
