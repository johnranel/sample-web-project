<!DOCTYPE html>
<html lang="en">
    {{-- Head Before AUTH--}}
    @include('auth.includes.head')
    <body class="bg-white text-dark">
        <div class="container" id="wrapper">
            {{-- Content Goes Here FOR Before AUTH --}}
            @yield('styles')
            @yield('content')
        </div>
        {{-- Scripts Before AUTH --}}
        @include('auth.includes.scripts')
        @yield('scripts')
    </body>
</html>