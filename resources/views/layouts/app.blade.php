<!DOCTYPE html>
<html lang="en">

{{-- Include Head --}}
@include('common.head')
@yield('styles')

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            @include('common.sidebar')
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content" class="animate-in">

                    <!-- Topbar -->
                    @include('common.header')
                    @yield('styles')
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    @yield('content')
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                @include('common.footer')
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->

        @if(auth()->user()->hasRole('SuperAdmin'))
            @include('common.franchise-select')
            @include('common.gym-select')
            <script>
                var base_url = $('meta[name="base-url"]').attr('content');

                $("#view-dashboard-franchise-btn").click(function() {
                    let franchise_id = $(".select-franchise").val();
                    window.open(base_url + '/dashboard/view/admin/franchise/transactions/' + franchise_id);
                });

                $("#view-dashboard-gym-btn").click(function() {
                    let gym_id = $(".select-gym").val();
                    window.open(base_url + '/dashboard/view/admin/gym/transactions/' + gym_id);
                });

                $(".franchise-selector-modal").click(function () {
                    $("#franchise-selector-modal").modal("show");
                });

                $(".gym-selector-modal").click(function () {
                    $("#gym-selector-modal").modal("show");
                });
            </script>
        @endif
        
        @include('common.logout-modal')
        @yield('scripts')
    </body>

</html>