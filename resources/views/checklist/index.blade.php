@if(App\Models\GymFranchise::get()->isEmpty() || App\Models\IndividualGym::get()->isEmpty() || App\Models\Setting::where('topup_amount', 30)->get()->isEmpty() || App\Models\Setting::where('topup_amount', 50)->get()->isEmpty() || App\Models\Setting::where('topup_amount', 100)->get()->isEmpty() || App\Models\Setting::where('topup_amount', 150)->get()->isEmpty() || App\Models\Product::get()->isEmpty())
    <div class="col-md-12 mt-3">
        <div class="alert alert-danger" role="alert">
            This is the checklist on first time setup. Please try to complete them before accessing or distributing the app.
            <ul>
                <li @if(!App\Models\GymFranchise::get()->isEmpty()) style="text-decoration: line-through;" @endif>Create a franchise.</li>
                <li @if(!App\Models\IndividualGym::get()->isEmpty()) style="text-decoration: line-through;" @endif>Create a gym.</li>
                <li @if(!App\Models\Setting::where('topup_amount', 30)->get()->isEmpty() && !App\Models\Setting::where('topup_amount', 50)->get()->isEmpty() && !App\Models\Setting::where('topup_amount', 100)->get()->isEmpty() && !App\Models\Setting::where('topup_amount', 150)->get()->isEmpty()) style="text-decoration: line-through;" @endif>Set your settings (30, 50, 100, 150).</li>
                <li @if(!App\Models\Product::get()->isEmpty()) style="text-decoration: line-through;" @endif>Create your products.</li>
            </ul>
        </div>
    </div>
@endif