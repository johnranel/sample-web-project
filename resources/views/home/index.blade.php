@extends('home.layouts.app')

@section('title', 'Home')

@section('scripts')
<script>
    function password_show_hide() {
        var x = document.getElementById("password");
        var show_eye = document.getElementById("show_eye");
        var hide_eye = document.getElementById("hide_eye");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
</script>
@endsection

@section('styles')
    <style>
        @font-face {
            font-family: 'MontserratLight';
            src: url('/fonts/MontserratLight.ttf');
        }

        @font-face {
            font-family: 'MontserratRegular';
            src: url('/fonts/MontserratRegular.ttf');
        }

        @font-face {
            font-family: 'MontserratBold';
            src: url('/fonts/MontserratBold.ttf');
        }

        .p-viewer {
            z-index: 9999;
            position: absolute;
            top: 30%;
            right: 10px;
        }

        .fa-eye {
            color: #fff;
        }

        .logo {
            height: 250px;
        }

        .card-body {
            box-shadow: unset;
            background-image: -webkit-linear-gradient(#fff, #fff, #fff);
        }

        .btn-info:hover {
            background-image: -webkit-linear-gradient(#fff, #fff, #fff);
            color: #000;
        }

        .btn-info {
            color: #ffffff;
            box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.25);
            background-image: -webkit-linear-gradient(#000, #000, #000);
            border-radius: 0em !important;
            border: 2px solid #36b9cc !important;
            padding: 5px;
            width: 97%;
        }

        .text-dark {
            color: #000 !important;
        }

        .form-control:focus {
            box-shadow: unset !important;
        }

        .form-control {
            border-radius: 0rem;
            border: 2px solid #36b9cc !important;
        }

        .input-group-text {
            border-radius: 0rem;
            border: 2px solid #36b9cc !important;
            background-color: #000 !important;
        }

        body {
            font-family: 'MontserratRegular';
            color: #000;
        }

        .card {
            background-color: transparent;
        }

        .nav-link {
            background-color: #000;
            border-radius: 1em;
            border: 3px solid #36b9cc;
        }

        .navbar-light .navbar-nav .nav-link {
            color: #fff;
        }

        .navbar-expand-lg .navbar-nav .nav-link {
            padding-right: 2rem;
            padding-left: 2rem;
        }

        .navbar-light .navbar-nav .nav-link:focus, .navbar-light .navbar-nav .nav-link:hover {
            color: #000;
        }

        .nav-link:hover {
            background-color: #000;
        }

        .navbar-light .navbar-brand:focus, .navbar-light .navbar-brand:hover {
            background-color: transparent;
        }

        .btn-coming-soon {
            background-color: #CFFF00;
            border-color: #CFFF00;
            font-size: 1.25rem;
            color: #000;
            margin-top: 40px;
        }

        h4 > i {
            color: #36b9cc;
        }

        a:hover {
            background-color: transparent;
            color: #FFFFFF;
        }

        .main-font {
            font-size: 48px;
            font-weight: bold;
        }

        .white-font {
            color: #FFFFFF;
        }

        .cyanne-font {
            color: #36b9cc;
        }

        .gray-font {
            color: #13181B;
        }

        .bold-font {
            font-weight: bold;
        }

        .bg-black {
            background-color: #000;
        }

        .bg-gray-gradient {
            background: linear-gradient(180deg, rgba(217, 217, 217, 0) 0%, #D9D9D9 100%);
        }

        .margin-top20 {
            margin-top: 20px;
        }

        .text-align-center {
            text-align: center;
        }

        .section-border-top {
            border-top: 5px solid #36b9cc;
        }

        .padding-vertical100 {
            padding-top: 100px;
            padding-bottom: 100px;
        }

        .margin-vertical-1-5rem {
            margin-top: 1rem; 
            margin-bottom: 1.5rem;
        }

        .app-description-text {
            color: #7B7B7B; 
            font-size: 15px;
        }
        
        .reviewer-text {
            color: #7B7B7B; 
            font-size: 20px;
        }

        .main-font2 {
            font-size: 32px;
            font-weight: bold;
        }

        .reviewer-name {
            font-size: 20px;
            font-weight: bold;
            color: #000;
        }

        .login-container {
            flex-direction: column; 
            align-items: center; 
            padding-top: 100px; 
            padding-bottom: 100px;
        }

        .login-header-title {
            font-weight: 600; 
            font-size: 16px;
        }

        .footer-container {
            flex-direction: column;
            align-items: center;
            padding-top: 30px;
            padding-bottom: 30px;
        }

        .footer-text {
            font-size: 12px; 
            color: #fff; 
            margin-top: 20px;
        }

        @media (min-width:326px) {
            .navbar-brand > img {
                height: 80px;
            }

            .navbar-expand-lg .navbar-nav .nav-link {
                text-align: center;
            }

            .top-banner-section {
                background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('{{ asset("images/top_banner.jpg") }}'); 
                height: 550px;
                background-size: cover;
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                background-position: center;
                background-repeat: no-repeat;
            }

            .app-image {
                width: 60%;
            }

            .app-description-container {
                justify-content: center; 
                display: flex; 
                flex-direction: column; 
                padding-left: 0rem;
                margin-top: 20px;
                text-align: center;
            }

            .review-container {
                margin-top: 50px; 
                padding: 20px; 
                border-radius: 25px; 
                text-align: left; 
                width: 100%;
            }

            .review-image-container, .app-image-container {
                justify-content: center;
            }

            .review-image {
                height: 250px;
                width: 250px;
                object-fit: cover;
                margin-bottom: 20px;
            }
        }

        @media (min-width:576px) {
            .navbar-brand > img {
                height: 100px;
            }

            .navbar-expand-lg .navbar-nav .nav-link {
                text-align: center;
            }

            .top-banner-section {
                background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('{{ asset("images/top_banner.jpg") }}'); 
                height: 550px;
                background-size: cover;
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                background-position: center;
                background-repeat: no-repeat;
            }

            .app-image {
                width: 100%;
            }

            .app-description-container {
                justify-content: center; 
                display: flex; 
                flex-direction: column; 
                padding-left: 0rem;
                margin-top: 20px;
                text-align: center;
            }

            .review-container {
                margin-top: 50px; 
                padding: 20px; 
                border-radius: 25px; 
                text-align: left; 
                width: 100%; 
            }

            .review-image-container, .app-image-container {
                justify-content: center;
            }

            .review-image {
                height: 100%;
                width: 100%;
                object-fit: cover;
                margin-bottom: 20px;
            }
        }

        @media (min-width:768px) {
            .navbar-brand > img {
                height: 150px;
            }

            .navbar-expand-lg .navbar-nav .nav-link {
                text-align: center;
            }

            .top-banner-section {
                background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('{{ asset("images/top_banner.jpg") }}'); 
                height: 550px; 
                background-size: cover; 
                display: flex; 
                justify-content: center; 
                align-items: center; 
                flex-direction: column; 
                background-position: center;
            }

            .app-image {
                width: 100%;
            }

            .app-description-container {
                justify-content: center; 
                display: flex; 
                flex-direction: column; 
                padding-left: 3rem;
                margin-top: 0px;
                text-align: left;
            }

            .review-container {
                margin-top: 50px; 
                padding: 20px; 
                border-radius: 25px; 
                text-align: left; 
                width: 100%; 
            }

            .review-image-container, .app-image-container {
                justify-content: center;
            }

            .review-image {
                height: 200px;
                width: 200px;
                object-fit: cover;
                margin-bottom: 0px;
            }
        }

        @media (min-width:992px) {
            .navbar-brand > img {
                height: 150px;
            }

            .navbar-expand-lg .navbar-nav .nav-link {
                text-align: center;
            }

            .top-banner-section {
                background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('{{ asset("images/top_banner.jpg") }}'); 
                height: 650px; 
                background-size: cover; 
                display: flex; 
                justify-content: center; 
                align-items: center; 
                flex-direction: column; 
                background-position-y: -120px;
            }

            .app-image {
                width: 100%;
            }

            .app-description-container {
                justify-content: center; 
                display: flex; 
                flex-direction: column; 
                padding-left: 3rem;
                margin-top: 0px;
                text-align: left;
            }

            .review-container {
                margin-top: 50px; 
                padding: 20px; 
                border-radius: 25px; 
                text-align: left; 
                width: 70%; 
            }

            .review-image-container, .app-image-container {
                justify-content: center;
            }

            .review-image {
                height: 100%;
                width: 100%;
                object-fit: cover;
                margin-bottom: 0px;
            }
        }

        @media (min-width:1200px) {
            .navbar-brand > img {
                height: 150px;
            }

            .navbar-expand-lg .navbar-nav .nav-link {
                text-align: center;
            }

            .top-banner-section {
                background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('{{ asset("images/top_banner.jpg") }}'); 
                height: 650px; 
                background-size: cover; 
                display: flex; 
                justify-content: center; 
                align-items: center; 
                flex-direction: column; 
                background-position-y: -120px;
            }

            .app-image {
                width: 100%;
            }

            .app-description-container {
                justify-content: center; 
                display: flex; 
                flex-direction: column; 
                padding-left: 3rem;
                margin-top: 0px;
                text-align: left;
            }

            .review-container {
                margin-top: 50px; 
                padding: 20px; 
                border-radius: 25px; 
                text-align: left; 
                width: 70%; 
                border: 5px solid #36b9cc;
            }

            .review-image-container, .app-image-container {
                justify-content: center;
            }

            .review-image {
                height: 100%;
                width: 100%;
                object-fit: cover;
                margin-bottom: 0px;
            }
        }
    </style>
@endsection
@section('content')
    <div class="w-100">
        <div class="w-100 bg-light">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light w-100">
                    <a class="navbar-brand" href="#">
                        <h3>MuscleMart</h3>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#about-section">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#reviews-section">Reviews</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#contact-section">Contact</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 section-border-top" id="download-section">
                <div class="top-banner-section">
                    <h1 class="main-font white-font margin-top20 text-align-center">Welcome to Muscle Mart</h1>
                </div>
            </div>
        </div>
        <div class="w-100 bg-light section-border-top" id="about-section">
            <div class="container padding-vertical100">
                <div class="row">
                    <div class="col-xl-1 col-lg-1"></div>
                    <div class="col-xl-3 col-lg-3 col-md-4 d-flex app-image-container">
                        <img src="{{ asset('images/mockup-app.png') }}" class="app-image" />
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-8 app-description-container">
                        <h5 class="bold-font">MUSCLEMART</h5>
                        <h1 class="main-font cyanne-font margin-vertical-1-5rem">Innovate Fitness</h1>
                        <p class="app-description-text">Fuel Your Gains with MuscleMart: Your Ultimate Fitness Companion.</p>
                        <p class="app-description-text">Unlock your potential and start your fitness journey today with MuscleMart. Join now and elevate your gains!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-100 bg-black section-border-top" id="reviews-section">
            <div class="container padding-vertical100 text-align-center">
                <h3 class="main-font2 white-font">MuscleMart Reviews</h3>
                <div class="row">
                    <div class="bg-light mx-auto review-container col-5">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <h4 class="reviewer-name">Nathan <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
                                <p class="reviewer-text">I love MuscleMart! Their app is convenient, and signing up was a breeze with Join Now. Plus, their customer service is top-notch. Highly recommend for anyone serious about their fitness goals.</p>
                            </div>
                        </div>
                    </div>
                    <div class="bg-light mx-auto review-container col-5">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <h4 class="reviewer-name">Emma <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
                                <p class="reviewer-text">MuscleMart is my one-stop shop for supplements. Their app is easy to use, and the Join Now feature simplified the process. With top-quality products and excellent customer service.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 bg-gray-gradient section-border-top" id="contact-section">
            <div class="container padding-vertical100">
                <div class="card o-hidden border-0">
                    <form method="post" action="{{ route('contact') }}">
                        @csrf
                        <div class="card-body p-0 ml-4 mr-4 mb-3">
                            <div class="row p-5">
                                <div class="col-md-12">
                                    <h3 class="main-font2 gray-font">CONTACT</h3>
                                    <p class="app-description-text">Got questions or feedback? We're here to help! Reach out to us at MuscleMart – your trusted fitness partner. Contact us now to fuel your journey to greatness!</p>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" autofocus placeholder="First name *">
                                                @if($errors->has('first_name'))
                                                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" autofocus placeholder="Last name *">
                                                @if($errors->has('last_name'))
                                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input id="postal_code" type="text" class="form-control" name="postal_code" value="{{ old('postal_code') }}" autofocus placeholder="Postal / Zip Code *">
                                                @if($errors->has('postal_code'))
                                                    <span class="text-danger">{{ $errors->first('postal_code') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="Email *">
                                                @if($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="message" placeholder="Message *"></textarea>
                                                @if($errors->has('message'))
                                                    <span class="text-danger">{{ $errors->first('message') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group m-0">
                                                <select class="form-control" name="country" placeholder="Country *" required="">
                                                    <option value="AU">Australia</option>
                                                    <option value="US">United States</option>
                                                    <option value="DE">Germany</option>
                                                    <option value="AF">Afghanistan</option>
                                                    <option value="AL">Albania</option>
                                                    <option value="DZ">Algeria</option>
                                                    <option value="AS">American Samoa</option>
                                                    <option value="AD">Andorra</option>
                                                    <option value="AO">Angola</option>
                                                    <option value="AI">Anguilla</option>
                                                    <option value="AR">Argentina</option>
                                                    <option value="AM">Armenia</option>
                                                    <option value="AW">Aruba</option>
                                                    <option value="AT">Austria</option>
                                                    <option value="AZ">Azerbaijan</option>
                                                    <option value="BS">Bahamas</option>
                                                    <option value="BH">Bahrain</option>
                                                    <option value="BD">Bangladesh</option>
                                                    <option value="BB">Barbados</option>
                                                    <option value="BY">Belarus</option>
                                                    <option value="BE">Belgium</option>
                                                    <option value="BZ">Belize</option>
                                                    <option value="BJ">Benin</option>
                                                    <option value="BM">Bermuda</option>
                                                    <option value="BT">Bhutan</option>
                                                    <option value="BO">Bolivia</option>
                                                    <option value="BA">Bosnia and Herzegowina</option>
                                                    <option value="BW">Botswana</option>
                                                    <option value="BV">Bouvet Island</option>
                                                    <option value="BR">Brazil</option>
                                                    <option value="IO">British Indian Ocean Territory</option>
                                                    <option value="BN">Brunei Darussalam</option>
                                                    <option value="BG">Bulgaria</option>
                                                    <option value="BF">Burkina Faso</option>
                                                    <option value="BI">Burundi</option>
                                                    <option value="KH">Cambodia</option>
                                                    <option value="CM">Cameroon</option>
                                                    <option value="CA">Canada</option>
                                                    <option value="CV">Cape Verde</option>
                                                    <option value="KY">Cayman Islands</option>
                                                    <option value="CF">Central African Republic</option>
                                                    <option value="TD">Chad</option>
                                                    <option value="CL">Chile</option>
                                                    <option value="CN">China</option>
                                                    <option value="CX">Christmas Island</option>
                                                    <option value="CC">Cocos (Keeling) Islands</option>
                                                    <option value="CO">Colombia</option>
                                                    <option value="KM">Comoros</option>
                                                    <option value="CG">Congo</option>
                                                    <option value="CD">Congo, the Democratic Republic of the</option>
                                                    <option value="CK">Cook Islands</option>
                                                    <option value="CR">Costa Rica</option>
                                                    <option value="CI">Cote d'Ivoire</option>
                                                    <option value="HR">Croatia (Hrvatska)</option>
                                                    <option value="CU">Cuba</option>
                                                    <option value="CY">Cyprus</option>
                                                    <option value="CZ">Czech Republic</option>
                                                    <option value="DK">Denmark</option>
                                                    <option value="DJ">Djibouti</option>
                                                    <option value="DM">Dominica</option>
                                                    <option value="DO">Dominican Republic</option>
                                                    <option value="EC">Ecuador</option>
                                                    <option value="EG">Egypt</option>
                                                    <option value="SV">El Salvador</option>
                                                    <option value="GQ">Equatorial Guinea</option>
                                                    <option value="ER">Eritrea</option>
                                                    <option value="EE">Estonia</option>
                                                    <option value="ET">Ethiopia</option>
                                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                                    <option value="FO">Faroe Islands</option>
                                                    <option value="FJ">Fiji</option>
                                                    <option value="FI">Finland</option>
                                                    <option value="FR">France</option>
                                                    <option value="GF">French Guiana</option>
                                                    <option value="PF">French Polynesia</option>
                                                    <option value="TF">French Southern Territories</option>
                                                    <option value="GA">Gabon</option>
                                                    <option value="GM">Gambia</option>
                                                    <option value="GE">Georgia</option>
                                                    <option value="GH">Ghana</option>
                                                    <option value="GI">Gibraltar</option>
                                                    <option value="GR">Greece</option>
                                                    <option value="GL">Greenland</option>
                                                    <option value="GD">Grenada</option>
                                                    <option value="GP">Guadeloupe</option>
                                                    <option value="GU">Guam</option>
                                                    <option value="GT">Guatemala</option>
                                                    <option value="GN">Guinea</option>
                                                    <option value="GW">Guinea-Bissau</option>
                                                    <option value="GY">Guyana</option>
                                                    <option value="HT">Haiti</option>
                                                    <option value="HM">Heard and Mc Donald Islands</option>
                                                    <option value="VA">Holy See (Vatican City State)</option>
                                                    <option value="HN">Honduras</option>
                                                    <option value="HK">Hong Kong</option>
                                                    <option value="HU">Hungary</option>
                                                    <option value="IS">Iceland</option>
                                                    <option value="IN">India</option>
                                                    <option value="ID">Indonesia</option>
                                                    <option value="IR">Iran (Islamic Republic of)</option>
                                                    <option value="IQ">Iraq</option>
                                                    <option value="IE">Ireland</option>
                                                    <option value="IL">Israel</option>
                                                    <option value="IT">Italy</option>
                                                    <option value="JM">Jamaica</option>
                                                    <option value="JP">Japan</option>
                                                    <option value="JO">Jordan</option>
                                                    <option value="KZ">Kazakhstan</option>
                                                    <option value="KE">Kenya</option>
                                                    <option value="KI">Kiribati</option>
                                                    <option value="KP">Korea, Democratic People's Republic of</option>
                                                    <option value="KR">Korea, Republic of</option>
                                                    <option value="KW">Kuwait</option>
                                                    <option value="KG">Kyrgyzstan</option>
                                                    <option value="LA">Lao People's Democratic Republic</option>
                                                    <option value="LV">Latvia</option>
                                                    <option value="LB">Lebanon</option>
                                                    <option value="LS">Lesotho</option>
                                                    <option value="LR">Liberia</option>
                                                    <option value="LY">Libyan Arab Jamahiriya</option>
                                                    <option value="LI">Liechtenstein</option>
                                                    <option value="LT">Lithuania</option>
                                                    <option value="LU">Luxembourg</option>
                                                    <option value="MO">Macau</option>
                                                    <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                                    <option value="MG">Madagascar</option>
                                                    <option value="MW">Malawi</option>
                                                    <option value="MY">Malaysia</option>
                                                    <option value="MV">Maldives</option>
                                                    <option value="ML">Mali</option>
                                                    <option value="MT">Malta</option>
                                                    <option value="MH">Marshall Islands</option>
                                                    <option value="MQ">Martinique</option>
                                                    <option value="MR">Mauritania</option>
                                                    <option value="MU">Mauritius</option>
                                                    <option value="YT">Mayotte</option>
                                                    <option value="MX">Mexico</option>
                                                    <option value="FM">Micronesia, Federated States of</option>
                                                    <option value="MD">Moldova, Republic of</option>
                                                    <option value="MC">Monaco</option>
                                                    <option value="MN">Mongolia</option>
                                                    <option value="MS">Montserrat</option>
                                                    <option value="MA">Morocco</option>
                                                    <option value="MZ">Mozambique</option>
                                                    <option value="MM">Myanmar</option>
                                                    <option value="NA">Namibia</option>
                                                    <option value="NR">Nauru</option>
                                                    <option value="NP">Nepal</option>
                                                    <option value="NL">Netherlands</option>
                                                    <option value="AN">Netherlands Antilles</option>
                                                    <option value="NC">New Caledonia</option>
                                                    <option value="NZ">New Zealand</option>
                                                    <option value="NI">Nicaragua</option>
                                                    <option value="NE">Niger</option>
                                                    <option value="NG">Nigeria</option>
                                                    <option value="NU">Niue</option>
                                                    <option value="NF">Norfolk Island</option>
                                                    <option value="MP">Northern Mariana Islands</option>
                                                    <option value="NO">Norway</option>
                                                    <option value="OM">Oman</option>
                                                    <option value="PK">Pakistan</option>
                                                    <option value="PW">Palau</option>
                                                    <option value="PA">Panama</option>
                                                    <option value="PG">Papua New Guinea</option>
                                                    <option value="PY">Paraguay</option>
                                                    <option value="PE">Peru</option>
                                                    <option value="PH">Philippines</option>
                                                    <option value="PN">Pitcairn</option>
                                                    <option value="PL">Poland</option>
                                                    <option value="PT">Portugal</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="QA">Qatar</option>
                                                    <option value="RE">Reunion</option>
                                                    <option value="RO">Romania</option>
                                                    <option value="RU">Russian Federation</option>
                                                    <option value="RW">Rwanda</option>
                                                    <option value="KN">Saint Kitts and Nevis</option>
                                                    <option value="LC">Saint LUCIA</option>
                                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                                    <option value="WS">Samoa</option>
                                                    <option value="SM">San Marino</option>
                                                    <option value="ST">Sao Tome and Principe</option>
                                                    <option value="SA">Saudi Arabia</option>
                                                    <option value="SN">Senegal</option>
                                                    <option value="SC">Seychelles</option>
                                                    <option value="SL">Sierra Leone</option>
                                                    <option value="SG">Singapore</option>
                                                    <option value="SK">Slovakia (Slovak Republic)</option>
                                                    <option value="SI">Slovenia</option>
                                                    <option value="SB">Solomon Islands</option>
                                                    <option value="SO">Somalia</option>
                                                    <option value="ZA">South Africa</option>
                                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                    <option value="ES">Spain</option>
                                                    <option value="LK">Sri Lanka</option>
                                                    <option value="SH">St. Helena</option>
                                                    <option value="PM">St. Pierre and Miquelon</option>
                                                    <option value="SD">Sudan</option>
                                                    <option value="SR">Suriname</option>
                                                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                    <option value="SZ">Swaziland</option>
                                                    <option value="SE">Sweden</option>
                                                    <option value="CH">Switzerland</option>
                                                    <option value="SY">Syrian Arab Republic</option>
                                                    <option value="TW">Taiwan, Province of China</option>
                                                    <option value="TJ">Tajikistan</option>
                                                    <option value="TZ">Tanzania, United Republic of</option>
                                                    <option value="TH">Thailand</option>
                                                    <option value="TG">Togo</option>
                                                    <option value="TK">Tokelau</option>
                                                    <option value="TO">Tonga</option>
                                                    <option value="TT">Trinidad and Tobago</option>
                                                    <option value="TN">Tunisia</option>
                                                    <option value="TR">Turkey</option>
                                                    <option value="TM">Turkmenistan</option>
                                                    <option value="TC">Turks and Caicos Islands</option>
                                                    <option value="TV">Tuvalu</option>
                                                    <option value="UG">Uganda</option>
                                                    <option value="UA">Ukraine</option>
                                                    <option value="AE">United Arab Emirates</option>
                                                    <option value="GB">United Kingdom</option>
                                                    <option value="UM">United States Minor Outlying Islands</option>
                                                    <option value="UY">Uruguay</option>
                                                    <option value="UZ">Uzbekistan</option>
                                                    <option value="VU">Vanuatu</option>
                                                    <option value="VE">Venezuela</option>
                                                    <option value="VN">Viet Nam</option>
                                                    <option value="VG">Virgin Islands (British)</option>
                                                    <option value="VI">Virgin Islands (U.S.)</option>
                                                    <option value="WF">Wallis and Futuna Islands</option>
                                                    <option value="EH">Western Sahara</option>
                                                    <option value="YE">Yemen</option>
                                                    <option value="ZM">Zambia</option>
                                                    <option value="ZW">Zimbabwe</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-0">
                                                <button class="btn btn-info">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 bg-gray-gradient section-border-top">
            <div class="row justify-content-center login-container">
                <h3 class="main-font2 gray-font">Admin Area</h3>
                <div class="col-xl-4 col-lg-4 col-md-8 mt-2">
                    <div class="card o-hidden border-0">
                        <form method="post" action="{{ route('login') }}">
                            @csrf
                            <div class="card-body p-0 ml-4 mr-4 mb-3">
                                <!-- Nested Row within Card Body -->
                                <div class="row justify-content-center">
                                    <div class="col-xl-11 col-lg-11 col-md-11 col-xs-11 col-11">
                                        <div class="py-4">
                                            @if(session('error'))
                                                <span class="text-danger"> {{ session('error') }}</span>
                                            @endif
                                            <div class="form-group">
                                                @if($errors->has('credential_error'))
                                                    <span class="text-danger"> {{ $errors->first('credential_error') }}</span>
                                                @endif
                                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" style="border: 1px solid #80C8BC" autofocus placeholder="Username">
                                                @if($errors->has('username'))
                                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group mb-0">
                                                <div class="input-group">
                                                    <input name="password" type="password" value="" class="input form-control form-control form-control-user @error('password') is-invalid @enderror" id="password"  placeholder="Password" aria-label="password" aria-describedby="basic-addon1" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="password_show" onclick="password_show_hide();">
                                                        <i class="fas fa-eye" id="show_eye"></i>
                                                        <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                                        </span>
                                                    </div>
                                                </div>

                                                @if($errors->has('password'))
                                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                                @endif
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col text-center">
                                <button class="btn btn-info">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 bg-black section-border-top">
            <div class="row justify-content-center footer-container">
                <h3 class="text-white">MuscleMart</h3>
                <span class="footer-text">© {{ date('Y') }} Sample Web Project by JRD</span>
            </div>
        </div>
    </div>
@endsection