------------------------------------------------------------------------------------------------------------------ <br>
This a message from our site (Odalis.com.au).<br>
Please do not reply directly to this email.<br>
------------------------------------------------------------------------------------------------------------------
<br><br>
<div>
    Sender Details:
    <ul>
        <li>Full name: {{ $data->first_name }} {{ $data->last_name }}</li>
        <li>Email: {{ $data->email }}</li>
        <li>Country: {{ $data->country }}</li>
        <li>Postal / Zip code: {{ $data->postal_code }}</li>
    </ul>
    Message:<br>
    <p style="width: 400px;">{{ $data->message }}</p>
</div>
<br><br>
------------------------------------------------------------------------------------------------------------------ <br>
This a message from our site (Odalis.com.au).<br>
Please do not reply directly to this email.<br>
------------------------------------------------------------------------------------------------------------------<br><br>